import React, {Component} from 'react'
import './App.scss'
import {
  connect,
  Provider
} from 'react-redux'
import {
  Redirect,
  Route,
  Router,
  Switch
} from 'react-router-dom'
import Home from './view/home/Home'
import ScrollToTop from './view/common/ScrollToTop'
import Navigation from './view/common/navigation/Navigation'
import Imprint from './view/imprint/Imprint'
import LoginContainer from './view/authentication/LoginContainer'
import PrivateRoute from './view/PrivateRoute'
import ApplicantListContainer from './view/applicantList/ApplicationListContainer'
import LotteryContainer from './view/lottery/LotteryContainer'
import ParticipantListContainer from './view/participantList/ParticipantListContainer'
import NameTagsContainer from './view/nameTags/NameTagsContainer'

export type Props = {
  store: any
  history: any
}

export class App extends Component<Props> {
  static defaultProps = {}

  render(): JSX.Element {
    return (
      <Provider store={this.props.store}>
        <Router history={this.props.history}>
          <ScrollToTop>
            <Switch>
              <Redirect exact from="/" to="/home"/>
              <PrivateRoute path="/home">
                <div>
                  <Navigation />
                  <Home/>
                </div>
              </PrivateRoute>
              <Route path="/imprint">
                <div>
                  <Navigation/>
                  <Imprint/>
                </div>
              </Route>
              <PrivateRoute path="/applicants">
                <div>
                  <Navigation/>
                  <ApplicantListContainer/>
                </div>
              </PrivateRoute>
              <PrivateRoute path="/participants">
                <div>
                  <Navigation/>
                  <ParticipantListContainer/>
                </div>
              </PrivateRoute>
              <PrivateRoute path="/lottery">
                <div>
                  <Navigation/>
                  <LotteryContainer/>
                </div>
              </PrivateRoute>
              <PrivateRoute path="/name-tags">
                <div>
                  <Navigation/>
                  <NameTagsContainer/>
                </div>
              </PrivateRoute>
              <Route path="/login">
                <div>
                  <Navigation/>
                  <LoginContainer/>
                </div>
              </Route>
            </Switch>
          </ScrollToTop>
        </Router>
      </Provider>
    )
  }
}

const mapStateToProps = (state: any): any => {
  return {...state}
}

export default connect(mapStateToProps)(App)
