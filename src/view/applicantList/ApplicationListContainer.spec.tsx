import React from 'react'
import {shallow, ShallowWrapper} from 'enzyme'
import ApplicationListContainer from './ApplicationListContainer'
import StoreFactory, {HistoryFactory} from '../store/store'

function* emptySaga(): any {
}

const reducer = (): any => {
  return {
    applicationList: {
      lastActionResult: {commandId: '', error: '', result: ''},
      roomTypes: [],
      rows: []
    }
  }
}
const store = StoreFactory.createTestStore(reducer, emptySaga, HistoryFactory.createTestHistory())

describe('(Component) ApplicationListContainer', () => {
  let wrapper: ShallowWrapper
  describe('renders', () => {
    beforeEach(() => {
      wrapper = shallow(
      // @ts-ignore
        <ApplicationListContainer store={store}/>)
    })

    it('without exploding', () => {
      expect(wrapper).toHaveLength(1)
    })

    it('renders correctly', () => {
      expect(wrapper).toMatchSnapshot()
    })
  })
})
