import React, {Component} from 'react'
import type {ApplicationListItem} from './commons'
import ApplicationListTable from './ApplicationListTable'
import {
  getApplications, closeModal, executeAction,
  getRoomTypes, changeRoomTypeFilter, handleAction, ApplicationListCommandId
} from '../commands/applicationListCommand'
import {connect} from 'react-redux'
import type {ApplicationListState} from '../reducers/applicantListReducer'
import type {Action} from '../common/table/TableTypes'
import ApplicationListModals from './ApplicationListModals'
import {emptyApplicationListItem} from './commons'
type StateFromProps = {
  applicationList: ApplicationListState
}

type DispatchFromProps = {
  closeModal: () => void
  executeAction: (item:ApplicationListItem, arg1:string, arg2:string) => void
  handleAction: (action:string, item: ApplicationListItem) => void
  changeRoomTypeFilter: (filter:string) => void
  getRoomTypes: () => void
  getApplications: (type:string) => void
}

export type Props =StateFromProps & DispatchFromProps

class ApplicationListContainer extends Component<Props> {
  _rowActions: Array<Action> = [
    {
      execute: (item) => this.props.handleAction(ApplicationListCommandId.DELETE, item),
      canExecute: () => true, buttonClass: 'btn-danger',
      icon: 'trash', text: 'Delete', visible: true
    },
    {
      execute: (item) => this.props.handleAction(ApplicationListCommandId.EDIT, item),
      canExecute: () => true, buttonClass: 'btn-info',
      icon: 'edit', text: 'Edit', visible: true
    },
    {
      execute: (item) => this.props.handleAction(ApplicationListCommandId.REGISTER, item),
      canExecute: (item) => (item.roomTypes && item.roomTypes.length === 1), buttonClass: 'btn-success',
      icon: 'registered', text: 'Register', visible: true
    }
  ]

  _tableActions: Array<Action> = [
    {
      execute: () => this.props.getApplications(this.props.applicationList.currentRoomTypeFilter),
      canExecute: () => true, buttonClass: 'btn-info',
      icon: 'sync', text: 'Refresh', visible: true
    }
  ]

  componentDidMount = (): void => {
    this.props.getRoomTypes()
    this.props.getApplications('all')
  }

  render = (): JSX.Element => {
    const {
      rows, roomTypes, currentCommand, currentRow, errorMessage, currentRoomTypeFilter
    } = this.props.applicationList
    return (
      <div className="container">
        {
          currentCommand !== '' ?
            <ApplicationListModals
              commandId={currentCommand} data={currentRow !== undefined? currentRow : emptyApplicationListItem} roomTypes={roomTypes}
              errorMessage={errorMessage} closeModal={this.props.closeModal}
              executeAction={(applicationItem, commandId) =>
                this.props.executeAction(applicationItem, commandId, currentRoomTypeFilter)}
            /> : null
        }
        <ApplicationListTable
          rowActions={this._rowActions}
          tableActions={this._tableActions}
          roomTypes={roomTypes}
          rows={rows}
          refresh={()=>{}}
          roomTypeFilterChanged={this.props.changeRoomTypeFilter}
        />
      </div>
    )
  }
}

const mapStateToProps = (state: any): StateFromProps => {
  return {applicationList: {...state.applicationList}}
}

const mapDispatchToProps: DispatchFromProps = {
  getRoomTypes, getApplications, changeRoomTypeFilter,
  handleAction, closeModal, executeAction
}

export default connect<StateFromProps, DispatchFromProps>(mapStateToProps, mapDispatchToProps)(ApplicationListContainer)
