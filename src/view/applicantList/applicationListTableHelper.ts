import moment from 'moment/moment'
import {TableValue} from './ApplicationListTable'

export function dateFilter (data: Array<Record<string, TableValue>>, field: string, value: string, format: string): Record<string,TableValue>[] {
  return data.filter((item: Record<string,TableValue>) => {
    if(typeof item[field] !== 'string') {
      return false
    } else {
      const valueToCheck = moment(item[field] as string).format(format)
      return valueToCheck.indexOf(value) >= 0
    }
  })
}

