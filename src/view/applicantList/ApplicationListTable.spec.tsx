import React from 'react'
import {shallow, mount, ReactWrapper, ShallowWrapper} from 'enzyme'
import axios from 'axios'
import ApplicationListTable from './ApplicationListTable'
import TableRow from '../common/table/body/TableRow'
import {SinonSpy, spy} from 'sinon'
import '../icons'
import {ApplicationListItem} from './commons'

jest.mock('axios')

const testData: ApplicationListItem[] = [
  {
    'personId': 280,
    'applicantId': 402,
    'date': '2018-03-01T15:00:00.000Z',
    'name': 'name 280',
    'email': 'name280@email.de',
    'roomTypes': [
      'single'
    ],
    'diversityReason': '',
    'diversityDescription': ''
  },
  {
    'personId': 400,
    'applicantId': 574,
    'date': '2018-03-01T15:00:00.000Z',
    'name': 'name 400',
    'email': 'name400@email.de',
    'roomTypes': [
      'single',
      'junior_shared'
    ],
    'diversityReason': '',
    'diversityDescription': ''
  },
  {
    'personId': 20,
    'applicantId': 27,
    'date': '2018-04-01T19:00:00.000Z',
    'name': 'name 20',
    'email': 'name20@email.de',
    'roomTypes': [
      'single',
      'bed_in_double',
      'junior_exclusively'
    ],
    'diversityReason': '',
    'diversityDescription': ''
  }
]

const roomTypes = [
  {value: 'single', label: 'Single'},
  {value: 'bed_in_double', label: 'Double'},
  {value: 'junior_exclusively', label: 'JuniorEx'},
  {value: 'junior_shared', label: 'JuniorSh'}
]

describe('(Component) ApplicationListTable', () => {
  beforeEach(() => {
    axios.get.mockImplementation(() => Promise.resolve({data: testData}))
  })
  afterEach(() => {
    axios.get.mockReset()
  })
  describe('renders', () => {
    let wrapper: ShallowWrapper
    beforeEach(() => {
      wrapper = shallow(
        <ApplicationListTable
          tableActions={[]}
          rowActions={[]}
          refresh={() => {}}
          roomTypes={roomTypes}
          rows={testData}
          roomTypeFilterChanged={() => {}}
        />
      )
    })

    it('without exploding', () => {
      expect(wrapper).toHaveLength(1)
    })

    it('correctly', () => {
      expect(wrapper).toMatchSnapshot()
    })
  })

  describe('after render', () => {
    let wrapper: ReactWrapper
    let roomFilterChangedSpy: SinonSpy
    let refreshSpy: SinonSpy
    beforeEach(() => {
      roomFilterChangedSpy = spy()
      refreshSpy = spy()
      wrapper = mount(
        <ApplicationListTable
          tableActions={[]}
          rowActions={[]}
          refresh={refreshSpy}
          roomTypes={roomTypes}
          rows={testData}
          roomTypeFilterChanged={roomFilterChangedSpy}
        />
      )
    })
    afterEach(() => {
      roomFilterChangedSpy.resetHistory()
      refreshSpy.resetHistory()
    })
    it('rows are filled', () => {
      const rows = wrapper.find(TableRow)
      expect(rows.length).toBe(3)
    })
  })
})
