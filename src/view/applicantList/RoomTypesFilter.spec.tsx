import React from 'react'
import {shallow} from 'enzyme'
import RoomTypesFilter from './RoomTypesFilter'

describe('(Component) RoomTypesFilter', () => {
  const wrapper = shallow(
    <RoomTypesFilter roomTypes={[{value: '1', title: 'test room type'}]} filterChanged={() => {}}/>)

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
