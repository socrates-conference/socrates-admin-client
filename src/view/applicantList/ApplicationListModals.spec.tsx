import React from 'react'
import {shallow} from 'enzyme'
import ApplicationListModals from './ApplicationListModals'
import {emptyApplicationListItem} from './commons'


describe('(Component) ApplicationListModals', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallow(
      <ApplicationListModals
        commandId="delete" data={emptyApplicationListItem} roomTypes={[{value: '1', title: 'test room type'}]}
        errorMessage="" closeModal={() => {}}
        executeAction={() => {}}
      />
    )
  })

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
