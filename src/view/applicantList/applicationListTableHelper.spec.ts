import {dateFilter} from './applicationListTableHelper'

const testData = [
  {
    'personId': 280,
    'applicantId': 402,
    'date': '2018-03-01T15:00:00.000Z'
  },
  {
    'personId': 400,
    'applicantId': 574,
    'date': '2018-03-01T15:00:00.000Z'
  },
  {
    'personId': 20,
    'applicantId': 27,
    'date': '2018-04-01T19:00:00.000Z'
  }
]


describe('applicationListTableHelper:', () => {
  describe('dateFilter', () => {
    it('returns filtered array', () => {
      const emptyResult = dateFilter(testData, 'date', '2000')
      expect(emptyResult.length).toBe(0)
      const result = dateFilter(testData, 'date', '04')
      expect(result.length).toBe(1)
    })
  })
})