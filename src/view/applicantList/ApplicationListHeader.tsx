import React from 'react'

type Props = {
  title: string
}
export default function ApplicationListHeader(props: Props): JSX.Element {
  return <div className="row">
    <div className="col-sm-12">
      <div className="page-header"><h1>{props.title}</h1></div>
    </div>
  </div>
}
