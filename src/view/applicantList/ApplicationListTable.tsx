import React from 'react'
import Table from '../common/table/Table'
import type {
  Action,
  ColumnDefinition
} from '../common/table/TableTypes'
import EmailCellTemplate from '../common/table/cellTemplates/EmailCellTemplate'
import ArrayCellTemplate from '../common/table/cellTemplates/ArrayCellTemplate'
import RoomTypesFilter from './RoomTypesFilter'
import ApplicationListHeader from './ApplicationListHeader'
import type {ApplicationListItem} from './commons'
import dateFormat from '../../utils/dateFormat'
import {dateFilter} from './applicationListTableHelper'
import {LabeledItem} from '../../types'
import {Address} from '../participantList/commons'

export type Props = {
  rowActions: Array<Action>
  tableActions: Array<Action>
  refresh: () => void
  rows: Array<ApplicationListItem>
  roomTypes: Array<LabeledItem>
  roomTypeFilterChanged: (filter: string) => void
}

const dateTimeFormat = 'DD.MM.YYYY HH:mm:SS'

function emailTemplate(value: string): JSX.Element {
  return <EmailCellTemplate value={value}/>
}

function arrayTemplate(values: JSX.Element[]): JSX.Element {
  return <ArrayCellTemplate displayName="ArrayCellTemplate" values={values}/>
}

export type TableValue = string | number | boolean | Address

const columns: Array<ColumnDefinition> = [
  {
    header: 'Name', field: 'name', isSortable: true, isFilterable: true,
    visible: true
  },
  {
    header: 'Email', field: 'email', displayClass: 'd-none d-sm-table-cell',
    cellTemplate: (value: string) => emailTemplate(value),
    isSortable: true, isFilterable: true,
    visible: true
  },
  {
    header: 'Room types', field: 'roomTypes',
    cellTemplate: (values: Array<JSX.Element>) => arrayTemplate(values),
    isSortable: true,
    isFilterable: false,
    visible: true
  },
  {
    header: 'Date', field: 'date', format: (value) => dateFormat(value, dateTimeFormat),
    displayClass: 'd-none d-md-table-cell', isSortable: true, isFilterable: true,
    filter: (data: Record<string, TableValue>[], field, value) => dateFilter(data, field, value, dateTimeFormat),
    visible: true
  }
]


export default function ApplicationListTable(props: Props): JSX.Element {
  return (<div id="applicantList">
    <ApplicationListHeader title="Application list"/>
    <RoomTypesFilter filterChanged={props.roomTypeFilterChanged} roomTypes={props.roomTypes}/>
    <Table
            columns={columns} data={props.rows}
            rowActions={props.rowActions}
            tableActions={props.tableActions}
    />
  </div>)
}

