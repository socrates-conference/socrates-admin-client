import React from 'react'
import {shallow} from 'enzyme'
import ApplicationListHeader from './ApplicationListHeader'

describe('(Component) ApplicationListHeader', () => {
  const wrapper = shallow(
    <ApplicationListHeader title="The Header Title"/>)

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
