import moment, {Moment} from 'moment/moment'

export type ApplicationListItem = {
  personId: number
  applicantId: number
  date: Moment|string
  name: string
  email: string
  roomTypes: Array<string|number>
  diversityReason: string
  diversityDescription?: string
}
export const emptyApplicationListItem: ApplicationListItem = {
  personId: -1,
  applicantId: -1,
  date: moment('2000-01-01 00:00:00.000Z'),
  name: '',
  email: '',
  roomTypes: [],
  diversityReason: ''
}

