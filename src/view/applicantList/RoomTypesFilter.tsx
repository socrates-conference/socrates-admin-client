import React, {SyntheticEvent} from 'react'
import * as PropTypes from 'prop-types'
import {LabeledItem} from '../../types'

export type Props = {
  filterChanged: (value: string) => void
  roomTypes: Array<LabeledItem>
}

export default function RoomTypesFilter(props: Props): JSX.Element {
  return (<div id="select-room-type-filter" className="row mb-2">
    <div className="col-lg-4 col-sm-12 align-self-center">
      <label htmlFor="select-room-type">Select the room type to show: </label>
    </div>
    <div className="col-lg-8 col-sm-12">
      <select
              id="select-room-type" className="custom-select"
              onChange={(e: SyntheticEvent<HTMLSelectElement>) => props.filterChanged(e.currentTarget.value)}
              defaultValue="all">
        <option value="all">All room types</option>
        {
          props.roomTypes && props.roomTypes.map((item: LabeledItem) => {
            return (<option key={`room-type-${item.value}`} value={item.value}>
              {item.label}
            </option>)
          })
        }
      </select>
    </div>
  </div>)
}
RoomTypesFilter.propTypes = {
  filterChanged: PropTypes.func.isRequired,
  roomTypes: PropTypes.array.isRequired
}

