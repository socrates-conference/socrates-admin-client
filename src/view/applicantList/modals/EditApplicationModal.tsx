import React, {Component} from 'react'
import type {ModalButton} from '../../common/modal/Modal'
import Modal from '../../common/modal/Modal'
import type {ApplicationListItem} from '../commons'
import {emptyApplicationListItem} from '../commons'
import {ApplicationListCommandId} from '../../commands/applicationListCommand'
import {LabeledItem} from '../../../types'

type StateFromProps = {
  editData: ApplicationListItem
  roomTypes: Array<LabeledItem>
  show: boolean
}

type DispatchFromProps = {
  closeModal: () => void
  executeAction: (item: ApplicationListItem, action: string) => void

}

type Props = StateFromProps & DispatchFromProps & { id: string }

type State = {
  data: ApplicationListItem
  message: string
}

export default class EditApplicationModal extends Component<Props, State> {


  state: State = {
    data: emptyApplicationListItem,
    message: ''
  }

  componentDidMount = (): void => {
    this.setState({data: this.props.editData})
  }
  shouldComponentUpdate = (nextProps: Props): boolean =>{
    if (this.props.editData !== nextProps.editData) {
      this.setState({data: nextProps.editData})
    }
    return true
  }

  _editRoomTypeChanged = (value: string): void => {
    // ToDo handle action
    const roomTypes = this.state.data.roomTypes.slice()
    const index = roomTypes.indexOf(value)
    if (index >= 0) {
      roomTypes.splice(index, 1)
    } else {
      roomTypes.push(value)
    }
    const newEdit = {...this.state.data}
    newEdit.roomTypes = roomTypes
    this.setState({data: newEdit})
  }


  render = (): JSX.Element => {
    const editData = this.state.data
    const buttons: Array<ModalButton> = [
      {
        isSubmit: true, onClick: () => {this.props.executeAction(this.state.data, ApplicationListCommandId.EDIT)},
        text: 'Ok'
      },
      {
        isSubmit: false, onClick: () => {this.props.closeModal()},
        text: 'Cancel'
      }
    ]
    return (
      <Modal show={this.props.show} buttons={buttons}>
        <div className="modal-header">
          <h3 className="mb-2 mx-auto">
            <strong>Edit Application</strong>
          </h3>
        </div>
        <div className="mb-2 mx-auto alert alert-info">
          <small>{`for: ${editData.name} (${editData.email})`}</small>
        </div>
        <div className="form-row">
          <div className="form-group">
            <label htmlFor="roomType" className="col-form-label">Room Type</label>
            <div className="table-responsive">
              {this.props.roomTypes && this.props.roomTypes.map(({value, label}) => (
                <div key={value} className="radio-inline">
                  <label>
                    <input
                      checked={editData.roomTypes && editData.roomTypes.includes(value)}
                      id={value}
                      name="roomType"
                      onChange={() => this._editRoomTypeChanged(value)}
                      type="checkbox"
                      value={value}
                    />
                    <b>&nbsp;{label}</b>
                  </label>
                </div>
              ))}
            </div>
          </div>
        </div>
      </Modal>
    )
  }
}
