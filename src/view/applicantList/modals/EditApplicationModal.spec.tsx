import React from 'react'
import {mount} from 'enzyme'
import EditApplicationModal from './EditApplicationModal'
import {emptyApplicationListItem} from '../commons'

describe('(Component) EditApplicationModal', () => {
  let wrapper
  describe('renders', () => {
    beforeEach(() => {
      wrapper = mount(
        <EditApplicationModal
          id="editApplicationModal"
          editData={emptyApplicationListItem} roomTypes={[{value: '1', title: 'test room type'}]} show={true}
          closeModal={() => {}} executeAction={() => {}}
        />
      )
    })

    it('without exploding', () => {
      expect(wrapper).toHaveLength(1)
    })
  })

  describe('contains', () => {
    beforeEach(() => {
      wrapper = mount(
        <EditApplicationModal
          id="editApplicationModal"
          editData={emptyApplicationListItem}
          roomTypes={
            [
              {value: '1', title: 'test room type 1'},
              {value: '2', title: 'test room type 2'},
              {value: '3', title: 'test room type 3'},
              {value: '4', title: 'test room type 4'}
            ]
          }
          show={true}
          closeModal={() => {}} executeAction={() => {}}
        />
      )
    })

    it('defined edit data after mount', () => {
      const data = wrapper.state('data')
      expect(data).toBe(emptyApplicationListItem)
    })

    it('edit data is set after props will update', () => {
      const newProps = {editData: {...emptyApplicationListItem, id: 4711}}
      wrapper.instance().shouldComponentUpdate(newProps)
      const data = wrapper.state('data')
      expect(data.id).toBe(4711)
    })

    it('initial room types plus the new checked one after changed event', () => {
      const newProps = {editData: {...emptyApplicationListItem, roomTypes: ['1', '2']}}
      wrapper.instance().shouldComponentUpdate(newProps)
      wrapper.instance()._editRoomTypeChanged('3')
      wrapper.update()
      const data = wrapper.state('data')
      expect(data.roomTypes.length).toBe(3)
      expect(data.roomTypes).toContain('1')
      expect(data.roomTypes).toContain('2')
      expect(data.roomTypes).toContain('3')
    })

    it('initial room types minus the unchecked one after changed event', () => {
      const newProps = {editData: {...emptyApplicationListItem, roomTypes: ['1', '2']}}
      wrapper.instance().shouldComponentUpdate(newProps)
      wrapper.instance()._editRoomTypeChanged('2')
      wrapper.update()
      const data = wrapper.state('data')
      expect(data.roomTypes.length).toBe(1)
      expect(data.roomTypes).toContain('1')
    })

  })
})
