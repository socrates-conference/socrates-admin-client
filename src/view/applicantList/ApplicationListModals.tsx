import React from 'react'
import EditApplicationModal from './modals/EditApplicationModal'
import type {ApplicationListItem} from './commons'
import AreYouSureModal from '../common/modal/AreYouSureModal'
import ErrorModal from '../common/modal/ErrorModal'
import {LabeledItem} from '../../types'

type Props = {
  closeModal: () => void
  executeAction: (item: ApplicationListItem, action: string) => void
  commandId: string
  data: ApplicationListItem
  errorMessage: string
  roomTypes: Array<LabeledItem>
}

export default function ApplicationListModals (props: Props) : JSX.Element {
  const {roomTypes, data, commandId, closeModal, errorMessage, executeAction} = props
  return commandId !== '' ?
    <div>
      <EditApplicationModal
        id="editApplicationModal"
        editData={data} roomTypes={roomTypes} show={commandId === 'edit'}
        closeModal={closeModal} executeAction={executeAction}/>
      <AreYouSureModal
        id="areYouSureModal" data={data} commandId={commandId}
        show={commandId === 'delete' || commandId === 'register'}
        closeModal={closeModal} title={commandId} text={data.name + ' ' + data.email}
        executeAction={executeAction}
      />
      <ErrorModal
        show={commandId === 'error'} closeModal={closeModal}
        title="Error" text="Operation failed" message={errorMessage}
      />
    </div> : <div/>
}
