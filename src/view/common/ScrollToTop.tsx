import {Component} from 'react'
import {
  RouteComponentProps,
  withRouter
} from 'react-router'


class ScrollToTop extends Component<RouteComponentProps> {
  componentDidUpdate(prevProps: Readonly<RouteComponentProps>): void {
    if (this.props.location.hash === '' && this.props.location !== prevProps.location) {
      window.scrollTo(0, 0)
    } else if (this.props.location.hash !== '') {
      const id = this.props.location.hash.replace('#', '')
      const element = document.getElementById(id)
      if (element) {
        element.scrollIntoView()
      }
    }
  }

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  render() {
    return this.props.children
  }
}

export default withRouter(ScrollToTop)
