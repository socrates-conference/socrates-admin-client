import React from 'react'
import type {Action} from '../TableTypes'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

export type TableActionButtonProps = {
  action: Action
  data: any
}

export default function RowActionButton(props: TableActionButtonProps): JSX.Element {
  const buttonClass = 'btn btn-sm ' + props.action.buttonClass
  const executeActionIfAllowed = (): void => {
    if (props.action.canExecute(props.data)) {
      props.action.execute(props.data)
    }
  }
  return (
    <button
      className={buttonClass}
      onClick={executeActionIfAllowed}
      disabled={!props.action.canExecute(props.data)}>
      <FontAwesomeIcon icon={props.action.icon} />
      <span className="sr-only">{props.action.text}</span>
    </button>
  )
}

