import type {Action} from '../TableTypes'

export const executeActionIfAllowed = (action: Action, items: Array<Object>): void => {
  if (action.canExecute(items)) {
    action.execute(items)
  }
}
