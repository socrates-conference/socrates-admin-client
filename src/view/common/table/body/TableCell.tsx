import React from 'react'

export type TableCellProps = {
  content: string
  displayClass?: string
  format?: (content:any) => string
  cellTemplate?: (content:any) => JSX.Element
}


export default function TableCell(props: TableCellProps): JSX.Element {
  const renderContent = (): string|JSX.Element=> {
    if(props.cellTemplate) {
      return props.cellTemplate(props.content)
    } else if (props.format) {
      return props.format(props.content)
    } else {
      return props.content
    }
  }
  return (
    <td className={props.displayClass}>
      {renderContent()}
    </td>
  )
}
