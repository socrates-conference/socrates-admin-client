import React from 'react'
import {shallow} from 'enzyme'
import TableBody from './TableBody'
import {Action} from '../TableTypes'

describe('(Component) TableBody', () => {
  const columns = [{header: 'Name', field: 'name', isSortable: true, isFilterable: true}]
  const data = [{name: 'Name1'}, {name: 'Name2'}]
  const actions: Action[] = [{
    icon: 'react',
    buttonClass: '',
    text: '',
    canExecute: () => false,
    execute: () => {
    }
  }]
  const wrapper = shallow(<TableBody rowActions={actions} data={data} columns={columns}/>)

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
