import React from 'react'
import {shallow} from 'enzyme'
import TableRow from './TableRow'
import {Action} from '../TableTypes'

describe('(Component) TableRow', () => {
  const columns = [{header: 'Name', field: 'name', isSortable: true, isFilterable: true}]
  const content = {name: 'Name1'}
  const actions: Action[] = [{
    icon: 'react',
    buttonClass: '',
    text: '', canExecute: ()=>false, execute: ()=>{}
  }]
  const wrapper = shallow(
    <TableRow rowActions={actions} content={content} columns={columns}/>
  )

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
