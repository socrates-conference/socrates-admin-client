import React from 'react'

export default function EmailCellTemplate(props: { value: boolean }): JSX.Element {
  if (props.value) {
    return <div>
      <div className="text-success">yes</div>
    </div>
  } else {
    return <div>
      <div className="text-danger">no</div>
    </div>
  }
}
