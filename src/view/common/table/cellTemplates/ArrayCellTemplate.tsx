import React from 'react'
import './arrayCellTemplate.css'

export default function ArrayCellTemplate (props: {displayName: string, values: Array<any>}): JSX.Element {
  return (
    <div>
      {
        props.values.map((item, index) =>
          <div key={'arrayItem-' + index.toString()} className="room-type-bubble d-md-inline-flex">
            {item}
          </div>
        )
      }
    </div>
  )
}
