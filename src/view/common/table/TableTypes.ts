import { IconProp } from '@fortawesome/fontawesome-svg-core'
import {Address} from '../../participantList/commons'

export type ColumnStatus = { header: string, visible: boolean }
export type ActionStatus = { text: string, visible: boolean }

export type SortableRow<T =string> = Record<string,T>

export type ColumnDefinition = {
  header: string
  displayClass?: string
  field: string
  format?: (item: any) => string
  cellTemplate?: (item: any) => JSX.Element
  isSortable?: boolean
  isFilterable?: boolean
  sort?: (items: Array<SortableRow<string|number|boolean|Address>>, arg1: string, arg2: string) => void
  filter?: (items: Array<SortableRow<string|number|boolean|Address>>, arg1: string, arg2: string) => Array<SortableRow<string|number|boolean|Address>>
  visible?: boolean
}

export type Action = {
  canExecute: (params: any) => boolean
  execute: (params: any) => void
  icon: IconProp
  buttonClass: string
  text: string
  visible?: boolean
}

export type SortOptions = {
  isSortable: boolean
  isSortColumn: boolean
  onSort: (one: string, two: string) => void
}

export type TableFilterItem = {
  columnName: string
  value: string
}

