import React, {
  Component,
  SyntheticEvent
} from 'react'
import {Optional} from '../../../../types'

export type TableFilterCellProps = {
  columnName: string
  displayClass: Optional<string>
  onChange: (columname: string, targetValue: string) => void
}

type TableFilterCellState = {
  value: string
}
export default class TableFilterCell extends Component<TableFilterCellProps, TableFilterCellState> {
  state: TableFilterCellState = {
    value: ''
  }

  _onChange = (e: SyntheticEvent<HTMLInputElement>): void => {
    this.setState({value: e.currentTarget.value})
    this.props.onChange(this.props.columnName, e.currentTarget.value)
  }

  render = (): JSX.Element => {
    return (<th className={this.props.displayClass || ''}>
      <div>
        <input
                type="text" className="form-control form-control-sm"
                onChange={this._onChange} value={this.state.value}
        />
      </div>
    </th>)
  }
}

