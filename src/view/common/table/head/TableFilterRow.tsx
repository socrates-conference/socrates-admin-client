import React, {Component} from 'react'
import type {
  ColumnDefinition,
  TableFilterItem
} from '../TableTypes'
import TableFilterCell from './TableFilterCell'

type TableFilterRowState = {
  filter: Array<TableFilterItem>
  filterChanged: boolean
  intervalId: number
  timeoutId?: NodeJS.Timeout
}

type TableFilterRowProps = {
  columns: Array<ColumnDefinition>
  hasActions: boolean
  onFilter: (items: Array<TableFilterItem>) => void

}

export default class TableFilterRow extends Component<TableFilterRowProps, TableFilterRowState> {
  state: TableFilterRowState = {
    filter: [],
    intervalId: 0,
    timeoutId: undefined,
    filterChanged: false
  }

  componentWillUnmount = (): void => {
    if (this.state.timeoutId !== undefined) {
      clearInterval(this.state.timeoutId)
    }
    this.setState({timeoutId: undefined})
  }

  _filter = (): void => {
    this.props.onFilter(this.state.filter)
  }
  _onChange = (columnName: string, value: string): void => {
    let updateState = false
    const newFilter = this.state.filter.slice(0)
    const index = this.state.filter.findIndex((i) => i.columnName === columnName)
    if (index >= 0) {
      newFilter.splice(index, 1)
      updateState = true
    }
    if (value.trim().length > 0) {
      newFilter.push({columnName, value: value.trim()})
      updateState = true
    }
    if (updateState) {
      this._resetTimer(newFilter)
    }
  }

  _resetTimer = (filter: Array<TableFilterItem>): void => {
    if (this.state.timeoutId !== undefined) {
      clearTimeout(this.state.timeoutId)
    }

    const timeoutId: NodeJS.Timeout = setTimeout(this._filter, 500)
    this.setState({filter, timeoutId})
  }

  render = (): JSX.Element => {
    return (
      <tr>
        {this.props.columns.map((column, index) =>
          column.visible ?
            column.isFilterable ?
              <TableFilterCell
                                              key={'filter' + index}
                                              columnName={column.header}
                                              displayClass={column.displayClass}
                                              onChange={this._onChange}
                                      /> : <th key={'filter' + index}/> : null
        )}
        {this.props.hasActions ? <th key="filterAction"/> : null}
      </tr>
    )
  }
}

