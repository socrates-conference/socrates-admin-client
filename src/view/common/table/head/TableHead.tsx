import React from 'react'
import TableHeaderRow, {sortHandler} from './TableHeaderRow'
import TableFilterRow from './TableFilterRow'
import type {
  ColumnDefinition,
  TableFilterItem
} from '../TableTypes'

export type TableHeadProps = {
  columns: Array<ColumnDefinition>
  hasActions: boolean
  onSort: sortHandler
  onFilter: (items: Array<TableFilterItem>) => void
}

export default function TableHead(props: TableHeadProps): JSX.Element {

  const hasFilterableColumns = props.columns.some(column => column.isFilterable)
  return (
    <thead className="thead-dark">
      <TableHeaderRow columns={props.columns} onSort={props.onSort} hasActions={props.hasActions}/>
      {hasFilterableColumns
        ? <TableFilterRow columns={props.columns} onFilter={props.onFilter} hasActions={props.hasActions}/>
        : null}
    </thead>
  )
}

