import React from 'react'
import {mount, ReactWrapper} from 'enzyme'
import {SinonSpy, spy} from 'sinon'
import ErrorModal from './ErrorModal'

describe('(Component) ErrorModal', () => {
  let wrapper: ReactWrapper
  let closeSpy: SinonSpy
  describe('renders', () => {
    beforeEach(() => {
      closeSpy = spy()
      wrapper = mount(
        <ErrorModal
          show={true} closeModal={closeSpy}
          title="Test Title" text="Test text" message="Error"
        />
      )
    })

    it('without exploding', () => {
      expect(wrapper).toHaveLength(1)
    })
    it('renders correctly', () => {
      expect(wrapper).toMatchSnapshot()
    })
    it('closes on ok', () => {
      wrapper.find('#modal-button-Ok').simulate('submit')
      expect(closeSpy.calledOnce).toBe(true)
    })
  })
})
