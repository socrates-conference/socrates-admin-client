import React from 'react'
import {mount, ReactWrapper} from 'enzyme'
import AreYouSureModal from './AreYouSureModal'
import {emptyApplicationListItem} from '../../applicantList/commons'
import {SinonSpy, spy} from 'sinon'

describe('(Component) AreYouSureModal', () => {
  let wrapper: ReactWrapper
  let closeSpy: SinonSpy
  let executeSpy: SinonSpy
  describe('renders', () => {
    beforeEach(() => {
      closeSpy = spy()
      executeSpy = spy()
      wrapper = mount(
        <AreYouSureModal
          id="areYouSureModal" data={emptyApplicationListItem} commandId="delete"
          show={true}
          closeModal={closeSpy} title="Test title" text="Test text"
          executeAction={executeSpy}
        />)
    })

    it('without exploding', () => {
      expect(wrapper).toHaveLength(1)
    })
    it('renders correctly', () => {
      expect(wrapper).toMatchSnapshot()
    })
    it('executes action on ok', () => {
      wrapper.find('#modal-button-Ok').simulate('submit')
      expect(executeSpy.withArgs(emptyApplicationListItem, 'delete').calledOnce).toBe(true)
    })
    it('closes on cancel', () => {
      wrapper.find('#modal-button-Cancel').simulate('click')
      expect(closeSpy.calledOnce).toBe(true)
    })
  })
})
