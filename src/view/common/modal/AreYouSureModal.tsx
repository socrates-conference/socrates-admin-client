import React from 'react'
import type {ModalButton} from './Modal'
import Modal from './Modal'

type StateFromProps = {
  commandId: string
  data: any
  title: string
  text: string
  show: boolean
}

type DispatchFromProps = {
  closeModal: () => void
  executeAction: (item: any, action: string) => void
}

type Props = StateFromProps & DispatchFromProps & { id: string }

export default function AreYouSureModal(props: Props): JSX.Element {

  const buttons: Array<ModalButton> = [
    {
      isSubmit: true, onClick: () => {
        props.executeAction(props.data, props.commandId)
      }, text: 'Ok'
    },
    {
      isSubmit: false, onClick: () => {
        props.closeModal()
      }, text: 'Cancel'
    }
  ]

  return (<Modal show={props.show} buttons={buttons}>
    <div className="modal-header">
      <h3 className="mb-2 mx-auto">
        <strong>{props.title}</strong>
      </h3>
    </div>
    <div className="mb-2 mx-auto alert alert-info">
      <small>{props.text}</small>
    </div>
    <div className="form-row">
      <h3>Are you sure?</h3>
    </div>
  </Modal>)
}

