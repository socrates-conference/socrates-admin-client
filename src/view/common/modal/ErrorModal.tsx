import React from 'react'
import type {ModalButton} from './Modal'
import Modal from './Modal'

type Props = {
  closeModal: (value:any, msg:string, error:string) => void
  title: string
  text: string
  message: string
  show: boolean
}

export default function ErrorModal(props: Props): JSX.Element {
  const buttons: Array<ModalButton> = [
    {
      isSubmit: true, onClick: () => {
        props.closeModal({}, '', 'error')
      }, text: 'Ok'
    }
  ]

  return (
    <Modal show={props.show} buttons={buttons}>
      <div className="modal-header alert alert-danger">
        <h3>
          <strong>{props.title}</strong>
        </h3>
      </div>
      <div className="modal-body">
        <div><strong>{props.text}</strong></div>
        <div>message: {props.message}</div>
      </div>
    </Modal>
  )
}

