import React from 'react'
import {shallow} from 'enzyme'
import DropdownToggler from './DropdownToggler'

describe('(Component) Dropdown toggler', () => {
  const wrapper =
    shallow(<DropdownToggler target="target" url="#navigation" title="Title" icon="react"/>)

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
