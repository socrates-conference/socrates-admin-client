import React from 'react'
import {shallow} from 'enzyme'
import NavigationLink from './NavigationLink'

describe('(Component) Navigation link', () => {
  const wrapper = shallow(<NavigationLink url="/url" title="Title" icon="react"/>)

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
