import {NavLink} from 'react-router-dom'
import React from 'react'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import { IconProp } from '@fortawesome/fontawesome-svg-core'

type NavigationLinkProps = {
  url: string
  title: string
  icon: IconProp
}

export default function NavigationLink(props: NavigationLinkProps): JSX.Element {
  return (
    <li className="nav-item">
      <NavLink
        className="nav-link"
        to={props.url}
        title={props.title}
        data-toggle="collapse"
        data-target=".navbar-collapse.show">
        <FontAwesomeIcon icon={props.icon} />
        <span className="d-md-none d-lg-none d-xl-inline"> {props.title}</span>
      </NavLink>
    </li>
  )
}

