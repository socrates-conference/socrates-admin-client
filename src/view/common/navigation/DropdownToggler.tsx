import React from 'react'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import { IconProp } from '@fortawesome/fontawesome-svg-core'

type DropdownTogglerProps = {
  target: string
  url: string
  title: string
  icon: IconProp
}

export default function DropdownToggler(props: DropdownTogglerProps): JSX.Element {
  return (
    <a
      className="nav-link dropdown-toggle"
      href={props.url}
      id={props.target}
      data-toggle="dropdown"
      aria-haspopup="true"
      aria-expanded="false"
    >
      <FontAwesomeIcon icon={props.icon} />
      <span className="d-md-none d-lg-none d-xl-inline"> {props.title}</span>
    </a>
  )
}
