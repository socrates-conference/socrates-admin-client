import React from 'react'
import {connect} from 'react-redux'
import NavigationBrand from './NavigationBrand'
import NavigationLink from './NavigationLink'
import NavigationToggler from './NavigationToggler'
import DropdownItem from './DropdownItem'
import DropdownToggler from './DropdownToggler'
import {withRouter} from 'react-router'
import type {AuthenticationState} from '../../reducers/authenticationReducer'
import {logout} from '../../commands/authenticationCommand'

import '../Header.css'

type StateFromProps = {
  authentication: AuthenticationState
}
type DispatchFromProps = {
  logout: () => void
}
type Props = StateFromProps & DispatchFromProps

function Navigation(props: Props): JSX.Element {
  const isLoggedIn = props.authentication.token.trim() !== ''
  const collapsiblePanelId = 'navbarSupportedContent'
  const userDropdown = 'navbarDropdownMenuLink'
  return (
    <div id="navigation">
      <nav className="navbar navbar-expand-md navbar-dark bg-dark">
        <NavigationBrand/>
        <NavigationToggler target={collapsiblePanelId}/>
        <div className="collapse navbar-collapse" id={collapsiblePanelId}>
          <ul className="navbar-nav mr-auto">
            <NavigationLink url="/applicants" title="Applicants" icon="users"/>
            <NavigationLink url="/participants" title="Participants" icon="users"/>
            <NavigationLink url="/lottery" title="Lottery" icon="ticket-alt"/>
            <NavigationLink url="/name-tags" title="Name Tags" icon="user-tag"/>
            <NavigationLink url="/imprint" title="Imprint" icon="calendar-check"/>
          </ul>
          <div className="navbar-nav navbar-item mr-3 dropdown">
            <DropdownToggler
              target={userDropdown}
              url="#navigation"
              title={props.authentication.userName}
              icon="user"
            />
            <div className="dropdown-menu bg-dark dropdown-menu-right" aria-labelledby={userDropdown}>
              <DropdownItem visible={!isLoggedIn} url="/login" title="Login" icon="sign-in-alt" onClick={()=>{}}/>
              <DropdownItem
                visible={isLoggedIn} url="#navigation" title="Logout" icon="sign-out-alt"
                onClick={props.logout}/>
            </div>
          </div>
        </div>
      </nav>
    </div>
  )
}

const mapStateToProps = (state:any): StateFromProps => {
  return {authentication: {...state.authentication}}
}

const mapDispatchToProps = {
  logout
}

export default withRouter(connect<StateFromProps, DispatchFromProps>(mapStateToProps, mapDispatchToProps)(Navigation))
