import {Link} from 'react-router-dom'
import React from 'react'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import { IconProp } from '@fortawesome/fontawesome-svg-core'

type DropdownItemProps = {
  visible: boolean
  url: string
  title: string
  icon: IconProp
  onClick: () => void
}

export default function DropdownItem(props: DropdownItemProps): JSX.Element {
  const linkClass = 'nav-link' + (props.visible ? ' d-block' : ' d-none')
  return (
    <Link
      className={linkClass}
      to={props.url}
      title={props.title}
      data-toggle="collapse"
      data-target=".navbar-collapse.show"
      onClick={props.onClick ? props.onClick : () => {}}
    >
      <FontAwesomeIcon icon={props.icon} />
      <span> {props.title}</span>
    </Link>
  )
}

