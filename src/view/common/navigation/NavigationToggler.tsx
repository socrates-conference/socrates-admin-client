import React from 'react'

type NavigationTogglerProps = {
  target: string
}

export default function NavigationToggler(props: NavigationTogglerProps): JSX.Element {
  return (
    <button
      className="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target={'#' + props.target}
      aria-controls={props.target}
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span className="navbar-toggler-icon">&nbsp;</span>
      <span className="sr-only">Toggle navigation</span>
    </button>
  )
}
