import React, { SyntheticEvent } from 'react'

export type SelectorItem = {label: string, value: string | number}
export type Props = {
  defaultValue: string | number
  id?: string
  label: string
  selectable: Array<SelectorItem>
  selectionChanged: (value:string) => void
}

export default function Selector (props: Props): JSX.Element {

  return (
    <div id={props.id ? props.id : 'selector'} className="row mb-2">
      <div className="col-lg-4 col-md-12 align-self-center">
        <label htmlFor="select-room-type">{props.label}</label>
      </div>
      <div className="col-lg-8 col-md-12">
        <select
          id={props.id ? `${props.id}-select` : 'select'} className="custom-select"
          onChange={(e: SyntheticEvent<HTMLSelectElement>) => props.selectionChanged(e.currentTarget.value)}
          defaultValue={props.defaultValue}>
          {
            props.selectable && props.selectable.map((item) => {
              return (
                <option
                  key={props.id ? `${props.id}-option-${item.value}` : `option-${item.value}`}
                  value={item.value}>
                  {item.label}
                </option>
              )
            })
          }
        </select>
      </div>
    </div>
  )
}

