import React from 'react'
import {shallow} from 'enzyme'
import Selector from './Selector'
import {spy} from 'sinon'

describe('(Component) Selector', () => {
  const spyChanges = spy()
  const wrapper = shallow(
    <Selector
      id="my-selector"
      label="select something"
      selectionChanged={spyChanges}
      defaultValue="1"
      selectable={[{label: 'label 1', value: 1}, {label: 'label 2', value: 'value 2'}]}
    />)

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })

  it('reacts to changes', () => {
    wrapper.find('select').simulate('change', {currentTarget: {value: 'value 2'}})
    expect(spyChanges.calledOnce).toBe(true)
  })
})
