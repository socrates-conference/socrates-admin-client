import React from 'react'
import './pagination.css'

type Props = {
  currentItemsPerPage: number
  itemsPerPageChanged: (itemsCount: number) => void
  availableItemsPerPageSelections: Array<number>
}

type State = {
  isOpen: boolean
}

export default class PaginationItemsPerPageSelector extends React.Component<Props, State> {

  state: State = {
    isOpen: false
  }

  _toggle = (): void => {
    this.setState({isOpen: !this.state.isOpen})
  }

  _selected = (length: number): void => {
    this.setState({isOpen: false})
    this.props.itemsPerPageChanged(length)
  }

  render = (): JSX.Element => {
    return (<div className="row justify-content-center text-center mb-2">
      <div id="itemsPerPageDropdown" className="dropdown">
        <button className="btn btn-sm btn-secondary dropdown" type="button" id="paginationLengthButton"
                onClick={this._toggle}>
          Page length: {this.props.currentItemsPerPage}
        </button>
        {
          this.state.isOpen
            ? (<div className="dropdown-menu show">
              {
                this.props.availableItemsPerPageSelections
                  .map((length) => <button
                                id={'length-' + length}
                                key={'length-' + length} className="dropdown-item"
                                onClick={() => this._selected(length)}>{length}</button>)
              }
            </div>)
            : <div />
        }
      </div>
    </div>)
  }
}
