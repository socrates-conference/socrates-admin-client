import React from 'react'
import PaginationItemsPerPageSelector from './PaginationItemsPerPageSelector'
import Paginator from './Paginator'

type Props = {
  currentPage: number
  currentItemsPerPage: number
  lastPage: number
  moveToPage: (page: number) => void
  itemsPerPageChanged: (itemsCount: number) => void
  availableItemsPerPageSelections: Array<number>
}

export default function PaginationContainer(props: Props): JSX.Element {
  return (<div id="pagination" className="container">
    <PaginationItemsPerPageSelector
            currentItemsPerPage={props.currentItemsPerPage}
            itemsPerPageChanged={props.itemsPerPageChanged}
            availableItemsPerPageSelections={props.availableItemsPerPageSelections}/>
    <Paginator
            currentPage={props.currentPage}
            currentItemsPerPage={props.currentItemsPerPage}
            lastPage={props.lastPage}
            moveToPage={props.moveToPage}/>
  </div>)
}
