import React from 'react'

type Props = {
  currentPage: number
  currentItemsPerPage: number
  lastPage: number
  moveToPage: (page: number) => void
}

export default function Paginator(props: Props): JSX.Element {
  const previousDisabled = (): boolean => {
    return props.currentPage <= 1
  }

  const nextDisabled = (): boolean => {
    return props.currentPage >= props.lastPage
  }

  return (<div className="row justify-content-center text-center">
    <nav aria-label="Page navigation example">
      <ul className="pagination">
        <li className="page-item">
          <button id="first"
                  className="page-btn-link" onClick={() => props.moveToPage(1)} disabled={previousDisabled()}>
            &lt;&lt;
          </button>
        </li>
        <li className="page-item">
          <button id="previous"
                  className="page-btn-link" onClick={() => props.moveToPage(props.currentPage - 1)}
                  disabled={previousDisabled()}
          >
            &lt;
          </button>
        </li>
        <li className="page-item">
          <button className="page-text-field" disabled>Page {props.currentPage}</button>
        </li>
        <li className="page-item">
          <button id="next"
                  className="page-btn-link" onClick={() => props.moveToPage(props.currentPage + 1)}
                  disabled={nextDisabled()}
          >
            &gt;
          </button>
        </li>
        <li className="page-item">
          <button id="last"
                  className="page-btn-link" onClick={() => props.moveToPage(props.lastPage)} disabled={nextDisabled()}>
            &gt;&gt;
          </button>
        </li>
      </ul>
    </nav>
  </div>)
}

