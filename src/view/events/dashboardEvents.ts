import {Action} from 'redux'

export default class DashboardEvents {
  static get GET_REGISTRATIONS_BY_ROOM_TYPE_SUCCEEDED(): string {
    return 'DashboardEvents/GET_REGISTRATIONS_BY_ROOM_TYPE_SUCCEEDED'
  }
  static get GET_REGISTRATIONS_BY_ROOM_TYPE_FAILED(): string {
    return 'DashboardEvents/GET_REGISTRATIONS_BY_ROOM_TYPE_FAILED'
  }
}

type Event = Action
type ErrorEvent = Event & { error: any }
type RegistrationByRoomTypeSucceededEvent = Event & {occupationRows: any[], diversityRows: any[]}

export const getRegistrationsByRoomTypeSucceeded =
  (occupationRows: Array<any>, diversityRows: Array<any>): RegistrationByRoomTypeSucceededEvent =>
    ({type: DashboardEvents.GET_REGISTRATIONS_BY_ROOM_TYPE_SUCCEEDED, occupationRows, diversityRows})
export const getRegistrationsByRoomTypeFailed =
  (error: any): ErrorEvent => ({type: DashboardEvents.GET_REGISTRATIONS_BY_ROOM_TYPE_FAILED, error})
