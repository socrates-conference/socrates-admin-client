import {Action} from 'redux'

export default class AuthenticationEvent {

  static get LOGIN_STARTED(): string {
    return 'Event/LOGIN_STARTED'
  }
  static get LOGIN_SUCCESS(): string {
    return 'Event/LOGIN_SUCCESS'
  }
  static get LOGIN_ERROR(): string {
    return 'Event/LOGIN_ERROR'
  }
  static get LOGOUT_SUCCESS(): string {
    return 'Event/LOGOUT_SUCCESS'
  }
  static get UPDATE_COMES_FROM_RECEIVED(): string {
    return 'Event/UPDATE_COMES_FROM_RECEIVED'
  }
}

type Event = Action
type LoginSuccessEvent = Event & { token: string, data: any}
type UpdateComesFromReceivedEvent = Event & { url: string }
export const loginStarted = (): Event => ({type: AuthenticationEvent.LOGIN_STARTED})
export const logoutSuccess = (): Event => ({type: AuthenticationEvent.LOGOUT_SUCCESS})
export const loginSuccess =
  (token: string, data: any): LoginSuccessEvent => ({type: AuthenticationEvent.LOGIN_SUCCESS, token, data})
export const loginError = (): Event => ({type: AuthenticationEvent.LOGIN_ERROR})
export const updateComesFromReceived =
  (url: string): UpdateComesFromReceivedEvent => ({type: AuthenticationEvent.UPDATE_COMES_FROM_RECEIVED, url})
