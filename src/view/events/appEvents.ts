import type {Conference} from '../reducers/appReducer'
import {Action} from 'redux'

export default class AppEvents {
  static get GET_CONFERENCE_SUCCEEDED(): string {
    return 'AppEvents/GET_CONFERENCE_SUCCEEDED'
  }

  static get GET_CONFERENCE_FAILED(): string {
    return 'AppEvents/GET_CONFERENCE_FAILED'
  }
}

type Event = Action
type ErrorEvent = Event & { error: any }
export const getConferenceSucceeded =
  (conference: Conference): Event & { conference: Conference } => ({
    type: AppEvents.GET_CONFERENCE_SUCCEEDED,
    conference
  })
export const getConferenceFailed = (error: any): ErrorEvent => ({type: AppEvents.GET_CONFERENCE_FAILED, error})
