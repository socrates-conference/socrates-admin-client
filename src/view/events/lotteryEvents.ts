import { Action } from 'redux'
import type {DrawItem} from '../reducers/lotteryReducer'

export default class LotteryEvents {
  static get READ_INFO_SUCCEEDED(): string { return 'LotteryEvents/READ_INFO_SUCCEEDED' }
  static get READ_INFO_FAILED(): string { return 'LotteryEvents/READ_INFO_FAILED' }
  static get START_LOTTERY_DRAW_FAILED(): string { return 'LotteryEvents/START_LOTTERY_DRAW_FAILED' }
  static get DISTRIBUTE_SLOTS_SUCCEEDED(): string { return 'LotteryEvents/DISTRIBUTE_SLOTS_SUCCEEDED' }
  static get HEALTH_CHECK_SUCCEEDED(): string { return 'LotteryEvents/HEALTH_CHECK_SUCCEEDED' }
  static get HEALTH_CHECK_FAILED(): string { return 'LotteryEvents/HEALTH_CHECK_FAILED' }
  static get REGISTRATION_STARTED(): string { return 'LotteryEvents/REGISTRATION_STARTED' }
  static get REGISTERED(): string { return 'LotteryEvents/REGISTERED' }
  static get REGISTRATION_DONE(): string { return 'LotteryEvents/REGISTRATION_DONE' }
}

type Event = Action
type SuccessEvent = Event & {rows?: any[], draw?: DrawItem[]}
type ErrorEvent = Event & {error?: any, errors?: string[]}
type RegistrationDoneEvent = Event & {results: any[]}
type RegisteredEvent = Event & {text:string, status: boolean}

export const readInfoSucceeded = (rows: Array<any>): SuccessEvent => ({type: LotteryEvents.READ_INFO_SUCCEEDED, rows})
export const readInfoFailed = (error: Error): ErrorEvent => ({type: LotteryEvents.READ_INFO_FAILED, error})
export const startLotteryDrawFailed =
  (error: any): ErrorEvent => ({type: LotteryEvents.START_LOTTERY_DRAW_FAILED, error})
export const distributeSlotsSucceeded =
  (draw: Array<DrawItem>): SuccessEvent => ({type: LotteryEvents.DISTRIBUTE_SLOTS_SUCCEEDED, draw})

export const distributionHealthCheckSucceeded =
  (): SuccessEvent => ({type: LotteryEvents.HEALTH_CHECK_SUCCEEDED})
export const distributionHealthCheckFailed =
  (errors: Array<string>): ErrorEvent => ({type: LotteryEvents.HEALTH_CHECK_FAILED, errors})

export const registrationDone =
  (results: Array<any>): RegistrationDoneEvent => ({type: LotteryEvents.REGISTRATION_DONE, results})
export const registrationStarted = (): Event => ({type: LotteryEvents.REGISTRATION_STARTED})
export const registered =
  (text: string, status: boolean): RegisteredEvent => ({type: LotteryEvents.REGISTERED, text, status})
