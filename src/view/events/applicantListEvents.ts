import type {ApplicationListItem} from '../applicantList/commons'
import {Action} from 'redux'

export default class ApplicationListEvent {

  static get EXECUTE_SUCCEEDED(): string {
    return 'ApplicationListEvent/EXECUTE_SUCCEEDED'
  }
  static get EXECUTE_FAILED(): string {
    return 'ApplicationListEvent/EXECUTE_ERROR'
  }
  static get EXECUTE_CANCELED(): string {
    return 'ApplicationListEvent/EXECUTE_CANCELED'
  }
  static get GET_ROOM_TYPES_SUCCEEDED(): string {
    return 'ApplicationListEvent/GET_ROOM_TYPES_SUCCEEDED'
  }
  static get GET_ROOM_TYPES_FAILED(): string {
    return 'ApplicationListEvent/GET_ROOM_TYPES_FAILED'
  }
  static get GET_APPLICATIONS_SUCCEEDED(): string {
    return 'ApplicationListEvent/GET_APPLICATIONS_SUCCEEDED'
  }
  static get GET_APPLICATIONS_FAILED(): string {
    return 'ApplicationListEvent/GET_APPLICATIONS_FAILED'
  }
  static get UPDATE_ROOM_TYPE_FILTER(): string {
    return 'ApplicationListEvent/UPDATE_ROOM_TYPE_FILTER'
  }
  static get HANDLED_ACTION(): string {
    return 'ApplicationListEvent/HANDLED_ACTION'
  }
}

type Event = Action
type ErrorEvent = Event & {error: any }
type SuccessEvent = Event & { rows : any[]}
type RoomTypeFilterUpdatedEvent = Event & {filter: string}
type HandledActionEvent = Event & {commandId: string, data: ApplicationListItem}

export const applicationListExecutionSucceed = (): Event => ({type: ApplicationListEvent.EXECUTE_SUCCEEDED})
export const applicationListExecutionFailed = (error: Error): ErrorEvent => ({type: ApplicationListEvent.EXECUTE_FAILED, error})
export const applicationListExecutionCanceled = (): Event => ({type: ApplicationListEvent.EXECUTE_CANCELED})
export const getRoomTypesSucceed =
  (rows: Array<any>): SuccessEvent => ({type: ApplicationListEvent.GET_ROOM_TYPES_SUCCEEDED, rows})
export const getRoomTypesFailed =
  (error: any): ErrorEvent => ({type: ApplicationListEvent.GET_ROOM_TYPES_FAILED, error})
export const getApplicationsSucceed =
  (rows: Array<any>): SuccessEvent => ({type: ApplicationListEvent.GET_APPLICATIONS_SUCCEEDED, rows})
export const getApplicationsFailed =
  (error: any): ErrorEvent => ({type: ApplicationListEvent.GET_APPLICATIONS_FAILED, error})
export const updateRoomTypeFilter =
  (filter: string): RoomTypeFilterUpdatedEvent => ({type: ApplicationListEvent.UPDATE_ROOM_TYPE_FILTER, filter})
export const handledAction =
  (commandId: string, data: ApplicationListItem): HandledActionEvent =>
    ({type: ApplicationListEvent.HANDLED_ACTION, commandId, data})
