import React from 'react'
import {Route} from 'react-router-dom'
import {connect} from 'react-redux'
import {
  Redirect,
  RouteComponentProps,
  RouteProps,
  withRouter
} from 'react-router'
import {AuthenticationState} from './reducers/authenticationReducer'

type Props = RouteComponentProps & RouteProps & {
  component?: React.Component
  children?: JSX.Element | JSX.Element[]
  state?: AuthenticationState
  location: Location
}
// eslint-disable-next-line no-shadow
const PrivateRoute = ({component, children, state, location, ...rest}: Props): JSX.Element => {
  const shouldShowComponent = (): boolean =>
    state !== undefined && state.token.trim().length > 0 && state.isAdministrator
  return (<Route {...rest} render={
    (props) => (shouldShowComponent()
      ? component ? <React.Component {...props} /> : children
      : <Redirect to={{
        pathname: '/login',
        state: {from: location.pathname}
      }}/>)}/>)
}

const mapStateToProps = (state: any): any => {
  return {state: {...state.authentication}}
}

export default withRouter(connect(mapStateToProps, null, null, {pure: false})(PrivateRoute))
