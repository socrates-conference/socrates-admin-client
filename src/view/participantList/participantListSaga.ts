import {
  getParticipantsFailed,
  getParticipantsSucceeded,
  handledAction,
  listTypeChanged,
  participantListExecutionCanceled,
  participantListExecutionFailed,
  participantListExecutionSucceed,
  getRoomTypesSucceeded,
  getRoomTypesFailed
} from './participantListEvents'
import {call, put} from 'redux-saga/effects'
import * as api from '../requests/api'
import {getParticipants} from './participantListCommand'
import {participantListCommandIdToApiCall} from '../sagas/resolveApiCallFromCommand'
import {
  prepareColumns,
  prepareRowActions,
  prepareTableActions,
  toParticipantListItemArray
} from './participantListSagaHelper'
import type {ParticipantListItem} from './commons'
import {CommandAction} from '../commands/applicationListCommand'

export function* participantListListTypeChangeSaga(action: any): Iterable<any> {
  yield put(
    listTypeChanged(
      action.newValue,
      prepareColumns(action.newValue),
      prepareRowActions(action.newValue),
      prepareTableActions(action.newValue))
  )
}

export function* participantListHandleActionSaga(action: CommandAction): Iterable<any> {
  yield put(handledAction(action.commandId, action.data))
}

export function* participantListCloseModalSaga(): Iterable<any> {
  yield put(participantListExecutionCanceled())
}

export function* participantListExecuteSaga(action: CommandAction): Iterable<any> {
  const apiCall = participantListCommandIdToApiCall(action.commandId)
  try {
    const success = yield call(apiCall, action.data)
    if (success) {
      yield put(participantListExecutionSucceed())
      yield put(getParticipants())
    } else {
      yield put(
        participantListExecutionFailed(new Error(`cannot execute command ${action.commandId}`)))
    }
  } catch (error) {
    yield put(participantListExecutionFailed(error))
  }
}

export function* participantListGetParticipants(): Iterable<any> {
  try {
    const rawData: Array<Object>|undefined = yield call(api.readParticipants)
    const rows: Array<ParticipantListItem> = toParticipantListItemArray(rawData)
    yield put(getParticipantsSucceeded(rows || []))
  } catch (error) {
    yield put(getParticipantsFailed(error))
  }
}

export function* participantListGetRoomTypes(): Iterable<any> {
  try {
    const options: Array<Object>|undefined = yield call(api.readOptions)
    if (options) {
      yield put(getRoomTypesSucceeded(options))
    }
  } catch (error) {
    yield put(getRoomTypesFailed(error))
  }
}
