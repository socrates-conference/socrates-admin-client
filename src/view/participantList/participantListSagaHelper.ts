import type {ActionStatus, ColumnStatus} from '../common/table/TableTypes'
import type {
  Address,
  ParticipantListItem
} from './commons'
import {arrivalDays, departureDays, shirtSizes} from './commons'
import {LabeledItem} from '../../types'

export const defaultColumnsStatus = [
  {header: 'Name', visible: true},
  {header: 'Email', visible: true},
  {header: 'Confirmed', visible: true},
  {header: 'Deadline', visible: true},
  {header: '#Reminders', visible: true},
  {header: 'Last Reminder', visible: true},
  {header: 'Room type', visible: false},
  {header: 'Diversity', visible: false},
  {header: 'First name', visible: false},
  {header: 'Last name', visible: false},
  {header: 'Address', visible: false},
  {header: 'Arrival', visible: false},
  {header: 'Departure', visible: false},
  {header: 'Dietary', visible: false},
  {header: 'Family', visible: false},
  {header: 'T-Shirt', visible: false},
  {header: 'Tag', visible: false},
  {header: 'Social', visible: false},
  {header: 'Pronoun', visible: false}

]

export const defaultRowActionsStatus = [
  {text: 'Delete', visible: true},
  {text: 'Change room type', visible: false},
  {text: 'Send confirmation reminder', visible: true}
]

export const defaultTableActionsStatus = [
  {text: 'Refresh', visible: true},
  {text: 'Confirm reminder email', visible: false}
]

function prepareColumnsForConfirmations(): Array<ColumnStatus> {
  return defaultColumnsStatus.map(column => {
    column.visible = column.header === 'Name' || column.header === 'Email' || column.header === 'Confirmed'
      || column.header === 'Deadline' || column.header === '#Reminders' || column.header === 'Last Reminder'
    return column
  })
}

function prepareColumnsForDetails(): Array<ColumnStatus> {
  return defaultColumnsStatus.map(column => {
    column.visible = column.header === 'Name' || column.header === 'Email' || column.header === 'Room type'
      || column.header === 'Diversity' || column.header === 'Arrival' || column.header === 'Departure'
    return column
  })
}

function prepareColumnsForAddresses(): Array<ColumnStatus> {
  return defaultColumnsStatus.map(column => {
    column.visible = column.header === 'Email'
      || column.header === 'First name' || column.header === 'Last name' || column.header === 'Address'
    return column
  })
}

function prepareColumnsForInformation(): Array<ColumnStatus> {
  return defaultColumnsStatus.map(column => {
    column.visible = column.header === 'Name' || column.header === 'Email'
      || column.header === 'Dietary' || column.header === 'Family' || column.header === 'T-Shirt'
    return column
  })
}

function prepareColumnsForTags(): Array<ColumnStatus> {
  return defaultColumnsStatus.map(column => {
    column.visible = column.header === 'Name' || column.header === 'Email'
      || column.header === 'Tag' || column.header === 'Social' || column.header === 'Pronoun'
    return column
  })
}

export function prepareColumns(listType: string): Array<ColumnStatus> {
  let columnsStatus: Array<ColumnStatus>
  switch (listType) {
    case '1':
      columnsStatus = prepareColumnsForConfirmations()
      break
    case '2':
      columnsStatus = prepareColumnsForDetails()
      break
    case '3':
      columnsStatus = prepareColumnsForAddresses()
      break
    case '4':
      columnsStatus = prepareColumnsForInformation()
      break
    case '5':
      columnsStatus = prepareColumnsForTags()
      break
    default:
      columnsStatus = defaultColumnsStatus.map(column => ({...column, visible: true}))
      break
  }
  return columnsStatus
}

function getLabel(list: Array<LabeledItem>, value: string): string {
  const item = list.find(d => d.value === value)
  return item ? item.label : ''
}

function getArrivalText(arrival: string):string {
  return getLabel(arrivalDays, arrival)
}

function getDepartureText(departure: string): string {
  return getLabel(departureDays, departure)
}

function getShirtText(shirt: string): string {
  return getLabel(shirtSizes, shirt)
}

function getAddressText({company, address1, address2, province, postal, city, country}: any): Address {
  return {
    company,
    address1,
    address2,
    province,
    postal,
    city,
    country
  }
}

export function toParticipantListItemArray(rawData?: Array<any>): Array<ParticipantListItem> {
  let theList: Array<ParticipantListItem> = []
  if (rawData !== undefined && rawData.length > 0) {
    theList = rawData.map((item) => ({
      ...item,
      arrival: getArrivalText(item.arrival),
      departure: getDepartureText(item.departure),
      shirt: getShirtText(item.shirt),
      address: getAddressText(item)
    }))
  }
  return theList
}

function prepareTableActionsForConfirmations(): Array<ActionStatus> {
  return defaultTableActionsStatus.map(action => {
    action.visible = action.text === 'Refresh'
    return action
  })
}

function prepareTableActionsForDetails(): Array<ActionStatus> {
  return defaultTableActionsStatus.map(action => {
    action.visible = action.text === 'Refresh'
    return action
  })
}

function prepareTableActionsForAddresses(): Array<ActionStatus> {
  return defaultTableActionsStatus.map(action => {
    action.visible = action.text === 'Refresh'
    return action
  })
}

function prepareTableActionsForInformation(): Array<ActionStatus> {
  return defaultTableActionsStatus.map(action => {
    action.visible = action.text === 'Refresh'
    return action
  })
}

function prepareTableActionsForTags(): Array<ActionStatus> {
  return defaultTableActionsStatus.map(action => {
    action.visible = action.text === 'Refresh'
    return action
  })
}

export function prepareTableActions(listType: string): Array<ActionStatus> {
  let actionsStatus: Array<ActionStatus>
  switch (listType) {
    case '1':
      actionsStatus = prepareTableActionsForConfirmations()
      break
    case '2':
      actionsStatus = prepareTableActionsForDetails()
      break
    case '3':
      actionsStatus = prepareTableActionsForAddresses()
      break
    case '4':
      actionsStatus = prepareTableActionsForInformation()
      break
    case '5':
      actionsStatus = prepareTableActionsForTags()
      break
    default:
      actionsStatus = defaultTableActionsStatus.map(action => ({...action, visible: true}))
      break
  }
  return actionsStatus
}

function prepareRowActionsForConfirmations(): Array<ActionStatus> {
  return defaultRowActionsStatus.map(action => {
    action.visible = action.text === 'Delete' || action.text === 'Send confirmation reminder'
    return action
  })
}

function prepareRowActionsForDetails(): Array<ActionStatus> {
  return defaultRowActionsStatus.map(action => {
    action.visible = action.text === 'Delete' || action.text === 'Change room type'
    return action
  })
}

function prepareRowActionsForOther(): Array<ActionStatus> {
  return defaultRowActionsStatus.map(action => {
    action.visible = action.text === 'Delete'
    return action
  })
}

export function prepareRowActions(listType: string): Array<ActionStatus> {
  let actionsStatus: Array<ActionStatus>
  switch (listType) {
    case '1':
      actionsStatus = prepareRowActionsForConfirmations()
      break
    case '2':
      actionsStatus = prepareRowActionsForDetails()
      break
    case '3':
    case '4':
    case '5':
      actionsStatus = prepareRowActionsForOther()
      break
    default:
      actionsStatus = defaultRowActionsStatus.map(action => ({...action, visible: true}))
      break
  }
  return actionsStatus
}

