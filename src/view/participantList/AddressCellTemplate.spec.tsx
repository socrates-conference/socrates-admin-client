import React from 'react'
import {
  shallow,
  ShallowWrapper
} from 'enzyme'
import {Address} from './commons'
import AddressCellTemplate from './AddressCellTemplate'


describe('(Component) ParticipantListModals', () => {
  let wrapper: ShallowWrapper
  beforeEach(() => {
    const address: Address = {address1: '', address2: '', city: '', company: '', country: '', postal: '', province: ''}
    wrapper = shallow(<AddressCellTemplate value={address}/>)
  })

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
