import {
  prepareColumns,
  prepareRowActions,
  prepareTableActions,
  toParticipantListItemArray
} from './participantListSagaHelper'
import {ColumnStatus} from '../common/table/TableTypes'

function findHeader(status: any, header: any): any {
  const theStatus = status.find((s: any) => s.header === header)
  if (theStatus) {
    return theStatus
  }
  throw new Error('Header not found: ' + header)
}

function findText(status: any, text: any): any {
  const theStatus = status.find((s: any) => s.text === text)
  if (theStatus) {
    return theStatus
  }
  throw new Error('Text not found: ' + text)
}

function expectHeadersVisible(status: Array<ColumnStatus>, ...headers: string[]): void {
  headers.forEach(header =>
    expect(findHeader(status, header).visible).toBe(true))
}

function expectHeadersInvisible(status: Array<ColumnStatus>, ...headers: string[]): void {
  headers.forEach(header =>
    expect(findHeader(status, header).visible).toBe(false))
}

describe('participant list saga helper:', () => {
  describe('(list type columns)', () => {
    it('confirmations list shows confirmation column', () => {
      const status = prepareColumns('1')

      expectHeadersVisible(status, 'Name', 'Email', 'Confirmed', 'Deadline', '#Reminders', 'Last Reminder')
      expectHeadersInvisible(status, 'Room type', 'Diversity', 'First name', 'Last name', 'Address', 'Arrival',
                             'Departure', 'Dietary', 'Family', 'T-Shirt', 'Tag', 'Social', 'Pronoun')
    })

    it('list type details shows room type, diversity, from and to columns', () => {
      const status = prepareColumns('2')
      expectHeadersVisible(status, 'Name', 'Email', 'Room type', 'Diversity', 'Arrival', 'Departure')
      expectHeadersInvisible(status, 'Confirmed', 'Deadline', '#Reminders', 'Last Reminder', 'First name',
                             'Last name', 'Address', 'Dietary', 'Family', 'T-Shirt', 'Tag', 'Social', 'Pronoun')
    })

    it('list type addresses shows address column', () => {
      const status = prepareColumns('3')
      expectHeadersVisible(status, 'Email', 'First name', 'Last name', 'Address')
      expectHeadersInvisible(status, 'Name', 'Confirmed', 'Deadline', '#Reminders', 'Last Reminder', 'Room type',
                             'Diversity', 'Arrival', 'Departure', 'Dietary', 'Family', 'T-Shirt', 'Tag', 'Social', 'Pronoun')
    })

    it('list type information shows dietary, family and t-shirt columns', () => {
      const status = prepareColumns('4')
      expectHeadersVisible(status,'Name','Email','Dietary','Family','T-Shirt' )
      expectHeadersInvisible(status, 'Confirmed','Deadline','#Reminders','Last Reminder','Room type'
        ,'Diversity','First name','Last name','Address','Arrival','Departure','Tag','Social','Pronoun')
    })

    it('list type tags shows tag label, social media and pronoun column', () => {
      const status = prepareColumns('5')
      expectHeadersVisible(status,'Name','Email','Tag','Social','Pronoun' )
      expectHeadersInvisible(status, 'Confirmed','Deadline','#Reminders','Last Reminder','Room type',
                             'Diversity','First name','Last name','Address','Arrival','Departure','Dietary','Family','T-Shirt')
    })

    it('unknown list type shows all columns', () => {
      const status = prepareColumns('4711')
      expectHeadersVisible(status, 'Name','Email','Confirmed','Deadline','#Reminders','Last Reminder',
                           'Room type','Diversity','First name','Last name','Address','Arrival','Departure','Dietary','Family',
                           'T-Shirt','Tag','Social','Pronoun')
    })
  })

  describe('(raw participants data)', () => {
    const rawData = [
      {
        participantId: 4,
        personId: 7,
        roomType: 'bedInDouble',
        diversityReason: 'no',
        date: '2018-05-18 02:42:01',
        isConfirmed: 1,
        name: 'Name',
        email: 'email@valid.de',
        firstName: 'first name',
        lastName: 'last name',
        company: 'company',
        address1: 'address1',
        address2: 'address2',
        province: 'province',
        postal: 'postal',
        city: 'city',
        country: 'country',
        arrival: '0',
        departure: '5',
        dietaryInfo: 'dietary info',
        familyInfo: 'family info',
        shirt: '12',
        labelname: 'label name',
        social: 'social',
        pronoun: 'pronoun'
      }
    ]
    it('can be converted to participant list item array', () => {
      const expectedAddress = {
        company: 'company',
        address1: 'address1',
        address2: 'address2',
        province: 'province',
        postal: 'postal',
        city: 'city',
        country: 'country'
      }
      const participants = toParticipantListItemArray(rawData)
      expect(participants).toBeDefined()
      expect(participants).toHaveLength(1)
      const participant = participants[0]
      expect(participant.arrival).toEqual('Thursday afternoon (with dinner)')
      expect(participant.departure).toEqual('Monday morning')
      expect(participant.shirt).toEqual('Unisex L')
      expect(participant.address).toEqual(expectedAddress)
    })
  })
  describe('prepare actions', () => {
    describe('(prepareTableActions)', () => {
      it('confirmations list shows confirmation column', () => {
        const status = prepareTableActions('1')
        expect(findText(status, 'Refresh').visible).toBe(true)
        expect(findText(status, 'Confirm reminder email').visible).toBe(false)
      })
      it('list type details shows room type, diversity, from and to columns', () => {
        const status = prepareTableActions('2')
        expect(findText(status, 'Refresh').visible).toBe(true)
        expect(findText(status, 'Confirm reminder email').visible).toBe(false)
      })
      it('list type addresses shows address column', () => {
        const status = prepareTableActions('3')
        expect(findText(status, 'Refresh').visible).toBe(true)
        expect(findText(status, 'Confirm reminder email').visible).toBe(false)
      })
      it('list type information shows dietary, family and t-shirt columns', () => {
        const status = prepareTableActions('4')
        expect(findText(status, 'Refresh').visible).toBe(true)
        expect(findText(status, 'Confirm reminder email').visible).toBe(false)
      })
      it('list type tags shows tag label, social media and pronoun column', () => {
        const status = prepareTableActions('5')
        expect(findText(status, 'Refresh').visible).toBe(true)
        expect(findText(status, 'Confirm reminder email').visible).toBe(false)
      })
      it('unknown list type shows all columns', () => {
        const status = prepareTableActions('4711')
        expect(findText(status, 'Refresh').visible).toBe(true)
        expect(findText(status, 'Confirm reminder email').visible).toBe(true)
      })
    })

    describe('(prepareRowActions)', () => {
      it('confirmations list shows confirmation column', () => {
        const status = prepareRowActions('1')
        expect(findText(status, 'Delete').visible).toBe(true)
        expect(findText(status, 'Change room type').visible).toBe(false)
        expect(findText(status, 'Send confirmation reminder').visible).toBe(true)
      })
      it('list type details shows room type, diversity, from and to columns', () => {
        const status = prepareRowActions('2')
        expect(findText(status, 'Delete').visible).toBe(true)
        expect(findText(status, 'Change room type').visible).toBe(true)
        expect(findText(status, 'Send confirmation reminder').visible).toBe(false)
      })
      it('list type addresses shows address column', () => {
        const status = prepareRowActions('3')
        expect(findText(status, 'Delete').visible).toBe(true)
        expect(findText(status, 'Change room type').visible).toBe(false)
        expect(findText(status, 'Send confirmation reminder').visible).toBe(false)
      })
      it('list type information shows dietary, family and t-shirt columns', () => {
        const status = prepareRowActions('4')
        expect(findText(status, 'Delete').visible).toBe(true)
        expect(findText(status, 'Change room type').visible).toBe(false)
        expect(findText(status, 'Send confirmation reminder').visible).toBe(false)
      })
      it('list type tags shows tag label, social media and pronoun column', () => {
        const status = prepareRowActions('5')
        expect(findText(status, 'Delete').visible).toBe(true)
        expect(findText(status, 'Change room type').visible).toBe(false)
        expect(findText(status, 'Send confirmation reminder').visible).toBe(false)
      })
      it('unknown list type shows all columns', () => {
        const status = prepareRowActions('4711')
        expect(findText(status, 'Delete').visible).toBe(true)
        expect(findText(status, 'Change room type').visible).toBe(true)
        expect(findText(status, 'Send confirmation reminder').visible).toBe(true)
      })
    })
  })
})
