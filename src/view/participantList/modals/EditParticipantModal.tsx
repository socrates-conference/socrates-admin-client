import React, {Component} from 'react'
import type {ModalButton} from '../../common/modal/Modal'
import Modal from '../../common/modal/Modal'
import {emptyParticipantListItem} from '../commons'
import {ParticipantListCommandId} from '../participantListCommand'
import {LabeledItem} from '../../../types'
import {SortableRow} from '../../common/table/TableTypes'

type Props = {
  id?: string
  closeModal: () => void
  executeAction: (item: SortableRow<any>, action: string) => void
  editData: SortableRow<any>
  roomTypes: Array<LabeledItem>
  show: boolean
  text: string
  title: string
}

type State = {
  data: SortableRow<any>
  message: string
}

export default class EditParticipantModal extends Component<Props, State> {
  state: State = {
    data: emptyParticipantListItem,
    message: ''
  }

  componentDidMount = (): void => {
    this.setState({data: this.props.editData})
  }
  shouldComponentUpdate = (nextProps: Props): boolean => {
    if (this.props.editData !== nextProps.editData) {
      this.setState({data: nextProps.editData})
    }
    return true
  }

  _editRoomTypeChanged = (value: string): void => {
    const newEdit = {...this.state.data}
    newEdit.roomType = value
    this.setState({data: newEdit})
  }


  render = (): JSX.Element => {
    const editData = this.state.data
    const buttons: Array<ModalButton> = [
      {
        isSubmit: true, onClick: () => {
          this.props.executeAction(this.state.data, ParticipantListCommandId.CHANGE_ROW_DETAILS)
        },
        text: 'Ok'
      },
      {
        isSubmit: false, onClick: () => {
          this.props.closeModal()
        },
        text: 'Cancel'
      }
    ]
    return (<Modal show={this.props.show} buttons={buttons}>
      <div className="modal-header">
        <h3 className="mb-2 mx-auto">
          <strong>Edit Participant</strong>
        </h3>
      </div>
      <div className="mb-2 mx-auto alert alert-info">
        <small>{`for: ${editData.name} (${editData.email})`}</small>
      </div>
      <div className="form-row">
        <div className="form-group">
          <label htmlFor="roomType" className="col-form-label">Room Type</label>
          <div className="table-responsive">
            {
              this.props.roomTypes && this.props.roomTypes
                .map(({value, label}) => (
                  <div key={value} className="radio-inline">
                    <label>
                      <input
                              checked={Boolean(editData.roomType && editData.roomType === value)}
                              id={value}
                              name="roomType"
                              onChange={() => this._editRoomTypeChanged(value)}
                              type="radio"
                              value={value}
                      />
                      <b>&nbsp;{label}</b>
                    </label>
                  </div>))
            }
          </div>
        </div>
      </div>
    </Modal>)
  }
}
