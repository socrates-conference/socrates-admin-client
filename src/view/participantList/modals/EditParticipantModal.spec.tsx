import React from 'react'
import {
  mount,
  ReactWrapper
} from 'enzyme'
import {
  emptyParticipantListItem,
  ParticipantListItem
} from '../commons'
import EditParticipantModal from './EditParticipantModal'

describe('(Component) EditParticipantModal', () => {
  let wrapper: ReactWrapper
  describe('renders', () => {
    beforeEach(() => {
      wrapper = mount(
        <EditParticipantModal
                text="Some text" title="Some title"
                id="editParticipantModal"
                editData={emptyParticipantListItem} roomTypes={[{value: '1', label: 'test room type'}]}
                show={true}
                closeModal={() => {}}
                executeAction={() => {}}
        />
      )
    })

    it('without exploding', () => {
      expect(wrapper).toHaveLength(1)
    })
  })

  describe('contains', () => {
    beforeEach(() => {
      wrapper = mount(
        <EditParticipantModal
                text="Some text" title="Some title"
                id="editParticipantModal"
                editData={emptyParticipantListItem}
                roomTypes={
                  [
                    {value: '1', label: 'test room type 1'},
                    {value: '2', label: 'test room type 2'},
                    {value: '3', label: 'test room type 3'},
                    {value: '4', label: 'test room type 4'}
                  ]
                }
                show={true}
                closeModal={() => {}}
                executeAction={() => {}}
        />
      )
    })

    it('defined edit data after mount', () => {
      const data: ParticipantListItem = wrapper.state('data')
      expect(data).toBe(emptyParticipantListItem)
    })

    it('edit data is set after props will update', () => {
      const instance: any = wrapper.instance()
      const newProps = {editData: {...emptyParticipantListItem, personId: 4711}}
      instance.shouldComponentUpdate(newProps)
      const data: ParticipantListItem = wrapper.state('data')
      expect(data.personId).toBe(4711)
    })

    it('changed room type after selection', () => {
      const instance: any = wrapper.instance()
      const newProps = {editData: {...emptyParticipantListItem, roomType: '1'}}
      instance.shouldComponentUpdate(newProps)
      instance._editRoomTypeChanged('3')
      wrapper.update()
      const data: ParticipantListItem = wrapper.state('data')
      expect(data.roomType).toEqual('3')
    })
  })
})
