import React from 'react'
import {
  mount,
  ReactWrapper
} from 'enzyme'
import ParticipantListContainer from './ParticipantListContainer'
import StoreFactory, {HistoryFactory} from '../store/store'

function* emptySaga(): any {
}

const reducer = (): any => {
  return {
    participantList: {
      rowsActionsStatus: [],
      tableActionsStatus: [],
      columnsStatus: [],
      lastActionResult: {commandId: '', error: '', result: ''},
      roomTypes: [],
      rows: []
    }
  }
}
const store = StoreFactory.createTestStore(reducer, emptySaga, HistoryFactory.createTestHistory())

describe('(Component) ParticipantListContainer', () => {
  let wrapper: ReactWrapper
  describe('renders', () => {
    beforeEach(() => {
      // @ts-ignore
      wrapper = mount(<ParticipantListContainer store={store}/>)
    })

    it('without exploding', () => {
      expect(wrapper).toHaveLength(1)
    })

    it('renders correctly', () => {
      expect(wrapper).toMatchSnapshot()
    })
  })
})
