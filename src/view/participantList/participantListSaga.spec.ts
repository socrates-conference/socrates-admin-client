import SagaTester from 'redux-saga-tester'
import rootSaga from '../sagas/rootSaga'
import * as sinon from 'sinon'
import * as api from '../requests/api'
import {
  emptyParticipantListItem,
  participantListTypes
} from './commons'
import participantListReducer, {ParticipantListState} from './participantListReducer'
import {
  closeModal,
  executeAction,
  getParticipants,
  handleAction,
  handleListTypeChange
} from './participantListCommand'
import ParticipantListEvents from './participantListEvents'
import ParticipantListEvent from './participantListEvents'
import {defaultColumnsStatus} from './participantListSagaHelper'

interface ParticipantsState {
  participantList: Partial<ParticipantListState>
}

describe('participantListSaga', () => {
  let sagaTester: SagaTester<ParticipantsState>
  describe('reacts to participant list handle action', () => {

    const INITIAL_STATE: ParticipantsState = {
      participantList: {
        columnsStatus: defaultColumnsStatus,
        currentCommand: '',
        currentListType: 1,
        currentRow: undefined,
        errorMessage: '',
        listTypes: participantListTypes,
        rows: []
      }
    }
    beforeEach(() => {
      sagaTester = new SagaTester({initialState: INITIAL_STATE, reducers: participantListReducer})
      sagaTester.start(rootSaga)
    })

    afterEach(() => {
      sagaTester.reset()
    })

    it('updating current command and current row', () => {
      sagaTester.dispatch(handleAction('testCommand', emptyParticipantListItem))
      expect(sagaTester.getState().participantList.currentCommand).toBe('testCommand')
      expect(sagaTester.getState().participantList.currentRow).toEqual(emptyParticipantListItem)
    })
  })

  describe('reacts to participant list execute action', () => {
    describe('on delete', () => {
      const deleteStub = sinon.stub(api, 'deleteParticipant')
      const INITIAL_STATE = {
        participantList: {
          columnsStatus: defaultColumnsStatus,
          currentCommand: 'delete',
          currentListType: 1,
          currentRow: emptyParticipantListItem,
          errorMessage: '',
          listTypes: participantListTypes,
          rows: []
        }
      }
      beforeEach(() => {
        sagaTester = new SagaTester({initialState: INITIAL_STATE, reducers: participantListReducer})
        sagaTester.start(rootSaga)
      })

      afterEach(() => {
        sagaTester.reset()
        deleteStub.reset()
      })

      it('deleting the current row', () => {
        deleteStub.returns(Promise.resolve(true))
        sagaTester.dispatch(executeAction(emptyParticipantListItem, 'delete'))
        return sagaTester.waitFor(ParticipantListEvent.EXECUTE_SUCCEEDED, true).then(() => {
          expect(deleteStub.getCalls().length).toBe(1)
          expect(sagaTester.getState().participantList.currentCommand).toBe('')
          expect(sagaTester.getState().participantList.currentRow).toBeUndefined()
        })
      })
      it('emitting failure event when delete fails', () => {
        deleteStub.returns(Promise.resolve(false))
        sagaTester.dispatch(executeAction(emptyParticipantListItem, 'delete'))
        return sagaTester.waitFor(ParticipantListEvent.EXECUTE_FAILED, true).then(() => {
          expect(deleteStub.getCalls().length).toBe(1)
          expect(sagaTester.getState().participantList.currentCommand).toBe('error')
          expect(sagaTester.getState().participantList.errorMessage).toBe('cannot execute command delete')
        })
      })
      it('emitting failure event when delete throws', () => {
        deleteStub.throws(new Error('Test Error'))
        sagaTester.dispatch(executeAction(emptyParticipantListItem, 'delete'))
        return sagaTester.waitFor(ParticipantListEvent.EXECUTE_FAILED, false).then(() => {
          expect(deleteStub.getCalls().length).toBe(1)
          expect(sagaTester.getState().participantList.currentCommand).toBe('error')
          expect(sagaTester.getState().participantList.errorMessage).toBe('Test Error')
        })
      })
    })
  })

  describe('reacts to participant list close modal', () => {
    const INITIAL_STATE: ParticipantsState = {
      participantList: {
        currentRoomTypeFilter: 'all',
        currentCommand: '',
        currentRow: undefined,
        errorMessage: '',
        roomTypes: [],
        rows: []
      }
    }
    beforeEach(() => {
      sagaTester = new SagaTester({initialState: INITIAL_STATE, reducers: participantListReducer})
      sagaTester.start(rootSaga)
    })

    afterEach(() => {
      sagaTester.reset()
    })

    it('deleting current command and current row', () => {
      sagaTester.dispatch(closeModal())
      expect(sagaTester.getState().participantList.currentCommand).toBe('')
      expect(sagaTester.getState().participantList.currentRow).toBeUndefined()
    })
  })

  describe('reacts to participant list get participants', () => {
    const INITIAL_STATE: ParticipantsState = {
      participantList: {
        columnsStatus: defaultColumnsStatus,
        currentCommand: '',
        currentListType: 1,
        currentRow: undefined,
        errorMessage: '',
        listTypes: participantListTypes,
        rows: []
      }
    }
    const readParticipantsStub = sinon.stub(api, 'readParticipants')

    beforeEach(() => {
      sagaTester = new SagaTester({initialState: INITIAL_STATE, reducers: participantListReducer})
      sagaTester.start(rootSaga)
    })

    afterEach(() => {
      readParticipantsStub.reset()
      sagaTester.reset()
    })

    it('updating state when data available', () => {
      // @ts-ignore
      readParticipantsStub.returns(Promise.resolve([{id: 4711}]))
      sagaTester.dispatch(getParticipants())
      return sagaTester.waitFor(ParticipantListEvent.GET_PARTICIPANTS_SUCCEEDED, true).then(() => {
        expect(readParticipantsStub.getCalls().length).toBeGreaterThan(0)
        expect(sagaTester.getState().participantList.rows?.length).toBe(1)
      })
    })
    it('updating state when no data available', () => {
      // @ts-ignore
      readParticipantsStub.returns(Promise.resolve(undefined))
      sagaTester.dispatch(getParticipants())
      return sagaTester.waitFor(ParticipantListEvent.GET_PARTICIPANTS_SUCCEEDED, true).then(() => {
        expect(readParticipantsStub.getCalls().length).toBe(1)
        expect(sagaTester.getState().participantList.rows?.length).toBe(0)
      })
    })
    it('emitting failure event when an error occurs', () => {
      readParticipantsStub.throws(new Error())
      sagaTester.dispatch(getParticipants())
      return sagaTester.waitFor(ParticipantListEvent.GET_PARTICIPANTS_FAILED).then(() => {
        expect(readParticipantsStub.getCalls().length).toBe(1)
        const actions = sagaTester.getCalledActions()
        expect(actions.filter(action => action.type === ParticipantListEvents.GET_PARTICIPANTS_FAILED)).toHaveLength(1)
      })
    })
  })

  describe('list type changes', () => {
    const INITIAL_STATE: ParticipantsState = {
      participantList: {
        columnsStatus: defaultColumnsStatus,
        currentCommand: '',
        currentListType: 1,
        currentRow: undefined,
        errorMessage: '',
        listTypes: participantListTypes,
        rows: []
      }
    }

    beforeEach(() => {
      sagaTester = new SagaTester({initialState: INITIAL_STATE, reducers: participantListReducer})
      sagaTester.start(rootSaga)
    })

    afterEach(() => {
      sagaTester.reset()
    })

    it('default list type is confirmations', () => {
      expect(sagaTester.getState().participantList.currentListType).toBe(1)
    })

    it('list type change updates current list type', () => {
      sagaTester.dispatch(handleListTypeChange('2'))
      return sagaTester.waitFor(ParticipantListEvent.LIST_TYPE_CHANGED).then(() => {
        expect(sagaTester.getState().participantList.currentListType).toBe(2)
      })
    })
  })
})
