import React from 'react'
import {shallow} from 'enzyme'
import ParticipantListHeader from './ParticipantListHeader'

describe('(Component) ParticipantListHeader', () => {
  const wrapper = shallow(
    <ParticipantListHeader title="The Header Title"/>)

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
