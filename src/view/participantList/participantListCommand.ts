import {SortableRow} from '../common/table/TableTypes'
import {Action} from 'redux'

export class ParticipantListCommandId {
  static DELETE_ROW = 'delete'
  static CHANGE_ROW_DETAILS = 'CHANGE_ROW_DETAILS'
  static SEND_CONFIRMATION_REMINDER = 'SEND_CONFIRMATION_REMINDER'
  static SEND_ALL_CONFIRMATION_REMINDERS = 'SEND_ALL_CONFIRMATION_REMINDER'
}

export default class ParticipantListCommand {
  static get HANDLE_ACTION(): string {
    return 'ParticipantListCommand/HANDLE_ACTION'
  }

  static get HANDLE_TABLE_ACTION(): string {
    return 'ParticipantListCommand/HANDLE_TABLE_ACTION'
  }

  static get EXECUTE_ACTION(): string {
    return 'ParticipantListCommand/EXECUTE_ACTION'
  }

  static get CLOSE_MODAL(): string {
    return 'ParticipantListCommand/CLOSE_MODAL'
  }

  static get GET_PARTICIPANTS(): string {
    return 'ParticipantListCommand/GET_PARTICIPANTS'
  }

  static get HANDLE_LIST_TYPE_CHANGE(): string {
    return 'ParticipantListCommand/HANDLE_LIST_TYPE_CHANGE'
  }

  static get GET_ROOM_TYPES(): string {
    return 'ParticipantListCommand/GET_ROOM_TYPES'
  }
}

type Command = Action
type HandleActionCommand = Command & { commandId: string, data?: any }
type ListTypeChangeCommand = Command & { newValue: string }

export const handleAction = (commandId: string, data: SortableRow<any>): HandleActionCommand =>
  ({type: ParticipantListCommand.HANDLE_ACTION, commandId, data})
export const handleTableAction = (commandId: string): HandleActionCommand =>
  ({type: ParticipantListCommand.HANDLE_TABLE_ACTION, commandId})
export const executeAction =
  (data: SortableRow<any>, commandId: string): HandleActionCommand =>
    ({type: ParticipantListCommand.EXECUTE_ACTION, data, commandId})

export const closeModal = (): Command => ({type: ParticipantListCommand.CLOSE_MODAL})

export const handleListTypeChange =
  (newValue: string): ListTypeChangeCommand => ({type: ParticipantListCommand.HANDLE_LIST_TYPE_CHANGE, newValue})

export const getParticipants = (): Command => ({type: ParticipantListCommand.GET_PARTICIPANTS})
export const getRoomTypes = (): Command => ({type: ParticipantListCommand.GET_ROOM_TYPES})


