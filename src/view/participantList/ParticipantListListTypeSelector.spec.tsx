import React from 'react'
import {shallow} from 'enzyme'
import Selector from '../common/selector/Selector'

describe('(Component) ParticipantListListTypeSelector', () => {
  const wrapper = shallow(
    <Selector
      id="my-selector"
      label="select something"
      selectionChanged={() => {}}
      defaultValue="1"
      selectable={[{label: 'label 1', value: 1}, {label: 'label 2', value: 'value 2'}]}
    />)

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
