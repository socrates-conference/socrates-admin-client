import {LabeledItem} from '../../types'

export type Address = {
  company: string
  address1: string
  address2: string
  province: string
  postal: string
  city: string
  country: string
}

export type ParticipantListItem = {
  personId: number
  participantId: number
  date: string
  name: string
  email: string
  diversityReason: number
  diversityDescription: string
  roomType?: string
  roomTypes: string[]
  from?: string
  to?: string
  firstName?: string
  lastName?: string
  address?: Address
  arrival?: string
  departure?: string
  dietaryInfo?: string
  familyInfo?: string
  shirt?: string
  labelName?: string
  social?: string
  pronoun?: string
  isConfirmed?: boolean
  confirmationDeadline?: string
  confirmationReminderCounter?: number
  confirmationLastReminder?: string
}

export const emptyAddress: Address = {
  company: '',
  address1: '',
  address2: '',
  province: '',
  postal: '',
  city: '',
  country: ''
}

export const emptyParticipantListItem: ParticipantListItem = {
  personId: -1,
  participantId: -1,
  date: '2000-01-01 00:00:00.000Z',
  name: '',
  email: '',
  roomTypes: [],
  diversityReason: 0,
  diversityDescription: '',
  from: '',
  to: '',
  firstName: '',
  lastName: '',
  address: emptyAddress,
  arrival: '',
  departure: '',
  dietaryInfo: '',
  familyInfo: '',
  shirt: '',
  labelName: '',
  social: '',
  pronoun: '',
  isConfirmed: false,
  confirmationDeadline: '',
  confirmationReminderCounter: 0,
  confirmationLastReminder: ''
}


export const participantListTypes: LabeledItem<number>[] = [
  {label: 'Confirmations', value: 1},
  {label: 'Details', value: 2},
  {label: 'Addresses', value: 3},
  {label: 'Information', value: 4},
  {label: 'Tags', value: 5}
]

export const arrivalDays: LabeledItem[] = [
  {value: '0', label: 'Thursday afternoon (with dinner)'},
  {value: '1', label: 'Thursday night (without dinner)'}
]

export const departureDays: LabeledItem[] = [
  {value: '0', label: 'Saturday afternoon (without dinner)'},
  {value: '1', label: 'Saturday night (with dinner)'},
  {value: '2', label: 'Sunday morning'},
  {value: '3', label: 'Sunday afternoon (with lunch)'},
  {value: '4', label: 'Sunday night (with dinner)'},
  {value: '5', label: 'Monday morning'}
]

export const shirtSizes: LabeledItem[] = [
  {value: '10', label: 'Unisex S'},
  {value: '11', label: 'Unisex M'},
  {value: '12', label: 'Unisex L'},
  {value: '13', label: 'Unisex XL'},
  {value: '14', label: 'Unisex XXL'},
  {value: '15', label: 'Unisex XXXL'},
  {value: '99', label: 'No t-shirt, please'}
]

