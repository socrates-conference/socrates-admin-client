import React from 'react'
import Selector from '../common/selector/Selector'
import type {SelectorItem} from '../common/selector/Selector'

export type Props = {
  listTypes: Array<SelectorItem>
  selectionChanged: (change:string) => void
}

export default function ParticipantListListTypeSelector(props: Props): JSX.Element {
  return (
    <Selector
      id="my-selector"
      label="Select a list type: "
      selectionChanged={props.selectionChanged}
      defaultValue="1"
      selectable={props.listTypes}
    />)
}

