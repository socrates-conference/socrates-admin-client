import React, {Component} from 'react'
import type {ParticipantListItem} from './commons'
import {emptyParticipantListItem} from './commons'
import ParticipantListTable from './ParticipantListTable'
import {
  closeModal,
  executeAction,
  getParticipants,
  getRoomTypes,
  handleAction,
  handleListTypeChange,
  handleTableAction,
  ParticipantListCommandId
} from './participantListCommand'
import {connect} from 'react-redux'
import type {ParticipantListState} from './participantListReducer'
import type {
  Action,
  SortableRow
} from '../common/table/TableTypes'
import ParticipantListModals from './ParticipantListModals'
import ParticipantListListTypeSelector from './participantListListTypeSelector'
import ParticipantListHeader from './ParticipantListHeader'
import {LabeledItem} from '../../types'
import {RoomType} from '../reducers/appReducer'

type StateFromProps = {
  participantList: ParticipantListState
  roomTypes: LabeledItem[]
}

type DispatchFromProps = {
  closeModal: () => void
  executeAction: (item: SortableRow<any>, action: string) => void
  getRoomTypes: () => void
  handleAction: (action: string, item: SortableRow<any>) => void
  handleTableAction: (action: string) => void
  getParticipants: () => void
  handleListTypeChange: (type: string) => void
}

type Props = StateFromProps & DispatchFromProps

class ParticipantListContainer extends Component<Props> {
  _rowActions: Array<Action> = [
    {
      execute: (item) => this.props.handleAction(ParticipantListCommandId.DELETE_ROW, item),
      canExecute: () => true, buttonClass: 'btn-danger',
      icon: 'trash', text: 'Delete', visible: true
    },
    {
      execute: (item) => this.props.handleAction(ParticipantListCommandId.CHANGE_ROW_DETAILS, item),
      canExecute: () => true, buttonClass: 'btn-info',
      icon: 'door-closed', text: 'Change room type', visible: false
    },
    {
      execute: (item) => this.props.handleAction(ParticipantListCommandId.SEND_CONFIRMATION_REMINDER, item),
      canExecute: (item) => !item.isConfirmed, buttonClass: 'btn-warning',
      icon: 'at', text: 'Send confirmation reminder', visible: true
    }
  ]

  _tableActions: Array<Action> = [
    {
      execute: () => this.props.getParticipants(),
      canExecute: () => true, buttonClass: 'btn-info',
      icon: 'sync', text: 'Refresh', visible: true
    },
    {
      execute: () => this.props.handleTableAction(ParticipantListCommandId.SEND_ALL_CONFIRMATION_REMINDERS),
      canExecute: () => true, buttonClass: 'btn-warning',
      icon: 'at', text: 'Confirm reminder email', visible: true
    }
  ]

  componentDidMount = (): void => {
    this.props.getRoomTypes()
    this.props.getParticipants()
  }

  getTitle = (commandId: string): string => {
    switch (commandId) {
      case ParticipantListCommandId.DELETE_ROW:
        return 'Delete participant'
      case ParticipantListCommandId.CHANGE_ROW_DETAILS:
        return 'change room type'
      case ParticipantListCommandId.SEND_CONFIRMATION_REMINDER:
      case ParticipantListCommandId.SEND_ALL_CONFIRMATION_REMINDERS:
        return 'Confirmation reminder'
      default:
        return ''
    }
  }
  getText = (commandId: string, data: ParticipantListItem): string => {
    switch (commandId) {
      case ParticipantListCommandId.DELETE_ROW:
      case ParticipantListCommandId.CHANGE_ROW_DETAILS:
      case ParticipantListCommandId.SEND_CONFIRMATION_REMINDER:
        return data.name + ' ' + data.email
      case ParticipantListCommandId.SEND_ALL_CONFIRMATION_REMINDERS:
        return 'Executing this will send an Email to each participant that has not yet confirmed the spot.'
      default:
        return ''
    }
  }
  render = (): JSX.Element => {
    const {
      rows, currentCommand, currentRow, errorMessage, columnsStatus, rowActionsStatus, tableActionsStatus, listTypes
    } = this.props.participantList
    return (
      <div className="container">
        {
                currentCommand !== '' ?
                  <ParticipantListModals
                                commandId={currentCommand} data={currentRow || emptyParticipantListItem}
                                errorMessage={errorMessage} closeModal={this.props.closeModal}
                                executeAction={(participantItem, commandId) =>
                                  this.props.executeAction(participantItem, commandId)}
                                text={this.getText(currentCommand, currentRow || emptyParticipantListItem)}
                                title={this.getTitle(currentCommand)} roomTypes={this.props.roomTypes}
                        /> : <div />
              }
        <ParticipantListHeader title="Participant list"/>
        <ParticipantListListTypeSelector
                      selectionChanged={this.props.handleListTypeChange}
                      listTypes={listTypes}/>
        <ParticipantListTable
                      rowActions={this._rowActions}
                      tableActions={this._tableActions}
                      rows={rows}
                      columnsStatus={columnsStatus}
                      rowActionsStatus={rowActionsStatus}
                      tableActionsStatus={tableActionsStatus}
              />
      </div>
    )
  }
}

const mapStateToProps = (state: any): StateFromProps => {
  return {
    participantList: {...state.participantList},
    roomTypes: state.participantList?.roomTypes?.map((r:RoomType) => ({label:r.display, value:r.id} as LabeledItem))
  }
}

const mapDispatchToProps = {
  getParticipants, handleListTypeChange, getRoomTypes,
  handleAction, closeModal, executeAction, handleTableAction
}

export default connect<StateFromProps, DispatchFromProps>(mapStateToProps, mapDispatchToProps)(ParticipantListContainer)
