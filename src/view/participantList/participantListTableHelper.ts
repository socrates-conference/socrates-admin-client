import moment from 'moment/moment'
import type {
  ColumnDefinition,
  ColumnStatus,
  ActionStatus,
  Action,
  SortableRow
} from '../common/table/TableTypes'
import {Address} from './commons'
import {TableValue} from '../applicantList/ApplicationListTable'

export function dateFilter(data: Array<SortableRow<TableValue>>, field: string, value: string, format: string): SortableRow<TableValue>[] {
  return data.filter((item) => {
    const valueToCheck = moment((item as SortableRow)[field]).format(format)
    return valueToCheck.indexOf(value) >= 0
  })
}

export function booleanFilter(data:Array<SortableRow<TableValue>>, field: string, value: string): SortableRow<TableValue>[] {
  return data.filter((item) => {
    const valueToCheck = (item as SortableRow)[field] ? 'yes' : 'no'
    return valueToCheck.indexOf(value) >= 0
  })
}

export function addressFilter(data: Array<SortableRow<boolean|number|string|Address>>, field: string, value: string): SortableRow<TableValue>[] {
  function companyContainsValue(address: Address): boolean {
    return Boolean(address.company?.toLowerCase().indexOf(value.toLowerCase()) >= 0)
  }

  function address1ContainsValue(address: Address): boolean {
    return Boolean(address.address1?.toLowerCase().indexOf(value.toLowerCase()) >= 0)
  }

  function address2ContainsValue(address: Address): boolean {
    return Boolean(address.address2?.toLowerCase().indexOf(value.toLowerCase()) >= 0)
  }

  function provinceContainsValue(address: Address): boolean {
    return Boolean(address.province?.toLowerCase().indexOf(value.toLowerCase()) >= 0)
  }

  function postalContainsValue(address: Address): boolean {
    return Boolean(address.postal?.toLowerCase().indexOf(value.toLowerCase()) >= 0)
  }

  function cityContainsValue(address: Address): boolean {
    return Boolean(address.city?.toLowerCase().indexOf(value.toLowerCase()) >= 0)
  }

  function countryContainsValue(address:Address): boolean {
    return Boolean(address.country?.toLowerCase().indexOf(value.toLowerCase()) >= 0)
  }

  return data.filter((item) => {
    const address:Address = (item as SortableRow<Address>)[field]
    return companyContainsValue(address)
      || address1ContainsValue(address)
      || address2ContainsValue(address)
      || provinceContainsValue(address)
      || postalContainsValue(address)
      || cityContainsValue(address)
      || countryContainsValue(address)
  })
}

export function setVisibleColumns(
  columns: Array<ColumnDefinition>, status: Array<ColumnStatus>): Array<ColumnDefinition> {
  return columns.map(column => {
    const theStatus = status?.find(c => c.header === column.header)
    column.visible = theStatus ? theStatus.visible : true
    return column
  }) || []
}

export function setVisibleActions(actions: Array<Action>, status: Array<ActionStatus>): Array<Action> {
  return actions.map(action => {
    const theStatus = status?.find(c => c.text === action.text)
    action.visible = theStatus ? theStatus.visible : true
    return action
  }) || []
}

