import React from 'react'
import type {Address} from './commons'

type Props = { value: Address }

export default function AddressCellTemplate(props: Props): JSX.Element {
  if (props.value) {
    return <div>
      {props.value.company ? <div>{props.value.company}</div> : null}
      {props.value.address1 ? <div>{props.value.address1}</div> : null}
      {props.value.address2 ? <div>{props.value.address2}</div> : null}
      {props.value.province ? <div>{props.value.province}</div> : null}
      {props.value.postal || props.value.city ?
        <div>{props.value.postal} <span>{props.value.city}</span></div> : null}
      {props.value.country ? <div>{props.value.country}</div> : null}
    </div>
  } else {
    return <div />
  }
}
