import type {ParticipantListItem} from './commons'
import type {ActionStatus, ColumnStatus} from '../common/table/TableTypes'
import {Action} from 'redux'

export default class ParticipantListEvent {

  static get EXECUTE_SUCCEEDED(): string {
    return 'ParticipantListEvent/EXECUTE_SUCCEEDED'
  }

  static get EXECUTE_FAILED(): string {
    return 'ParticipantListEvent/EXECUTE_ERROR'
  }

  static get EXECUTE_CANCELED(): string {
    return 'ParticipantListEvent/EXECUTE_CANCELED'
  }

  static get GET_PARTICIPANTS_SUCCEEDED(): string {
    return 'ParticipantListEvent/GET_PARTICIPANTS_SUCCEEDED'
  }

  static get GET_PARTICIPANTS_FAILED(): string {
    return 'ParticipantListEvent/GET_PARTICIPANTS_FAILED'
  }

  static get HANDLED_ACTION(): string {
    return 'ParticipantListEvent/HANDLED_ACTION'
  }

  static get LIST_TYPE_CHANGED(): string {
    return 'ParticipantListEvent/LIST_TYPE_CHANGED'
  }

  static get GET_ROOM_TYPES_SUCCEEDED(): string {
    return 'ParticipantListEvent/GET_ROOM_TYPES_SUCCEEDED'
  }

  static get GET_ROOM_TYPES_FAILED(): string {
    return 'ParticipantListEvent/GET_ROOM_TYPES_FAILED'
  }
}

type Event = Action
type ErrorEvent = Event & { error: any }
type SuccessEvent = Action & { rows: any[] }
type HandledEvent = Event & {commandId: string, data: any}
type ListTypeChangedEvent = Event & {
  newListType:string
  columnsStatus: ColumnStatus[]
  rowActionsStatus: ActionStatus[]
  tableActionsStatus: ActionStatus[]
}

export const participantListExecutionSucceed = (): Action => ({type: ParticipantListEvent.EXECUTE_SUCCEEDED})
export const participantListExecutionFailed = (error: Error): ErrorEvent => ({type: ParticipantListEvent.EXECUTE_FAILED, error})
export const participantListExecutionCanceled = (): Action => ({type: ParticipantListEvent.EXECUTE_CANCELED})

export const getParticipantsSucceeded =
  (rows: Array<any>): SuccessEvent => ({type: ParticipantListEvent.GET_PARTICIPANTS_SUCCEEDED, rows})
export const getParticipantsFailed =
  (error: any): ErrorEvent => ({type: ParticipantListEvent.GET_PARTICIPANTS_FAILED, error})

export const handledAction =
  (commandId: string, data: ParticipantListItem): HandledEvent =>
    ({type: ParticipantListEvent.HANDLED_ACTION, commandId, data})
export const listTypeChanged = (newListType: string, columnsStatus: Array<ColumnStatus>, rowActionsStatus: Array<ActionStatus>,
  tableActionsStatus: Array<ActionStatus>): ListTypeChangedEvent =>
  ({type: ParticipantListEvent.LIST_TYPE_CHANGED, newListType, columnsStatus, rowActionsStatus, tableActionsStatus})

export const getRoomTypesSucceeded =
  (rows: Array<any>): SuccessEvent => ({type: ParticipantListEvent.GET_ROOM_TYPES_SUCCEEDED, rows})
export const getRoomTypesFailed =
  (error: any): ErrorEvent => ({type: ParticipantListEvent.GET_ROOM_TYPES_FAILED, error})
