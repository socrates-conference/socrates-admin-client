import React from 'react'
import Table from '../common/table/Table'
import type {
  Action,
  ActionStatus,
  ColumnDefinition,
  ColumnStatus,
  SortableRow
} from '../common/table/TableTypes'
import EmailCellTemplate from '../common/table/cellTemplates/EmailCellTemplate'
import type {
  Address,
  ParticipantListItem
} from './commons'
import BooleanCellTemplate from '../common/table/cellTemplates/BooleanCellTemplate'
import {
  addressFilter,
  booleanFilter,
  dateFilter,
  setVisibleActions,
  setVisibleColumns
} from './participantListTableHelper'
import AddressCellTemplate from './AddressCellTemplate'
import dateFormat from '../../utils/dateFormat'

export type Props = {
  rowActions: Array<Action>
  tableActions: Array<Action>
  columnsStatus: Array<ColumnStatus>
  rowActionsStatus: Array<ActionStatus>
  rows: Array<ParticipantListItem>
  tableActionsStatus: Array<ActionStatus>
  refresh?: () => void
}

function emailTemplate(value: string): JSX.Element {
  return <EmailCellTemplate value={value}/>
}

function booleanTemplate(value: boolean): JSX.Element {
  return <BooleanCellTemplate value={value}/>
}

function addressTemplate(value: Address): JSX.Element {
  return <AddressCellTemplate value={value}/>
}

const dateTimeFormat = 'DD.MM.YYYY HH:mm:SS'

const columns: Array<ColumnDefinition> = [
  {
    header: 'Name', field: 'name', displayClass: 'd-none d-md-table-cell', isSortable: true,
    isFilterable: true, visible: true
  },
  {
    header: 'Email', field: 'email', cellTemplate: (value: string) => emailTemplate(value),
    isSortable: true, isFilterable: true, visible: true
  },
  {
    header: 'Confirmed', field: 'isConfirmed',
    cellTemplate: (value: boolean) => booleanTemplate(value),
    filter: (data: SortableRow<string | number | boolean | Address>[], field, value) => booleanFilter(data, field, value),
    isSortable: true, isFilterable: true, visible: true
  },
  {
    header: 'Deadline', field: 'confirmationDeadline', format: (value) => dateFormat(value, dateTimeFormat),
    filter: (data, field, value) => dateFilter(data, field, value, dateTimeFormat),
    isSortable: true, isFilterable: true, visible: true
  },
  {
    header: '#Reminders', field: 'confirmationReminderCounter',
    isSortable: true, isFilterable: true, visible: true
  },
  {
    header: 'Last Reminder', field: 'confirmationLastReminder', format: (value) => dateFormat(value, dateTimeFormat),
    filter: (data, field, value) => dateFilter(data, field, value, dateTimeFormat),
    isSortable: true, isFilterable: true, visible: true
  },
  {
    header: 'Room type', field: 'roomType',
    isSortable: true, isFilterable: true, visible: false
  },
  {
    header: 'Diversity', field: 'diversityReason', displayClass: 'd-none d-md-table-cell',
    isSortable: true, isFilterable: true, visible: false
  },
  {
    header: 'First name', field: 'firstName', displayClass: 'd-none d-lg-table-cell',
    isSortable: true, isFilterable: true, visible: false
  },
  {
    header: 'Last name', field: 'lastName', displayClass: 'd-none d-lg-table-cell',
    isSortable: true, isFilterable: true, visible: false
  },
  {
    header: 'Address', field: 'address',
    cellTemplate: (value: Address) => addressTemplate(value),
    filter: (data, field, value) => addressFilter(data, field, value),

    isSortable: true, isFilterable: true, visible: false
  },
  {
    header: 'Arrival', field: 'arrival', displayClass: 'd-none d-lg-table-cell',
    isSortable: true, isFilterable: true, visible: false
  },
  {
    header: 'Departure', field: 'departure', displayClass: 'd-none d-lg-table-cell',
    isSortable: true, isFilterable: true, visible: false
  },
  {
    header: 'T-Shirt', field: 'shirt',
    isSortable: true, isFilterable: true, visible: false
  },
  {
    header: 'Dietary', field: 'dietaryInfo', displayClass: 'd-none d-md-table-cell',
    isSortable: true, isFilterable: true, visible: false
  },
  {
    header: 'Family', field: 'familyInfo', displayClass: 'd-none d-md-table-cell',
    isSortable: true, isFilterable: true, visible: false
  },
  {
    header: 'Tag', field: 'labelName', displayClass: 'd-none d-md-table-cell',
    isSortable: true, isFilterable: true, visible: false
  },
  {
    header: 'Social', field: 'social',
    isSortable: true, isFilterable: true, visible: false
  },
  {
    header: 'Pronoun', field: 'pronoun', displayClass: 'd-none d-md-table-cell',
    isSortable: true, isFilterable: true, visible: false
  }
]


export default function ParticipantListTable(props: Props): JSX.Element {
  return (<div id="participantList">
    <Table
            columns={setVisibleColumns(columns, props.columnsStatus)} data={props.rows}
            rowActions={setVisibleActions(props.rowActions, props.rowActionsStatus)}
            tableActions={setVisibleActions(props.tableActions, props.tableActionsStatus)}
    />
  </div>)
}
