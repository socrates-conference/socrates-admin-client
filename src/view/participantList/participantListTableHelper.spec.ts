import {
  addressFilter,
  booleanFilter,
  dateFilter,
  setVisibleActions,
  setVisibleColumns
} from './participantListTableHelper'
import {
  Action,
  ActionStatus,
  ColumnDefinition,
  ColumnStatus,
  SortableRow
} from '../common/table/TableTypes'
import {Address} from './commons'

const testData: SortableRow<boolean|number|string|Address>[] = [
  {
    personId: 280,
    participantId: 402,
    date: '2018-03-01T15:00:00.000Z',
    name: 'Test 1',
    email: 'test1@email.de',
    roomType: 'single',
    diversityReason: 'yes',
    firstName: 'Test',
    lastName: 'One',
    address: {company: '', address1:'Street', address2:'c/o bla', province: 'province', postal: '11111',
      city: 'somewhere', country: 'Germany'},
    arrival: '0',
    departure: '3',
    dietaryInfo: '',
    familyInfo: '',
    shirt: '',
    labelName: '',
    social: '',
    pronoun: '',
    isConfirmed: true,
    confirmationDeadline: '2018-04-01T15:00:00.000Z',
    confirmationReminderCounter: 1,
    confirmationLastReminder: '2018-03-15T15:00:00.000Z'
  },
  {
    personId: 400,
    participantId: 574,
    date: '2018-03-01T15:00:00.000Z',
    name: 'Test 1',
    email: 'test1@email.de',
    roomType: 'single',
    diversityReason: 'yes',
    firstName: 'Test',
    lastName: 'One',
    address: {company: '', address1:'Street', address2:'c/o bla', province: 'province', postal: '55555',
      city: 'somewhere', country: 'Germany'},
    arrival: '0',
    departure: '3',
    dietaryInfo: '',
    familyInfo: '',
    shirt: '',
    labelName: '',
    social: '',
    pronoun: '',
    isConfirmed: false,
    confirmationDeadline: '2018-04-01T15:00:00.000Z',
    confirmationReminderCounter: 1,
    confirmationLastReminder: '2018-03-15T15:00:00.000Z'
  },
  {
    personId: 20,
    participantId: 27,
    date: '2018-03-02T15:00:00.000Z',
    name: 'Test 1',
    email: 'test1@email.de',
    roomType: 'single',
    diversityReason: 'yes',
    firstName: 'Test',
    lastName: 'One',
    address: {company: '', address1:'Avenue', address2:'c/o bla', province: 'province', postal: '22222',
      city: 'somewhere', country: 'Germany'},
    arrival: '0',
    departure: '3',
    dietaryInfo: '',
    familyInfo: '',
    shirt: '',
    labelName: '',
    social: '',
    pronoun: '',
    isConfirmed: false,
    confirmationDeadline: '2018-04-01T15:00:00.000Z',
    confirmationReminderCounter: 1,
    confirmationLastReminder: '2018-03-15T15:00:00.000Z'
  }
]


describe('applicationListTableHelper:', () => {
  describe('dateFilter', () => {
    it('returns filtered array', () => {
      const emptyResult = dateFilter(testData, 'date', '2000', 'DD.MM.YYYY HH:mm:ss')
      expect(emptyResult.length).toBe(0)
      const result = dateFilter(testData, 'date', '01.03', 'DD.MM.YYYY HH:mm:ss')
      expect(result.length).toBe(2)
    })
  })
  describe('booleanFilter', () => {
    it('returns expected values array', () => {
      const yesResult = booleanFilter(testData, 'isConfirmed', 'yes')
      expect(yesResult.length).toBe(1)
      const noResult = booleanFilter(testData, 'isConfirmed', 'no')
      expect(noResult.length).toBe(2)
    })
  })
  describe('addressFilter', () => {
    it('returns expected values array', () => {
      const somewhere = addressFilter(testData, 'address', '11111')
      expect(somewhere.length).toBe(1)
      const street = addressFilter(testData, 'address', 'street')
      expect(street.length).toBe(2)
    })
  })
  describe('setVisibleColumns', () => {
    it('returns expected values array', () => {
      const columns: ColumnDefinition[] = [{header:'1', field:'', visible: false}, {header:'2',field:'', visible: false}, {header:'3',field:'', visible: false}]
      const status1: ColumnStatus[] = [{header:'1', visible: true}, {header:'2',visible: true}, {header:'3', visible: false}]
      const status2: ColumnStatus[] = [{header:'1', visible: false},{header:'2',visible: false},{header:'3', visible: true}]
      const withStatus1 = setVisibleColumns(columns, status1)
      expect(withStatus1.length).toBe(3)
      expect(withStatus1.filter(x => x.visible).length).toBe(2)
      const withStatus2 = setVisibleColumns(columns, status2)
      expect(withStatus2.length).toBe(3)
      expect(withStatus1.filter(x => x.visible).length).toBe(1)
    })
  })
  describe('setVisibleActions', () => {
    it('returns expected values array', () => {
      // @ts-ignore
      const actions: Action[] = [{text:'1', visible: false}, {text:'2', visible: false}, {text:'3', visible: false}]
      const status1: ActionStatus[] = [{text:'1', visible: true}, {text:'2', visible: true}, {text:'3', visible: false}]
      const status2: ActionStatus[] = [{text:'1', visible: false}, {text:'2', visible: false}, {text:'3', visible: true}]
      const withStatus1 = setVisibleActions(actions, status1)
      expect(withStatus1.length).toBe(3)
      expect(withStatus1.filter(x => x.visible).length).toBe(2)
      const withStatus2 = setVisibleActions(actions, status2)
      expect(withStatus2.length).toBe(3)
      expect(withStatus1.filter(x => x.visible).length).toBe(1)
    })
  })
})
