import ParticipantListEvent from './participantListEvents'
import type {ParticipantListItem} from './commons'
import {arrivalDays, departureDays, participantListTypes, shirtSizes} from './commons'
import type {ColumnStatus, ActionStatus} from '../common/table/TableTypes'
import type {SelectorItem} from '../common/selector/Selector'
import {defaultColumnsStatus, defaultRowActionsStatus, defaultTableActionsStatus} from './participantListSagaHelper'
import {RoomType} from '../reducers/appReducer'


export type ParticipantListState = {
  arrivalDays: Array<{value: string, label: string}>
  columnsStatus: Array<ColumnStatus>
  currentCommand: string
  currentRoomTypeFilter?: string
  currentListType: number
  currentRow?: ParticipantListItem
  departureDays: Array<{value: string, label: string}>
  errorMessage: string
  listTypes: Array<SelectorItem>
  roomTypes: Array<RoomType>
  rowActionsStatus: Array<ActionStatus>
  rows: Array<ParticipantListItem>
  shirtSizes: Array<{value: string, label: string}>
  tableActionsStatus: Array<ActionStatus>
}

const INITIAL: ParticipantListState = {
  arrivalDays: arrivalDays,
  columnsStatus: defaultColumnsStatus,
  currentCommand: '',
  currentListType: 1,
  currentRow: undefined,
  departureDays: departureDays,
  errorMessage: '',
  listTypes: participantListTypes,
  roomTypes: [],
  rowActionsStatus: defaultRowActionsStatus,
  rows: [],
  shirtSizes: shirtSizes,
  tableActionsStatus: defaultTableActionsStatus
}

const participantListReducer = (state: ParticipantListState = INITIAL, action: any): ParticipantListState => {
  switch (action.type) {
    case ParticipantListEvent.EXECUTE_SUCCEEDED:
      return {...state, currentCommand: '', errorMessage: '', currentRow : undefined}
    case ParticipantListEvent.EXECUTE_FAILED:
      return {...state, currentCommand: 'error', errorMessage: action.error.message}
    case ParticipantListEvent.EXECUTE_CANCELED:
      return {...state, currentCommand: '', errorMessage: '', currentRow : undefined}
    case ParticipantListEvent.GET_PARTICIPANTS_SUCCEEDED:
      return {...state, rows: action.rows}
    case ParticipantListEvent.GET_PARTICIPANTS_FAILED:
      return {...state, rows: []}
    case ParticipantListEvent.HANDLED_ACTION:
      return {...state, currentCommand: action.commandId, currentRow: action.data}
    case ParticipantListEvent.LIST_TYPE_CHANGED:
      return {
        ...state,
        currentListType: Number(action.newListType),
        columnsStatus: action.columnsStatus,
        rowActionsStatus: action.rowActionsStatus,
        tableActionsStatus: action.tableActionsStatus
      }
    case ParticipantListEvent.GET_ROOM_TYPES_SUCCEEDED:
      return {...state, roomTypes: action.rows}
    case ParticipantListEvent.GET_ROOM_TYPES_FAILED:
      return {...state, roomTypes: []}
    default:
      return state
  }
}

export default {participantList: participantListReducer}
