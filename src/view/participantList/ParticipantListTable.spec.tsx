import React from 'react'
import {
  mount,
  ReactWrapper,
  shallow,
  ShallowWrapper
} from 'enzyme'
import axios from 'axios'
import ParticipantListTable from './ParticipantListTable'
import TableRow from '../common/table/body/TableRow'
import {
  SinonSpy,
  spy
} from 'sinon'
import '../icons'
import {ParticipantListItem} from './commons'
import { mocked } from 'ts-jest/utils'

jest.mock('axios')
const axiosMock = mocked(axios, true)
const testData: ParticipantListItem[] = [
  {
    'personId': 280,
    'participantId': 402,
    'date': '2018-03-01T15:00:00.000Z',
    'name': 'name 280',
    'email': 'name280@email.de',
    'roomTypes': [
      'single'
    ],
    'diversityReason': 0,
    'diversityDescription': ''
  },
  {
    'personId': 400,
    'participantId': 574,
    'date': '2018-03-01T15:00:00.000Z',
    'name': 'name 400',
    'email': 'name400@email.de',
    'roomTypes': [
      'single',
      'junior_shared'
    ],
    'diversityReason': 0,
    'diversityDescription': ''
  },
  {
    'personId': 20,
    'participantId': 27,
    'date': '2018-04-01T19:00:00.000Z',
    'name': 'name 20',
    'email': 'name20@email.de',
    'roomTypes': [
      'single',
      'bed_in_double',
      'junior_exclusively'
    ],
    'diversityReason': 0,
    'diversityDescription': ''
  }
]

describe('(Component) ParticipantListTable', () => {
  beforeEach(() => {
    axiosMock.get.mockImplementation(() => Promise.resolve({data: testData}))
  })
  afterEach(() => {
    axiosMock.get.mockReset()
  })
  describe('renders', () => {
    let wrapper: ShallowWrapper<typeof ParticipantListTable>
    beforeEach(() => {
      wrapper = shallow(
        <ParticipantListTable
                rowActions={[]}
                tableActions={[]}
                refresh={() => {
                }}
                columnsStatus={[]}
                rowActionsStatus={[]}
                tableActionsStatus={[]}
                rows={testData}
        />
      )
    })

    it('without exploding', () => {
      expect(wrapper).toHaveLength(1)
    })

    it('correctly', () => {
      expect(wrapper).toMatchSnapshot()
    })
  })

  describe('after render', () => {
    let wrapper: ReactWrapper
    let refreshSpy: SinonSpy
    beforeEach(() => {
      refreshSpy = spy()
      wrapper = mount(
        <ParticipantListTable
                      rowActions={[]}
                      tableActions={[]}
                      refresh={refreshSpy}
                      columnsStatus={[]}
                      rowActionsStatus={[]}
                      tableActionsStatus={[]}
                      rows={testData}
              />
      )
    })
    afterEach(() => {
      refreshSpy.resetHistory()
    })
    it('rows are filled', () => {
      const rows = wrapper.find(TableRow)
      expect(rows.length).toBe(3)
    })
  })
})
