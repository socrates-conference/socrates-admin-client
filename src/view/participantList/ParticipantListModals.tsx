import React from 'react'
import AreYouSureModal from '../common/modal/AreYouSureModal'
import ErrorModal from '../common/modal/ErrorModal'
import {ParticipantListCommandId} from './participantListCommand'
import EditParticipantModal from './modals/EditParticipantModal'
import {LabeledItem} from '../../types'
import {SortableRow} from '../common/table/TableTypes'

export type Props = {
  closeModal: () => void
  executeAction: (item:SortableRow<any>, action:string) => void
  commandId: string
  data: SortableRow<any>
  errorMessage: string
  title: string
  text: string
  roomTypes: Array<LabeledItem>
}

export default function ParticipantListModals (props: Props): JSX.Element {
  const {data, commandId, closeModal, errorMessage, executeAction, title, text, roomTypes} = props
  return commandId !== '' ?
    <div>
      <EditParticipantModal
                    editData={data} show={commandId === ParticipantListCommandId.CHANGE_ROW_DETAILS}
                    closeModal={closeModal} title={title} text={text}
                    executeAction={executeAction} roomTypes={roomTypes}
            />
      <AreYouSureModal
                    id="areYouSureModal" data={data} commandId={commandId}
                    show={
                      commandId === ParticipantListCommandId.DELETE_ROW
                      || commandId === ParticipantListCommandId.SEND_ALL_CONFIRMATION_REMINDERS
                      || commandId === ParticipantListCommandId.SEND_CONFIRMATION_REMINDER
                    }
                    closeModal={closeModal} title={title} text={text}
                    executeAction={executeAction}
            />
      <ErrorModal
                    show={commandId === 'error'} closeModal={closeModal}
                    title="Error" text="Operation failed" message={errorMessage}
            />
    </div> : <div/>
}
