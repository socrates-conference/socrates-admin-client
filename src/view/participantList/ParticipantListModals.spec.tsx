import React from 'react'
import {
  shallow,
  ShallowWrapper
} from 'enzyme'
import ParticipantListModals from './ParticipantListModals'
import {emptyParticipantListItem} from './commons'


describe('(Component) ParticipantListModals', () => {
  let wrapper: ShallowWrapper<typeof ParticipantListModals>
  beforeEach(() => {
    wrapper = shallow(
      <ParticipantListModals
                    commandId="delete" data={emptyParticipantListItem}
                    roomTypes={[{value: '1', label: 'test room type'}]}
                    errorMessage="" closeModal={() => {
                    }}
                    executeAction={() => {
                    }} text="Test Text" title="Title"
            />
    )
  })

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
