import React from 'react'
import PropTypes from 'prop-types'
import moment, {Moment} from 'moment/moment'

type Props = {
  lotteryDay?: Moment
}

export default function LotteryInfo(props: Props): JSX.Element {
  return (<div>
    <div className="row">
      <div className="col-sm-12">
        <div className="page-header"><h1>Lottery</h1></div>
      </div>
    </div>
    <div className="row">
      <div className="col-sm-12">
        <p>By pressing the button below, a lottery draw will be carried out.</p>
        <p>Be careful, this will randomize vacancies for the conference between applicants.</p>
        {(props.lotteryDay && moment().isBefore(props.lotteryDay))
          ? <p>
            The first draw should not be made before
            the <strong>{props.lotteryDay.format('LLL')}</strong>. Please
            wait until then.
          </p>
          : null
        }
      </div>
    </div>
  </div>)
}

LotteryInfo.propTypes = {
  lotteryDay: PropTypes.object
}
