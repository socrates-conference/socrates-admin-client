import React from 'react'
import {shallow} from 'enzyme'
import LotteryLog from './LotteryLog'

describe('(Component) LotteryLog', () => {
  const wrapper = shallow(
    <LotteryLog log={[{text:'some data', status: true}]} registeringIsRunning={true}/>
  )

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
