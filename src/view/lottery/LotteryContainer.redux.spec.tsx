import React from 'react'
import {shallow} from 'enzyme'
import LotteryContainer from './LotteryContainer'
import StoreFactory, {HistoryFactory} from '../store/store'

function* emptySaga(): any {
}

const appReducer = (): any => {
  return {conference: {}}
}
const lotteryReducer = (): any => {
  return {
    distribution: [],
    distributionErrors: [],
    hasCheckedDistributionHealth: false,
    info: [],
    registrationResults: [],
    rows: [],
    registeringIsRunning: false,
    log: []
  }
}


const theReducers = {
  app: appReducer,
  lottery: lotteryReducer
}

const store = StoreFactory.createTestStore(theReducers, emptySaga, HistoryFactory.createTestHistory())


describe('(Component) LotteryContainer', () => {
  // @ts-ignore
  const wrapper = shallow(<LotteryContainer store={store}/>)

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
