import React from 'react'
import PropTypes from 'prop-types'

type Props = {
  distributionErrors: Array<string>
}

export default function LotteryCheckResult(props: Props): JSX.Element {
  if (props.distributionErrors && props.distributionErrors.length > 0) {
    return (<div className="row">
      <hr/>
      <div className="col-sm-12">
        {props.distributionErrors.map((item, index) => <div key={`error-${index}`}>{item}</div>)}
      </div>
    </div>)
  } else {
    return (<div className="row">
      <hr/>
      <div className="col-sm-12">
        <div>Distribution is healthy</div>
      </div>
    </div>)
  }
}

LotteryCheckResult.propTypes = {
  distributionErrors: PropTypes.array.isRequired
}
