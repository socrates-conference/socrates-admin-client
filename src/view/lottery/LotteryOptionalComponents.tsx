import React from 'react'
import LotteryCheckResult from './LotteryCheckResult'
import LotteryLog from './LotteryLog'

type Props = {
  distributionErrors: Array<string>
  hasCheckedDistributionHealth: boolean
  registeringIsRunning: boolean
  log: Array<Object>
}

export default function LotteryOptionalComponents(props: Props): JSX.Element {
  return (<div>
    {
      props.hasCheckedDistributionHealth
        ? <LotteryCheckResult distributionErrors={props.distributionErrors}/>
        : null
    }
    {
      props.registeringIsRunning
        ? <LotteryLog log={props.log} registeringIsRunning={props.registeringIsRunning}/>
        : null
    }
  </div>)
}
