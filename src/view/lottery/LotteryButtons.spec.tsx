import React from 'react'
import {shallow} from 'enzyme'
import LotteryButtons from './LotteryButtons'

const spyCheck = jest.fn()
const spyRegister = jest.fn()
const spyStart = jest.fn()

describe('(Component) LotteryButtons', () => {
  const wrapper = shallow(
    <LotteryButtons
      onRegister={spyRegister} onCheckHealth={spyCheck} onStartDraw={spyStart}
      preventStartDraw={false} preventRegister={false} preventCheckHealth={false}
    />
  )

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
