import React from 'react'
import PropTypes from 'prop-types'
import './lotteryLog.css'

type Props = {
  registeringIsRunning: boolean
  log: Array<any>
}

export default function LotteryLog(props: Props): JSX.Element {
  if (props.registeringIsRunning) {
    return (
      <div id="lottery-log">
        <hr/>
        {
          props.log.map((item, index) => {
            const itemClass = item.status ? 'success' : 'failure'
            return (
              <div key={`log-${index}`} className="row">
                <div className="col-sm-12">
                  <div className={itemClass}>{item.text}</div>
                </div>
              </div>
            )
          })
        }
      </div>
    )
  } else {
    return (
      <div id="lottery-log">
        <hr/>
      </div>
    )
  }
}

LotteryLog.propTypes = {
  log: PropTypes.array.isRequired,
  registeringIsRunning: PropTypes.bool.isRequired
}
