import React from 'react'
import {mount} from 'enzyme'
import {LotteryContainer} from './LotteryContainer'
import {conference} from '../../test/testData'
import '../icons'

describe('(Component) LotteryContainer', () => {

  const getConference = jest.fn()
  const getInfo = jest.fn()
  const startLottery = jest.fn()
  const healthCheck = jest.fn()
  const register = jest.fn()

  afterEach(() => {
    getConference.mockReset()
    getInfo.mockReset()
    startLottery.mockReset()
    healthCheck.mockReset()
    register.mockReset()
  })

  describe('when conference is null', () => {
    beforeEach(() => {
      mount(
        <LotteryContainer
          conference={null} info={[]} getLotteryInfo={getInfo} getConference={getConference}
          startLotteryDraw={startLottery} distribution={[]} distributionErrors={[]}
          hasCheckedDistributionHealth={false} healthCheck={healthCheck} register={register}
          registeringIsRunning={false} log={[]}
        />)
    })
    it('constructor calls getConference', () => {
      expect(getConference.mock.calls.length).toBe(1)
    })
    it('constructor calls getLotteryInfo', () => {
      expect(getInfo.mock.calls.length).toBe(1)
    })
  })
  describe('when conference provided', () => {
    beforeEach(() => {
      mount(
        <LotteryContainer
          conference={conference} info={[]} getLotteryInfo={getInfo} getConference={getConference}
          startLotteryDraw={startLottery} distribution={[]} distributionErrors={[]}
          hasCheckedDistributionHealth={false} healthCheck={healthCheck} register={register}
          registeringIsRunning={false} log={[]}
        />)
    })
    it('constructor does not call getConference, when conference provided', () => {
      expect(getConference.mock.calls.length).toBe(0)
    })
    it('constructor calls getLotteryInfo', () => {
      expect(getInfo.mock.calls.length).toBe(1)
    })
  })
})
