import React from 'react'
import {shallow} from 'enzyme'
import LotteryCheckResult from './LotteryCheckResult'

describe('(Component) LotteryCheckResult without errors', () => {
  const wrapper = shallow(
    <LotteryCheckResult distributionErrors={[]}/>
  )

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
describe('(Component) LotteryCheckResult with errors', () => {
  const wrapper = shallow(
    <LotteryCheckResult distributionErrors={['One Error']}/>
  )

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
