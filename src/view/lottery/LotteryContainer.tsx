import React, {ReactElement} from 'react'
import {connect} from 'react-redux'
import {getConference} from '../commands/appCommand'
import type {Conference} from '../reducers/appReducer'
import moment from 'moment'
import {
  getLotteryInfo,
  healthCheck,
  register,
  startLotteryDraw
} from '../commands/lotteryCommand'
import Table from '../common/table/Table'
import LotteryButtons from './LotteryButtons'
import LotteryInfo from './LotteryInfo'
import LotteryOptionalComponents from './LotteryOptionalComponents'
import {ColumnDefinition} from '../common/table/TableTypes'
import {
  DrawItem,
  LotteryInfoRow
} from '../reducers/lotteryReducer'


type StateFromProps = {
  conference: Conference | undefined
  distribution: DrawItem[]
  distributionErrors: Array<string>
  hasCheckedDistributionHealth: boolean
  registeringIsRunning: boolean
  info: LotteryInfoRow[]
  log: Array<Object>
}

  type DispatchFromProps = {
    getConference: () => void
    getLotteryInfo: () => void
    healthCheck: (distribution: Array<DrawItem>) => void
    register: (distribution: Array<DrawItem>) => void
    startLotteryDraw: (conference: Conference | undefined, params: Array<LotteryInfoRow>) => void
  }

type Props = StateFromProps & DispatchFromProps
type Moment = moment.Moment
type ColDef = ColumnDefinition & {visible:boolean}
const columns:ColDef[] = [
  {header: 'Room type', field: 'roomType', visible: true},
  {header: 'Waiting', field: 'waitingPeople', visible: true},
  {header: 'Attending', field: 'registeredPeople', visible: true},
  {header: 'Reserved', field: 'spotsToReserve', displayClass: 'd-none d-md-table-cell', visible: true},
  {header: 'Free', field: 'free', visible: true}
]

export class LotteryContainer extends React.Component<Props> {

  constructor(props: Props) {
    super(props)
    if (!this.props.conference) {
      this.props.getConference()
    }
    this.props.getLotteryInfo()
  }

  render = (): ReactElement => {
    const lotteryDay: Moment | undefined = this.props.conference ? this.props.conference.lotteryDay : undefined
    const disableStartButton = !lotteryDay || moment().isBefore(lotteryDay)
    const disableCheckButton = disableStartButton || !this.props.distribution || this.props.distribution.length === 0
    const disableRegisterButton = disableCheckButton || !this.props.hasCheckedDistributionHealth
            || (!this.props.hasCheckedDistributionHealth && this.props.distributionErrors.length > 0)
    return (
      <div className="container">
        <LotteryInfo lotteryDay={lotteryDay}/>
        <Table columns={columns} data={this.props.info || []}/>
        <LotteryButtons
                      onStartDraw={() => this.props.startLotteryDraw(this.props.conference, this.props.info)}
                      onCheckHealth={() => this.props.healthCheck(this.props.distribution)}
                      onRegister={() => this.props.register(this.props.distribution)}
                      preventCheckHealth={disableCheckButton} preventRegister={disableRegisterButton}
                      preventStartDraw={disableStartButton}
              />
        <LotteryOptionalComponents
                      registeringIsRunning={this.props.registeringIsRunning} log={this.props.log}
                      distributionErrors={this.props.distributionErrors}
                      hasCheckedDistributionHealth={this.props.hasCheckedDistributionHealth}
              />
      </div>
    )
  }
}

const mapStateToProps = (state: any): StateFromProps => {
  return {
    conference: {...state.app}.conference,
    distribution: {...state.lottery}.distribution,
    distributionErrors: {...state.lottery}.distributionErrors,
    hasCheckedDistributionHealth: {...state.lottery}.hasCheckedDistributionHealth,
    info: {...state.lottery}.info,
    log: {...state.lottery}.log,
    registeringIsRunning: {...state.lottery}.registeringIsRunning
  }
}

const mapDispatchToProps: DispatchFromProps = {
  getConference, startLotteryDraw, getLotteryInfo, healthCheck, register
}

export default connect<StateFromProps, DispatchFromProps>(mapStateToProps, mapDispatchToProps)(LotteryContainer)


