import React, {SyntheticEvent} from 'react'

type Props = {
  onStartDraw: () => void
  onCheckHealth: () => void
  onRegister: () => void
  preventCheckHealth: boolean
  preventRegister: boolean
  preventStartDraw: boolean
}

export default function LotteryButtons(props: Props): JSX.Element {
  const _submit = (event: SyntheticEvent<HTMLFormElement>): void => {
    event.preventDefault()
    props.onStartDraw()
  }

  return (<div className="row">
    <div className="col-sm-12">
      <form onSubmit={_submit}>
        <button
                type="submit" className="btn btn-success mr-2 mb-2" disabled={props.preventStartDraw}>
          Generate draw distribution
        </button>
        <button
                type="button" className="btn btn-warning mr-2 mb-2" disabled={props.preventCheckHealth}
                onClick={props.onCheckHealth}>
          Distribution Health check
        </button>
        <button
                type="button" className="btn btn-danger mr-2 mb-2" disabled={props.preventRegister}
                onClick={props.onRegister}>
          Register drawn applications
        </button>
      </form>
    </div>
  </div>)
}
