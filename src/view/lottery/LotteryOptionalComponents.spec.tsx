import React from 'react'
import {shallow} from 'enzyme'
import LotteryOptionalComponents from './LotteryOptionalComponents'

describe('(Component) LotteryLog', () => {
  const wrapper = shallow(
    <LotteryOptionalComponents
      log={[{text:'some data', status: true}]} registeringIsRunning={true}
      distributionErrors={[]} hasCheckedDistributionHealth={true}
    />
  )

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
