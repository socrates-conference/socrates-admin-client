import SagaTester from 'redux-saga-tester'
import rootSaga from './rootSaga'
import * as sinon from 'sinon'
import * as api from '../requests/api'
import {
  ApplicationListItem,
  emptyApplicationListItem
} from '../applicantList/commons'
import applicantListReducer from '../reducers/applicantListReducer'
import {
  closeModal,
  executeAction,
  getApplications,
  getRoomTypes,
  handleAction
} from '../commands/applicationListCommand'
import ApplicationListEvents from '../events/applicantListEvents'
import {RoomType} from '../reducers/appReducer'
import {Optional} from '../../types'

type ApplicationListState = {
  applicationList: {
    currentRoomTypeFilter: string
    currentCommand: string
    currentRow: Optional<ApplicationListItem>
    errorMessage: string
    roomTypes: RoomType[]
    rows: ApplicationListItem[]
  }
}
const INITIAL_STATE: ApplicationListState = {
  applicationList: {
    currentRoomTypeFilter: 'all',
    currentCommand: '',
    currentRow: undefined,
    errorMessage: '',
    roomTypes: [],
    rows: []
  }
}
describe('applicationListSaga', () => {
  let sagaTester: SagaTester<ApplicationListState>
  describe('reacts to application list handle action', () => {


    beforeEach(() => {
      sagaTester = new SagaTester({initialState: INITIAL_STATE, reducers: applicantListReducer})
      sagaTester.start(rootSaga)
    })

    afterEach(() => {
      sagaTester.reset()
    })

    it('updating current command and current row', () => {
      sagaTester.dispatch(handleAction('testCommand', emptyApplicationListItem))
      expect(sagaTester.getState().applicationList.currentCommand).toBe('testCommand')
      expect(sagaTester.getState().applicationList.currentRow).toEqual(emptyApplicationListItem)
    })
  })

  describe('reacts to application list execute action', () => {
    describe('on delete', () => {
      const deleteStub = sinon.stub(api, 'deleteApplication')
      const initialState = {
        applicationList: {
          currentRoomTypeFilter: 'all',
          currentCommand: 'delete',
          currentRow: emptyApplicationListItem,
          errorMessage: '',
          roomTypes: [],
          rows: []
        }
      }
      beforeEach(() => {
        sagaTester = new SagaTester({initialState, reducers: applicantListReducer})
        sagaTester.start(rootSaga)
      })

      afterEach(() => {
        sagaTester.reset()
        deleteStub.reset()
      })

      it('deleting the current row', () => {
        deleteStub.returns(Promise.resolve(true))
        sagaTester.dispatch(executeAction(emptyApplicationListItem, 'delete', 'all'))
        return sagaTester.waitFor(ApplicationListEvents.EXECUTE_SUCCEEDED).then(() => {
          expect(deleteStub.getCalls().length).toBe(1)
          expect(sagaTester.getState().applicationList.currentCommand).toBe('')
          expect(sagaTester.getState().applicationList.currentRow).toBeUndefined()
        })
      })
      it('emitting failure event when delete fails', () => {
        deleteStub.returns(Promise.resolve(false))
        sagaTester.dispatch(executeAction(emptyApplicationListItem, 'delete', 'all'))
        return sagaTester.waitFor(ApplicationListEvents.EXECUTE_FAILED).then(() => {
          expect(deleteStub.getCalls().length).toBe(1)
          expect(sagaTester.getState().applicationList.currentCommand).toBe('error')
          expect(sagaTester.getState().applicationList.errorMessage).toBe('cannot execute command delete')
        })
      })
      it('emitting failure event when delete throws', () => {
        deleteStub.throws(new Error('Test Error'))
        sagaTester.dispatch(executeAction(emptyApplicationListItem, 'delete', 'all'))
        return sagaTester.waitFor(ApplicationListEvents.EXECUTE_FAILED).then(() => {
          expect(deleteStub.getCalls().length).toBe(1)
          expect(sagaTester.getState().applicationList.currentCommand).toBe('error')
          expect(sagaTester.getState().applicationList.errorMessage).toBe('Test Error')
        })
      })
    })
    describe('on edit', () => {
      const editStub = sinon.stub(api, 'updateApplication')
      const initialState = {
        applicationList: {
          currentRoomTypeFilter: 'all',
          currentCommand: 'edit',
          currentRow: emptyApplicationListItem,
          errorMessage: '',
          roomTypes: [],
          rows: []
        }
      }
      beforeEach(() => {
        sagaTester = new SagaTester({initialState, reducers: applicantListReducer})
        sagaTester.start(rootSaga)
      })

      afterEach(() => {
        sagaTester.reset()
        editStub.reset()
      })

      it('editing the current row', () => {
        editStub.returns(Promise.resolve(true))
        sagaTester.dispatch(executeAction(emptyApplicationListItem, 'edit', 'all'))
        return sagaTester.waitFor(ApplicationListEvents.EXECUTE_SUCCEEDED).then(() => {
          expect(editStub.getCalls().length).toBe(1)
          expect(sagaTester.getState().applicationList.currentCommand).toBe('')
          expect(sagaTester.getState().applicationList.currentRow).toBeUndefined()
        })
      })
      it('emitting failure event when edit fails', () => {
        editStub.returns(Promise.resolve(false))
        sagaTester.dispatch(executeAction(emptyApplicationListItem, 'edit', 'all'))
        return sagaTester.waitFor(ApplicationListEvents.EXECUTE_FAILED).then(() => {
          expect(editStub.getCalls().length).toBe(1)
          expect(sagaTester.getState().applicationList.currentCommand).toBe('error')
          expect(sagaTester.getState().applicationList.errorMessage).toBe('cannot execute command edit')
        })
      })
      it('emitting failure event when edit throws', () => {
        editStub.throws(new Error('Test Error'))
        sagaTester.dispatch(executeAction(emptyApplicationListItem, 'edit', 'all'))
        return sagaTester.waitFor(ApplicationListEvents.EXECUTE_FAILED).then(() => {
          expect(editStub.getCalls().length).toBe(1)
          expect(sagaTester.getState().applicationList.currentCommand).toBe('error')
          expect(sagaTester.getState().applicationList.errorMessage).toBe('Test Error')
        })
      })
    })
    describe('on register', () => {
      const registerStub = sinon.stub(api, 'registerApplication')
      const initialState = {
        applicationList: {
          currentRoomTypeFilter: 'all',
          currentCommand: 'register',
          currentRow: emptyApplicationListItem,
          errorMessage: '',
          roomTypes: [],
          rows: []
        }
      }
      beforeEach(() => {
        sagaTester = new SagaTester({initialState, reducers: applicantListReducer})
        sagaTester.start(rootSaga)
      })

      afterEach(() => {
        sagaTester.reset()
        registerStub.reset()
      })

      it('deleting the current row', () => {
        registerStub.returns(Promise.resolve(true))
        sagaTester.dispatch(executeAction(emptyApplicationListItem, 'register', 'all'))
        return sagaTester.waitFor(ApplicationListEvents.EXECUTE_SUCCEEDED).then(() => {
          expect(registerStub.getCalls().length).toBe(1)
          expect(sagaTester.getState().applicationList.currentCommand).toBe('')
          expect(sagaTester.getState().applicationList.currentRow).toBeUndefined()
        })
      })
      it('emitting failure event when register fails', () => {
        registerStub.returns(Promise.resolve(false))
        sagaTester.dispatch(executeAction(emptyApplicationListItem, 'register', 'all'))
        return sagaTester.waitFor(ApplicationListEvents.EXECUTE_FAILED).then(() => {
          expect(registerStub.getCalls().length).toBe(1)
          expect(sagaTester.getState().applicationList.currentCommand).toBe('error')
          expect(sagaTester.getState().applicationList.errorMessage).toBe('cannot execute command register')
        })
      })
      it('emitting failure event when register throws', () => {
        registerStub.throws(new Error('Test Error'))
        sagaTester.dispatch(executeAction(emptyApplicationListItem, 'register', 'all'))
        return sagaTester.waitFor(ApplicationListEvents.EXECUTE_FAILED).then(() => {
          expect(registerStub.getCalls().length).toBe(1)
          expect(sagaTester.getState().applicationList.currentCommand).toBe('error')
          expect(sagaTester.getState().applicationList.errorMessage).toBe('Test Error')
        })
      })
    })
  })

  describe('reacts to application list close modal', () => {
    const initialState = {
      applicationList: {
        currentRoomTypeFilter: 'all',
        currentCommand: '',
        currentRow: undefined,
        errorMessage: '',
        roomTypes: [],
        rows: []
      }
    }
    beforeEach(() => {
      sagaTester = new SagaTester({initialState, reducers: applicantListReducer})
      sagaTester.start(rootSaga)
    })

    afterEach(() => {
      sagaTester.reset()
    })

    it('deleting current command and current row', () => {
      sagaTester.dispatch(closeModal())
      expect(sagaTester.getState().applicationList.currentCommand).toBe('')
      expect(sagaTester.getState().applicationList.currentRow).toBeUndefined()
    })
  })

  describe('reacts to application list get room types', () => {
    const initialState = {
      applicationList: {
        currentRoomTypeFilter: 'all',
        currentCommand: '',
        currentRow: undefined,
        errorMessage: '',
        roomTypes: [],
        rows: []
      }
    }
    const readOptionsStub = sinon.stub(api, 'readOptions')

    beforeEach(() => {
      sagaTester = new SagaTester({initialState, reducers: applicantListReducer})
      sagaTester.start(rootSaga)
    })

    afterEach(() => {
      readOptionsStub.reset()
      sagaTester.reset()
    })

    it('updating state event when data available', () => {
      readOptionsStub.returns(Promise.resolve([{value: '1', title: 'test room type'}]))
      sagaTester.dispatch(getRoomTypes())
      return sagaTester.waitFor(ApplicationListEvents.GET_ROOM_TYPES_SUCCEEDED, false).then(() => {
        expect(readOptionsStub.getCalls().length).toBe(1)
        expect(sagaTester.getState().applicationList.roomTypes.length).toBe(1)
      })
    })
    it('updating state when no data available', () => {
      readOptionsStub.returns(Promise.resolve([]))
      sagaTester.dispatch(getRoomTypes())
      return sagaTester.waitFor(ApplicationListEvents.GET_ROOM_TYPES_SUCCEEDED, false).then(() => {
        expect(readOptionsStub.getCalls().length).toBe(1)
        expect(sagaTester.getState().applicationList.roomTypes.length).toBe(0)
      })
    })
    it('emitting failure event when an error occurs', () => {
      readOptionsStub.throws(new Error())
      sagaTester.dispatch(getRoomTypes())
      return sagaTester.waitFor(ApplicationListEvents.GET_ROOM_TYPES_FAILED, false).then(() => {
        expect(readOptionsStub.getCalls().length).toBe(1)
        const actions = sagaTester.getCalledActions()
        expect(actions.filter(action => action.type === ApplicationListEvents.GET_ROOM_TYPES_FAILED)).toHaveLength(1)
      })
    })
  })

  describe('reacts to application list get applications without filter', () => {
    const initialState = {
      applicationList: {
        currentRoomTypeFilter: 'all',
        currentCommand: '',
        currentRow: undefined,
        errorMessage: '',
        roomTypes: [],
        rows: []
      }
    }
    const readApplicantsStub = sinon.stub(api, 'readApplications')

    beforeEach(() => {
      sagaTester = new SagaTester({initialState, reducers: applicantListReducer})
      sagaTester.start(rootSaga)
    })

    afterEach(() => {
      readApplicantsStub.reset()
      sagaTester.reset()
    })

    it('without filtering updating state when data available', () => {
      // @ts-ignore
      readApplicantsStub.returns(Promise.resolve([{id: 4711}]))
      sagaTester.dispatch(getApplications('all'))
      return sagaTester.waitFor(ApplicationListEvents.GET_APPLICATIONS_SUCCEEDED).then(() => {
        expect(readApplicantsStub.getCalls().length).toBeGreaterThan(0)
        expect(sagaTester.getState().applicationList.rows.length).toBe(1)
      })
    })
    it('updating state when no data available', () => {
      readApplicantsStub.returns(Promise.resolve([]))
      sagaTester.dispatch(getApplications('all'))
      return sagaTester.waitFor(ApplicationListEvents.GET_APPLICATIONS_SUCCEEDED).then(() => {
        expect(readApplicantsStub.getCalls().length).toBe(1)
        expect(sagaTester.getState().applicationList.rows.length).toBe(0)
      })
    })
    it('emitting failure event when an error occurs', () => {
      readApplicantsStub.throws(new Error())
      sagaTester.dispatch(getApplications('all'))
      return sagaTester.waitFor(ApplicationListEvents.GET_APPLICATIONS_FAILED).then(() => {
        expect(readApplicantsStub.getCalls().length).toBe(1)
        const actions = sagaTester.getCalledActions()
        expect(actions.filter(action => action.type === ApplicationListEvents.GET_APPLICATIONS_FAILED)).toHaveLength(1)
      })
    })
  })

  describe('reacts to application list get applications with filter', () => {
    const initialState = {
      applicationList: {
        currentRoomTypeFilter: 'all',
        currentCommand: '',
        currentRow: undefined,
        errorMessage: '',
        roomTypes: [],
        rows: []
      }
    }
    const readApplicantsStub = sinon.stub(api, 'readApplicationsByRoomType')

    beforeEach(() => {
      sagaTester = new SagaTester({initialState, reducers: applicantListReducer})
      sagaTester.start(rootSaga)
    })

    afterEach(() => {
      readApplicantsStub.reset()
      sagaTester.reset()
    })

    it('without filtering emitting success event when succeed', () => {
      // @ts-ignore
      readApplicantsStub.returns(Promise.resolve([{id: 4711}]))
      sagaTester.dispatch(getApplications('single'))
      return sagaTester.waitFor(ApplicationListEvents.GET_APPLICATIONS_SUCCEEDED).then(() => {
        expect(readApplicantsStub.getCalls().length).toBe(1)
        expect(sagaTester.getState().applicationList.rows.length).toBe(1)
      })
    })
    it('updating state when no data available', () => {
      readApplicantsStub.returns(Promise.resolve([]))
      sagaTester.dispatch(getApplications('single'))
      return sagaTester.waitFor(ApplicationListEvents.GET_APPLICATIONS_SUCCEEDED).then(() => {
        expect(readApplicantsStub.getCalls().length).toBe(1)
        expect(sagaTester.getState().applicationList.rows.length).toBe(0)
      })
    })
    it('emitting failure event when an error occurs', () => {
      readApplicantsStub.throws(new Error())
      sagaTester.dispatch(getApplications('single'))
      return sagaTester.waitFor(ApplicationListEvents.GET_APPLICATIONS_FAILED).then(() => {
        expect(readApplicantsStub.getCalls().length).toBe(1)
        const actions = sagaTester.getCalledActions()
        expect(actions.filter(action => action.type === ApplicationListEvents.GET_APPLICATIONS_FAILED)).toHaveLength(1)
      })
    })
  })
})
