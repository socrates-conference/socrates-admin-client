import SagaTester from 'redux-saga-tester'
import rootSaga from './rootSaga'
import * as sinon from 'sinon'
import * as api from '../requests/api'
import {getRegistrationsByRoomType} from '../commands/dashboardCommand'
import dashboardReducer from '../reducers/dashboardReducer'
import {conference} from '../../test/testData'

describe('rootSaga', () => {
  let sagaTester: SagaTester
  describe('reacts to get registrations by room type', () => {
    const INITIAL_STATE = {
      appState: {
        dashboard: undefined
      }
    }
    const readRegistrationsByRoomType = sinon.stub(api, 'readRegistrationsByRoomType')
    const readConference = sinon.stub(api, 'readConference')

    beforeEach(() => {
      sagaTester = new SagaTester({INITIAL_STATE, reducers: dashboardReducer})
      sagaTester.start(rootSaga)
    })

    afterEach(() => {
      readRegistrationsByRoomType.reset()
      sagaTester.reset()
    })

    it('updating state when data available', () => {
      readConference.returns(conference)
      readRegistrationsByRoomType.returns([])
      sagaTester.dispatch(getRegistrationsByRoomType())
      expect(readRegistrationsByRoomType.getCalls().length).toBe(1)
      expect(sagaTester.getState().dashboard.occupationRows.length).toBe(5)
      expect(sagaTester.getState().dashboard.diversityRows.length).toBe(4)
    })
    it('clearing state when no conference available', () => {
      readConference.returns(undefined)
      readRegistrationsByRoomType.returns([])
      sagaTester.dispatch(getRegistrationsByRoomType())
      expect(readRegistrationsByRoomType.getCalls().length).toBe(0)
      expect(sagaTester.getState().dashboard.occupationRows.length).toBe(0)
      expect(sagaTester.getState().dashboard.diversityRows.length).toBe(0)
    })
    it('clearing state when error occurs', () => {
      readConference.throws(new Error())
      readRegistrationsByRoomType.returns([])
      sagaTester.dispatch(getRegistrationsByRoomType())
      expect(readRegistrationsByRoomType.getCalls().length).toBe(0)
      expect(sagaTester.getState().dashboard.occupationRows.length).toBe(0)
      expect(sagaTester.getState().dashboard.diversityRows.length).toBe(0)
    })
  })
})
