import type {RegistrationsByRoomType} from '../reducers/dashboardReducer'

export function calculateOccupied(roomType: string, occupation: Array<RegistrationsByRoomType>): number {
  if (occupation && occupation.length > 0) {
    const roomsOccupied = occupation
      .filter(item => item.roomType === roomType)
    return roomsOccupied.reduce((accumulator, item) => accumulator + item.counter, 0)
  } else {
    return 0
  }
}

export function calculateDiversity(roomType: string, occupation: Array<RegistrationsByRoomType>): number {
  const roomDiversity = occupation
    .filter(
      item => item.roomType === roomType && item.diversityReason.toLowerCase() !== 'no'
        && item.diversityReason.trim() !== '')
  return roomDiversity.reduce((accumulator, item) => accumulator + item.counter, 0)
}

