import authenticationReducer, {AuthenticationState} from '../reducers/authenticationReducer'
import rootSaga from './rootSaga'
import {routeTo} from '../commands/routingCommand'
import SagaTester from 'redux-saga-tester'
type SagaState = { authentication: AuthenticationState }

describe('routeToSaga', () => {
  let sagaTester: SagaTester<SagaState>
  const INITIAL_STATE = {
    authentication: {
      token: '',
      userName: 'Guest',
      isAdministrator: false,
      hasFinished: false
    }
  }
  beforeEach(() => {
    sagaTester = new SagaTester({initialState: INITIAL_STATE, reducers: authenticationReducer})
    sagaTester.start(rootSaga)
  })

  afterEach(() => {
    sagaTester.reset()
  })


  it('reacts to routeTo putting the new destination to the router', () => {
    sagaTester.dispatch(routeTo('/home'))
    sagaTester.waitFor('@@router/CALL_HISTORY_METHOD').then(() => {
      const pushAction = sagaTester.getLatestCalledAction()
      expect(pushAction.payload.method).toBe('push')
      expect(pushAction.payload.args[0]).toBe('/home')
    })
  })
})
