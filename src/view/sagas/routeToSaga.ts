import {push} from 'react-router-redux'
import {put} from 'redux-saga/effects'

export function* routeToSaga(action: any): Iterable<any> {
  yield put(push(action.url))
}
