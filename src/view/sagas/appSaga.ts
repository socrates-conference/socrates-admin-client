import {call, put} from 'redux-saga/effects'
import {getConferenceFailed, getConferenceSucceeded} from '../events/appEvents'
import * as api from '../requests/api'
import type {Conference} from '../reducers/appReducer'
import {Optional} from '../../types'

export function* conferenceGetData(): Iterable<any> {
  try {
    const conference: Optional<Conference> = yield call(api.readConference)
    if(conference) {
      yield put(getConferenceSucceeded(conference))
    } else {
      yield put(getConferenceFailed(new Error('No conference data available.')))
    }
  } catch(error) {
    yield put(getConferenceFailed(error))
  }
}
