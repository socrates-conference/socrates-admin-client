import {calculateDiversity, calculateOccupied} from './commons'

describe('Commons:', () => {
  describe('calculateOccupied', () => {
    const data = [
      {counter: 3, roomType: 'bedInDouble', diversityReason: 'yes'},
      {counter: 2, roomType: 'bedInDouble', diversityReason: 'no'}
    ]
    it('returns zero, when there is no registration for the room type', () => {
      expect(calculateOccupied('single', data)).toBe(0)
    })
    it('returns the amount of occupied spots for a given room type', () => {
      expect(calculateOccupied('bedInDouble', data)).toBe(5)
    })
  })
  describe('calculateDiversity', () => {
    const data = [
      {counter: 3, roomType: 'single', diversityReason: 'no'},
      {counter: 3, roomType: 'bedInDouble', diversityReason: 'yes'},
      {counter: 2, roomType: 'bedInDouble', diversityReason: 'no'}
    ]
    it('returns zero, when there is no diversity registration for the room type', () => {
      expect(calculateDiversity('single', data)).toBe(0)
    })
    it('returns the amount of diversity occupied spots for a given room type', () => {
      expect(calculateDiversity('bedInDouble', data)).toBe(3)
    })
  })
})