import type {RoomType} from '../reducers/appReducer'
import {calculateDiversity, calculateOccupied} from './commons'
import type {RegistrationsByRoomType} from '../reducers/dashboardReducer'
import type {DrawItem, LotteryInfoRow} from '../reducers/lotteryReducer'
import type {ApplicationListItem} from '../applicantList/commons'
import {Optional} from '../../types'

export function createLotteryInfos(roomTypes: Array<RoomType>, applications: Array<Record<string,any>>,
  participants: Array<RegistrationsByRoomType>): Array<LotteryInfoRow> {
  const infos: Array<LotteryInfoRow> = []
  roomTypes.forEach((roomType) => {
    const waiting = applications.find(item => item.roomType === roomType.id)
    const registered = calculateOccupied(roomType.id, participants)
    const diversity = calculateDiversity(roomType.id, participants)
    const freeSpots = (roomType.numberOfRooms * roomType.peoplePerRoom) - registered -
      (roomType.reservedOrganization + roomType.reservedSponsors)
    infos.push(
      {
        roomTypeId: roomType.id,
        roomType: roomType.display,
        spotsToReserve: roomType.reservedOrganization + roomType.reservedSponsors,
        waitingPeople: (waiting ? waiting.counter : 0),
        registeredPeople: registered,
        diversityPeople: diversity,
        free: freeSpots
      }
    )
  })
  return infos
}

export function getRandomInt(min: number, max: number): number {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min)) + min
}

export function isDiversity(app: Partial<ApplicationListItem>): boolean {
  return app.diversityReason !== 'no' && app.diversityReason?.trim() !== ''
}

export function fillDraw(drawItem: Optional<DrawItem>, applications: Optional<Array<ApplicationListItem>>,
  alreadySelectedPersons: number[]): void {
  if (applications && drawItem && drawItem.info.waitingPeople > 0 && drawItem.info.free > 0) {
    drawItem.applications = applications.slice()
    const filteredApplications = applications.filter(app => alreadySelectedPersons.indexOf(app.personId) < 0)
    const freePlaces = drawItem.info.free
    let numberOfDraws = Math.min(filteredApplications.length, freePlaces)
    const diversityApplications = filteredApplications.filter(app => isDiversity(app))
    if (diversityApplications && diversityApplications.length > 0) {
      const numberOfDiversityDraws = Math.min(diversityApplications.length, freePlaces)
      for (let counter = 1; counter <= numberOfDiversityDraws; counter++) {
        const random = getRandomInt(0, diversityApplications.length - 1)
        const application = diversityApplications[random]
        alreadySelectedPersons.push(application.personId)
        drawItem.drawn.push(application.applicantId)
        const indexOfApplication = filteredApplications.findIndex(app => app.applicantId === application.applicantId)
        filteredApplications.splice(indexOfApplication, 1)
        diversityApplications.splice(random, 1)
      }
      numberOfDraws -= numberOfDiversityDraws
    }

    for (let counter = 1; counter <= numberOfDraws; counter++) {
      const random = getRandomInt(0, filteredApplications.length - 1)
      const application = filteredApplications[random]
      alreadySelectedPersons.push(application.personId)
      drawItem.drawn.push(application.applicantId)
      filteredApplications.splice(random, 1)
    }
  }
}

export function checkApplicationRoomTypeSelection(distribution: Array<DrawItem>): Array<string> {
  // Check that all drawn applications has an application object for the room type
  const errors: Array<string> = []
  distribution.forEach(result => {
    if (result.drawn.length > 0) {
      result.drawn.forEach((id: number) => {
        const filtered = result.applications.filter(app => app.applicantId === id)
        const presentOnlyOnce = filtered.length === 1
        if (!presentOnlyOnce) {
          errors.push('Error: Drawn application id ' + id + ' has no application for room type '
            + result.info.roomType)
        }
      })
    }
  })
  return errors
}

export function checkApplicationsArePresentOnlyOnce(distribution: Array<DrawItem>): Array<string> {
  // Check that all applications are present only once
  const errors: Array<string> = []
  const applicantIds: number[] = []
  distribution.forEach(result => {
    if (result.drawn.length > 0) {
      result.drawn.forEach((id: number) => {
        if (applicantIds.indexOf(id) >= 0) {
          errors.push('Error: application id ' + id + ' is present more than once')
        } else {
          applicantIds.push(id)
        }
      })
    }
  })
  return errors
}

export function checkPersonsArePresentOnlyOnce(distribution: Array<DrawItem>): Array<string> {
  const errors: Array<string> = []
  const personIds: number[] = []
  distribution.forEach(result => {
    if (result.drawn.length > 0) {
      result.drawn.forEach((id: number) => {
        const filtered = result.applications.filter(app => app.applicantId === id)
        if (filtered && filtered.length === 1) {
          if (personIds.indexOf(filtered[0].personId) >= 0) {
            errors.push('Error: person id ' + filtered[0].personId + ' is present more than once')
          } else {
            personIds.push(filtered[0].personId)
          }
        } else {
          errors.push('Error: cannot find unique application for drawn value!')
        }
      })
    }
  })
  return errors
}
