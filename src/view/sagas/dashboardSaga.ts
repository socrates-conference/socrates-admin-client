import type {Conference} from '../reducers/appReducer'
import {call, put} from 'redux-saga/effects'
import {getConferenceSucceeded} from '../events/appEvents'
import {getRegistrationsByRoomTypeFailed, getRegistrationsByRoomTypeSucceeded} from '../events/dashboardEvents'
import type {RegistrationsByRoomType} from '../reducers/dashboardReducer'
import * as api from '../requests/api'
import {prepareDiversityRows, prepareOccupationRows} from './dashboardSagaHelper'
import {Optional} from '../../types'

export function* dashboardGetRegistrationsByRoomType(): Iterable<any> {
  try {
    const conference: Optional<Conference> = yield call(api.readConference)
    if(conference) {
      yield put(getConferenceSucceeded(conference))
      const occupation: Optional<Array<RegistrationsByRoomType>> = yield call(api.readRegistrationsByRoomType)
      // @ts-ignore
      const occupationRows: Optional<Array<Object>> = yield call(prepareOccupationRows, conference, occupation)
      // @ts-ignore
      const diversityRows: Optional<Array<Object>> = yield call(prepareDiversityRows, conference, occupation)
      yield put(getRegistrationsByRoomTypeSucceeded(occupationRows || [], diversityRows || []))
    } else {
      yield put(getRegistrationsByRoomTypeFailed('No conference data available'))
    }
  } catch (err) {
    yield put(getRegistrationsByRoomTypeFailed(err))
  }
}
