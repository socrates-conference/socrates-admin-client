import {applicationListCommandIdToApiCall} from './resolveApiCallFromCommand'
import {ApplicationListCommandId} from '../commands/applicationListCommand'
import {deleteApplication, registerApplication, updateApplication} from '../requests/api'

describe('resolveApiCallFromCommand', () => {
  describe('can resolve api calls for', () => {
    it('delete', () => {
      const apiCall = applicationListCommandIdToApiCall(ApplicationListCommandId.DELETE)
      expect(apiCall).toBe(deleteApplication)
    })
    it('edit', () => {
      const apiCall = applicationListCommandIdToApiCall(ApplicationListCommandId.EDIT)
      expect(apiCall).toBe(updateApplication)
    })
    it('register', () => {
      const apiCall = applicationListCommandIdToApiCall(ApplicationListCommandId.REGISTER)
      expect(apiCall).toBe(registerApplication)
    })
  })
  it('throws on unknown command id', () => {
    expect(() => applicationListCommandIdToApiCall('unknown'))
      .toThrow(new Error('Unknown action \'unknown\' for application list.'))
  })
})
