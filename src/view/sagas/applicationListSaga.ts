import {
  applicationListExecutionCanceled,
  applicationListExecutionFailed,
  applicationListExecutionSucceed,
  getApplicationsFailed,
  getApplicationsSucceed,
  getRoomTypesFailed,
  getRoomTypesSucceed,
  handledAction,
  updateRoomTypeFilter
} from '../events/applicantListEvents'
import {
  call,
  put
} from 'redux-saga/effects'
import * as api from '../requests/api'
import {
  CommandAction,
  getApplications,
  QueryAction
} from '../commands/applicationListCommand'
import {applicationListCommandIdToApiCall} from './resolveApiCallFromCommand'
import {
  ensure,
  Optional
} from '../../types'

export function* applicationListHandleActionSaga(action: CommandAction): Iterable<any> {
  yield put(handledAction(action.commandId, action.data))
}

export function* applicationListCloseModalSaga(): Iterable<any> {
  yield put(applicationListExecutionCanceled())
}

export function* applicationListExecuteSaga(action: CommandAction): Iterable<any> {
  const apiCall = applicationListCommandIdToApiCall(action.commandId)
  try {
    const success = yield call(apiCall, action.data)
    if (success) {
      yield put(applicationListExecutionSucceed())
      yield put(getApplications(ensure(action.roomTypeFilter)))
    } else {
      yield put(
        applicationListExecutionFailed(new Error(`cannot execute command ${action.commandId}`)))
    }
  } catch (error) {
    yield put(applicationListExecutionFailed(error))
  }
}

export function* applicationListGetRoomTypes(): Iterable<any> {
  try {
    const options: Optional<Array<Object>> = yield call(api.readOptions)
    if (options !== undefined) {
      yield put(getRoomTypesSucceed(options))
    } else {
      yield put(getRoomTypesFailed('No room types available.'))
    }
  } catch (error) {
    yield put(getRoomTypesFailed(error))
  }
}

export function* applicationListChangeRoomTypeFilter(action: QueryAction): Iterable<any> {
  yield put(updateRoomTypeFilter(ensure(action.filter)))
  yield put(getApplications(ensure(action.filter)))
}

export function* applicationListGetApplications(action: QueryAction): Iterable<any> {
  try {
    let rows: Optional<Array<Object>>
    if (action.filter === 'all') {
      rows = yield call(api.readApplications)
    } else {
      rows = yield call(api.readApplicationsByRoomType, ensure(action.filter))
    }
    if (rows !== undefined) {
      yield put(getApplicationsSucceed(rows || []))
    } else {
      yield put(getApplicationsFailed('No applications available.'))
    }
  } catch (error) {
    yield put(getApplicationsFailed(error))
  }
}

