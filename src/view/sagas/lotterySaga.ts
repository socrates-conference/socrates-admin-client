import {call, put} from 'redux-saga/effects'
import * as api from '../requests/api'
import {
  distributeSlotsSucceeded,
  distributionHealthCheckFailed,
  distributionHealthCheckSucceeded,
  readInfoFailed,
  readInfoSucceeded,
  registrationDone, registrationStarted, registered,
  startLotteryDrawFailed
} from '../events/lotteryEvents'
import type {Conference, RoomType} from '../reducers/appReducer'
import type {DrawItem, LotteryInfoRow} from '../reducers/lotteryReducer'
import {
  checkApplicationRoomTypeSelection,
  checkApplicationsArePresentOnlyOnce,
  checkPersonsArePresentOnlyOnce,
  createLotteryInfos,
  fillDraw
} from './lotterySagaHelper'
import moment from 'moment'
import {
  ensure,
  Optional
} from '../../types'
import {RegistrationsByRoomType} from '../reducers/dashboardReducer'
import {ApplicationListItem} from '../applicantList/commons'

export function* readLotteryInfoSaga(): Iterable<any> {
  try {
    const conference: Optional<Conference> = yield call(api.readConference)
    if (conference) {
      const conf: Conference = ensure(conference)
      const a: Optional<Array<Record<string,any>>> = yield call(api.readApplicationsForLottery)
      const applications: Array<Record<string,any>> = a || []
      const p: Optional<Array<Record<string,any>>> = yield call(api.readRegistrationsByRoomType)
      const participants: Array<RegistrationsByRoomType> = p || []
      const infos: Array<Record<string,any>> = createLotteryInfos(conf.roomTypes, applications, participants)
      yield put(readInfoSucceeded(infos))
    } else {
      yield put(readInfoFailed(new Error('No conference data available.')))
    }
  } catch (err) {
    yield put(readInfoFailed(err))
  }
}

export function* startLotteryDrawSaga(action: any): Iterable<any> {
  try {
    if (action.conference && moment().isAfter(action.conference.lotteryDay)) {
    //if(action.conference) {
      const infos: Optional<Array<LotteryInfoRow>> = action.info
      let roomTypes = []
      const draw: Array<DrawItem> = []
      const alreadySelectedPersons: number[] = []
      if (infos) {
        infos.forEach(info => draw.push({info, applications: [], drawn: []}))
        roomTypes = action.conference.roomTypes.slice()
        roomTypes.sort((a:RoomType, b:RoomType) => a.lotterySortOrder - b.lotterySortOrder)
      }
      for (let index = 0; index < roomTypes.length; index++) {
        const item: RoomType = roomTypes[index]
        const roomTypeDrawInfo = draw.find(drawItem => drawItem.info.roomTypeId === item.id)
        const temp = yield call(api.readApplicationsByRoomType, item.id)
        fillDraw(roomTypeDrawInfo, temp, alreadySelectedPersons)
      }
      yield put(distributeSlotsSucceeded(draw))
    } else {
      yield put(startLotteryDrawFailed(new Error('No conference data available or to early to start a draw.')))
    }
  } catch (err) {
    yield put(startLotteryDrawFailed(err))
  }
}

export function* healthCheckSaga(action: any): Iterable<any> {
  const errors = []
  if (action.distribution) {
    const selectionErrors = checkApplicationRoomTypeSelection(action.distribution)
    errors.push(...selectionErrors)
    const appDuplicates = checkApplicationsArePresentOnlyOnce(action.distribution)
    errors.push(...appDuplicates)
    const personDuplicates = checkPersonsArePresentOnlyOnce(action.distribution)
    errors.push(...personDuplicates)
  }
  if (errors.length === 0) {
    yield put(distributionHealthCheckSucceeded())
  } else {
    yield put(distributionHealthCheckFailed(errors))
  }
}

export function* registerSaga(action: any): Iterable<any> {
  if(action.distribution) {
    const results = []
    yield put(registrationStarted())
    for (const result of action.distribution) {
      for (const draw of result.drawn) {
        const application = result.applications.find((app:ApplicationListItem) => app.applicantId === draw)
        const success: Optional<boolean> = yield call(api.registerApplication, application)
        results.push({application, registered: success})
        let text = `Registration for ${application.name} (${application.email}) `
        text += success ? 'successful' : 'failed'
        yield put(registered(text, Boolean(success)))
      }
    }
    yield put(registrationDone(results))
  }
}
