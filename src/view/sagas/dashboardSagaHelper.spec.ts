import {prepareDiversityRows, prepareOccupationRows} from './dashboardSagaHelper'
import {Conference} from '../reducers/appReducer'
import moment from 'moment'

const conference: Conference = {
  dinnerPrice: 0,
  endApplications: moment(),
  endDate: moment(),
  flatFridaySaturday: 0,
  flatSunday: 0,
  flatThursday: 0,
  lengthsOfStay: [],
  lotteryDay: moment(),
  nameLong: '',
  startApplications: moment(),
  startDate: moment(),
  year: 0,
  name: 'Test',
  roomTypes: [
    {
      id: 'single', display: 'Single', nightPricePerPerson: 62.50, numberOfRooms: 120, peoplePerRoom: 1,
      reservedOrganization: 10, reservedSponsors: 50, lotterySortOrder:0
    },
    {
      id: 'bedInDouble', display: 'Double shared', nightPricePerPerson: 47.95, numberOfRooms: 16, peoplePerRoom: 2,
      reservedOrganization: 4, reservedSponsors: 0, lotterySortOrder:0
    },
    {
      id: 'juniorExclusively', display: 'Junior (exclusively)', nightPricePerPerson: 62.50, numberOfRooms: 42,
      peoplePerRoom: 1, reservedOrganization: 0, reservedSponsors: 0, lotterySortOrder:0
    },
    {
      id: 'juniorShared', display: 'Junior (shared)', nightPricePerPerson: 45.45, numberOfRooms: 14,
      peoplePerRoom: 2, reservedOrganization: 0, reservedSponsors: 0, lotterySortOrder:0
    }
  ]
}

describe('dashboard saga helper:', () => {
  describe('calculateOccupied', () => {
    it('returns one row per room type plus one totals row', () => {
      const occupationRows = prepareOccupationRows(conference, [])
      expect(occupationRows.length).toBe(5)
    })
  })
  describe('calculateDiversity', () => {
    it('returns one row per room type', () => {
      const diversityRows = prepareDiversityRows(conference, [])
      expect(diversityRows.length).toBe(4)
    })
  })
})
