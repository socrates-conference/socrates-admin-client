import type {Conference} from '../reducers/appReducer'
import type {RegistrationsByRoomType} from '../reducers/dashboardReducer'
import {calculateDiversity, calculateOccupied} from './commons'

export type OccupationRow = {
  roomType: string
  numberOfRooms: number
  capacity: number
  reservedOrganization: number
  reservedSponsors: number
  occupied: number
  free: number
}

export type DiversityRow = {
  roomType: string
  minimumDiversity: number
  currentDiversity: number
}

export function prepareOccupationRows(conference: Conference, data: Array<RegistrationsByRoomType>): Array<Object> {
  const rows = []
  if (conference && conference.roomTypes && conference.roomTypes.length > 0) {
    const totals: OccupationRow = {
      roomType: 'Total', numberOfRooms: 0, capacity: 0, reservedOrganization: 0, reservedSponsors: 0,
      occupied: 0, free: 0
    }
    conference.roomTypes.forEach(room => {
      const occupied = calculateOccupied(room.id, data)
      const capacity = room.numberOfRooms * room.peoplePerRoom
      const item = {
        roomType: room.display,
        numberOfRooms: room.numberOfRooms,
        capacity: capacity,
        reservedOrganization: room.reservedOrganization,
        reservedSponsors: room.reservedSponsors,
        occupied: occupied,
        free: capacity - room.reservedOrganization - room.reservedSponsors - occupied
      }
      rows.push(item)
      totals.numberOfRooms += item.numberOfRooms
      totals.capacity += item.capacity
      totals.reservedOrganization += item.reservedOrganization
      totals.reservedSponsors += item.reservedSponsors
      totals.occupied += item.occupied
      totals.free += item.free
    })
    rows.push(totals)
  }
  return rows
}

export function prepareDiversityRows(conference: Conference, data: Array<RegistrationsByRoomType>): Array<Object> {
  const rows: DiversityRow[] = []
  if (conference && conference.roomTypes && conference.roomTypes.length > 0) {
    conference.roomTypes.forEach(room => {
      const currentDiversity = calculateDiversity(room.id, data)
      const capacity = room.numberOfRooms * room.peoplePerRoom
      const item: DiversityRow = {
        roomType: room.display,
        minimumDiversity: Math.ceil(capacity * 0.5),
        currentDiversity: currentDiversity
      }
      rows.push(item)
    })
  }
  return rows
}

