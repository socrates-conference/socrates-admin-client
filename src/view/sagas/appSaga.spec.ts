import SagaTester from 'redux-saga-tester'
import rootSaga from './rootSaga'
import * as sinon from 'sinon'
import * as api from '../requests/api'
import {getConference} from '../commands/appCommand'
import appReducer, {Conference} from '../reducers/appReducer'
import {Optional} from '../../types'
import AppEvents from '../events/appEvents'

type AppState = {
  app: {
    conference: Optional<Conference>
  }
}

describe('rootSaga', () => {
  let sagaTester: SagaTester<AppState>
  describe('reacts to get conference data', () => {
    const initialState: AppState = {
      app: {
        conference: undefined
      }
    }
    const readConference = sinon.stub(api, 'readConference')

    beforeEach(() => {
      sagaTester = new SagaTester({initialState, reducers: appReducer})
      sagaTester.start(rootSaga)
    })

    afterEach(() => {
      readConference.reset()
      sagaTester.reset()
    })

    it('emitting success event when succeed', () => {
      // @ts-ignore
      readConference.returns(Promise.resolve({id: 4711}))
      sagaTester.dispatch(getConference())
      return sagaTester.waitFor(AppEvents.GET_CONFERENCE_SUCCEEDED).then(() => {

        expect(readConference.getCalls().length).toBe(1)
        const conference = sagaTester.getState().app.conference
        expect(conference).toBeDefined()
        expect(conference).toEqual({id: 4711})
      })
    })
  })
})
