import {
  checkApplicationRoomTypeSelection,
  checkApplicationsArePresentOnlyOnce, checkPersonsArePresentOnlyOnce, createLotteryInfos, fillDraw, getRandomInt,
  isDiversity
} from './lotterySagaHelper'
import {RoomType} from '../reducers/appReducer'
import {DrawItem} from '../reducers/lotteryReducer'
import {ApplicationListItem} from '../applicantList/commons'
import {Optional} from '../../types'

const roomTypes: RoomType[] = [
  {
    id: 'single', display: 'Single', nightPricePerPerson: 62.50, numberOfRooms: 120, peoplePerRoom: 1,
    reservedOrganization: 0, reservedSponsors: 0, lotterySortOrder: 0
  },
  {
    id: 'bedInDouble', display: 'Double shared', nightPricePerPerson: 47.95, numberOfRooms: 16, peoplePerRoom: 2,
    reservedOrganization: 0, reservedSponsors: 0, lotterySortOrder: 0
  },
  {
    id: 'juniorExclusively', display: 'Junior (exclusively)', nightPricePerPerson: 62.50, numberOfRooms: 42,
    peoplePerRoom: 1, reservedOrganization: 0, reservedSponsors: 0, lotterySortOrder: 0
  },
  {
    id: 'juniorShared', display: 'Junior (shared)', nightPricePerPerson: 45.45, numberOfRooms: 14,
    peoplePerRoom: 2, reservedOrganization: 0, reservedSponsors: 0, lotterySortOrder: 0
  }
]

const applications = [{
  'personId': 1153,
  'applicantId': 1444,
  'date': '2018-03-02T04:48:13.000Z',
  'name': 'name 31',
  'email': 'name31@email.de',
  'roomTypes': ['single'],
  'diversityReason': ''
}, {
  'personId': 1351,
  'applicantId': 1796,
  'date': '2018-03-02T04:53:14.000Z',
  'name': 'name 229',
  'email': 'name229@email.de',
  'roomTypes': ['single'],
  'diversityReason': 'yes'
}, {
  'personId': 1376,
  'applicantId': 1840,
  'date': '2018-03-02T10:18:03.000Z',
  'name': 'name 254',
  'email': 'name254@email.de',
  'roomTypes': ['single'],
  'diversityReason': ''
}, {
  'personId': 1214,
  'applicantId': 1552,
  'date': '2018-03-02T12:08:15.000Z',
  'name': 'name 92',
  'email': 'name92@email.de',
  'roomTypes': ['single'],
  'diversityReason': ''
}, {
  'personId': 1382,
  'applicantId': 1850,
  'date': '2018-03-02T15:03:13.000Z',
  'name': 'name 260',
  'email': 'name260@email.de',
  'roomTypes': ['single'],
  'diversityReason': ''
}, {
  'personId': 1322,
  'applicantId': 1744,
  'date': '2018-03-02T17:34:27.000Z',
  'name': 'name 200',
  'email': 'name200@email.de',
  'roomTypes': ['single'],
  'diversityReason': ''
}, {
  'personId': 1196,
  'applicantId': 1520,
  'date': '2018-03-02T19:33:41.000Z',
  'name': 'name 74',
  'email': 'name74@email.de',
  'roomTypes': ['single'],
  'diversityReason': ''
}, {
  'personId': 1225,
  'applicantId': 1572,
  'date': '2018-03-02T20:19:20.000Z',
  'name': 'name 103',
  'email': 'name103@email.de',
  'roomTypes': ['single'],
  'diversityReason': ''
}, {
  'personId': 1330,
  'applicantId': 1758,
  'date': '2018-03-02T21:08:38.000Z',
  'name': 'name 208',
  'email': 'name208@email.de',
  'roomTypes': ['single'],
  'diversityReason': 'yes'
}, {
  'personId': 1313,
  'applicantId': 1728,
  'date': '2018-03-02T21:58:21.000Z',
  'name': 'name 191',
  'email': 'name191@email.de',
  'roomTypes': ['single'],
  'diversityReason': ''
}, {
  'personId': 1265,
  'applicantId': 1642,
  'date': '2018-03-02T22:11:42.000Z',
  'name': 'name 143',
  'email': 'name143@email.de',
  'roomTypes': ['single'],
  'diversityReason': 'yes'
}]

describe('lottery saga helper:', () => {
  describe('createLotteryInfos', () => {
    it('returns one row per room type', () => {
      const lotteryInfos = createLotteryInfos(roomTypes, [], [])
      expect(lotteryInfos.length).toBe(4)
    })
    it('applications amount is used', () => {
      const theApplications = [{counter: 4711, roomType: 'single'}]
      const lotteryInfos = createLotteryInfos(roomTypes, theApplications, [])
      expect(lotteryInfos.find(item => item.roomType === 'Single')?.waitingPeople).toBe(4711)
    })
    it('occupation and diversity data amount is used', () => {
      const occupation = [
        {counter: 50, roomType: 'single', diversityReason: 'yes'},
        {counter: 40, roomType: 'single', diversityReason: 'no'}
      ]
      const lotteryInfos = createLotteryInfos(roomTypes, [], occupation)
      expect(lotteryInfos.find(item => item.roomType === 'Single')?.registeredPeople).toBe(90)
      expect(lotteryInfos.find(item => item.roomType === 'Single')?.diversityPeople).toBe(50)
    })
  })
  describe('getRandomInt', () => {
    it('returns a value between min and max value', () => {
      const value = getRandomInt(10, 20)
      expect(value).toBeGreaterThanOrEqual(10)
      expect(value).toBeLessThanOrEqual(20)
    })
  })
  describe('isDiversity', () => {
    it('returns false on application with diversity reason equals \'no\'', () => {
      const value = isDiversity({diversityReason: 'no'})
      expect(value).toBe(false)
    })
    it('returns false on application with diversity reason equals a trimmed empty string', () => {
      const value = isDiversity({diversityReason: '     '})
      expect(value).toBe(false)
    })
    it('returns true on application with any diversity reason but \'no\' and a trimmed empty string', () => {
      const value = isDiversity({diversityReason: 'some other value'})
      expect(value).toBe(true)
    })
  })
  describe('fillDraw', () => {
    let drawItem: DrawItem
    beforeEach(() => {
      drawItem = {
        'info': {
          'roomTypeId': 'single',
          'roomType': 'Single',
          'waitingPeople': 10,
          'registeredPeople': 0,
          'spotsToReserve': 0,
          'diversityPeople': 0,
          'free': 5
        },
        'applications':[],
        'drawn': []
      }
    })
    it('does not fill draws when no draw item is provided', () => {
      const selectedPersonIds: number[] = []
      fillDraw(undefined, [], selectedPersonIds)
      expect(selectedPersonIds.length).toBe(0)
    })
    it('does not fill draws when applications is null', () => {
      const selectedPersonIds: number[] = []
      fillDraw(drawItem, undefined, selectedPersonIds)
      expect(selectedPersonIds.length).toBe(0)
      expect(drawItem.drawn.length).toBe(0)
    })
    it('does not fill draws when applications is empty', () => {
      const selectedPersonIds: number[] = []
      fillDraw(drawItem, [], selectedPersonIds)
      expect(selectedPersonIds.length).toBe(0)
      expect(drawItem.drawn.length).toBe(0)
    })
    it('does not fill draws when the number of waiting people is zero', () => {
      const selectedPersonIds: number[] = []
      drawItem.info.waitingPeople = 0
      fillDraw(drawItem, applications, selectedPersonIds)
      expect(selectedPersonIds.length).toBe(0)
      expect(drawItem.drawn.length).toBe(0)
    })
    it('does not fill draws when the number of free slots is zero', () => {
      const selectedPersonIds: number[] = []
      drawItem.info.free = 0
      fillDraw(drawItem, applications, selectedPersonIds)
      expect(selectedPersonIds.length).toBe(0)
      expect(drawItem.drawn.length).toBe(0)
    })
    it('fills draws when all data is available', () => {
      const selectedPersonIds: number[] = []
      fillDraw(drawItem, applications, selectedPersonIds)
      expect(selectedPersonIds.length).toBeGreaterThan(0)
      expect(drawItem.drawn.length).toBeGreaterThan(0)
    })
    it('draws all free spots, when the number of free spots is less than or equal the number of applications', () => {
      const selectedPersonIds: number[] = []
      drawItem.info.free = 3
      fillDraw(drawItem, applications, selectedPersonIds)
      expect(selectedPersonIds.length).toBe(3)
      expect(drawItem.drawn.length).toBe(3)
    })
    it('draws all applications, when the number of applications is less than or equal the free spots number', () => {
      const selectedPersonIds: number[] = []
      drawItem.info.free = 20
      const expectedDraws = applications.length
      fillDraw(drawItem, applications, selectedPersonIds)
      expect(selectedPersonIds.length).toBe(expectedDraws)
      expect(drawItem.drawn.length).toBe(expectedDraws)
    })
    it('does not draw applications for already selected persons', () => {
      const selectedPersonIds = []
      selectedPersonIds.push(applications[0].personId)
      drawItem.info.free = 20
      const expectedDraws = applications.length - selectedPersonIds.length
      fillDraw(drawItem, applications, selectedPersonIds)
      expect(selectedPersonIds.length).toBe(expectedDraws + 1)
      expect(drawItem.drawn.length).toBe(expectedDraws)
    })
    it('diversity applications are preferred', () => {
      function findApplication(x: number): Optional<ApplicationListItem> {
        return applications.find(app => app.applicantId === x && isDiversity(app))
      }

      const selectedPersonIds: number[] = []
      drawItem.info.free = 20
      const expectedDraws = applications.length
      const expectedDiversityDraws = applications.filter(x => isDiversity(x)).length
      fillDraw(drawItem, applications, selectedPersonIds)
      expect(selectedPersonIds.length).toBe(expectedDraws)
      expect(drawItem.drawn.length).toBe(expectedDraws)

      const diversityDraws = drawItem.drawn.filter(x => findApplication(x))
      expect(diversityDraws.length).toBe(expectedDiversityDraws)
    })

  })
  describe('checkApplicationsArePresentOnlyOnce', () => {
    let distribution: DrawItem[]
    const drawItem: DrawItem = {
      'info': {
        'roomTypeId': 'single',
        'roomType': 'Single',
        'waitingPeople': 10,
        'registeredPeople': 0,
        'diversityPeople': 0,
        'free': 2,
        'spotsToReserve': 0
      },
      'applications': applications,
      'drawn': [1796, 1642]
    }
    beforeEach(() => {
      distribution = []
      distribution.push(drawItem)
    })
    it('finds no error on a correct distribution', () => {
      const errors = checkApplicationsArePresentOnlyOnce(distribution)
      expect(errors.length).toBe(0)
    })
    it('finds error if an application is present more than once', () => {
      distribution[0].drawn = [1796, 1796]
      const errors = checkApplicationsArePresentOnlyOnce(distribution)
      expect(errors.length).toBe(1)
    })
  })
  describe('checkApplicationRoomTypeSelection', () => {
    let distribution: DrawItem[]
    const drawItem = {
      'info': {
        'roomTypeId': 'single',
        'roomType': 'Single',
        'waitingPeople': 10,
        'registeredPeople': 0,
        'diversityPeople': 0,
        'spotsToReserve':0,
        'free': 2
      },
      'applications': applications,
      'drawn': [1796, 1642]
    }
    beforeEach(() => {
      distribution = []
      distribution.push(drawItem)
    })
    it('finds no error on a correct distribution', () => {
      const errors = checkApplicationRoomTypeSelection(distribution)
      expect(errors.length).toBe(0)
    })
    it('finds error if an application is present more than once', () => {
      distribution[0].drawn = [757823, 4711]
      const errors = checkApplicationRoomTypeSelection(distribution)
      expect(errors.length).toBe(2)
    })
  })
  describe('checkPersonsArePresentOnlyOnce', () => {
    let distribution: DrawItem[]
    const drawItem = {
      'info': {
        'roomTypeId': 'single',
        'roomType': 'Single',
        'waitingPeople': 10,
        'registeredPeople': 0,
        'diversityPeople': 0,
        'spotsToReserve': 0,
        'free': 2
      },
      'applications': applications,
      'drawn': [1796, 1642]
    }
    beforeEach(() => {
      distribution = []
      distribution.push(drawItem)
    })
    it('finds no error on a correct distribution', () => {
      const errors = checkPersonsArePresentOnlyOnce(distribution)
      expect(errors.length).toBe(0)
    })
    it('finds error if an application is present more than once', () => {
      distribution[0].drawn = [1796, 1796, 32423]
      const errors = checkPersonsArePresentOnlyOnce(distribution)
      expect(errors.length).toBe(2)
    })
  })
})
