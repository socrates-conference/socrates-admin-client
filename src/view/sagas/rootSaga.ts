import {all, takeEvery, takeLatest} from 'redux-saga/effects'
import RoutingCommand from '../commands/routingCommand'
import AuthenticationCommand from '../commands/authenticationCommand'
import ApplicationListCommand from '../commands/applicationListCommand'
import OverviewCommand from '../commands/dashboardCommand'
import AppCommand from '../commands/appCommand'
import {loginSaga, logoutSaga} from './authorizationSaga'
import {routeToSaga} from './routeToSaga'
import {
  applicationListChangeRoomTypeFilter, applicationListCloseModalSaga,
  applicationListGetApplications, applicationListExecuteSaga,
  applicationListGetRoomTypes, applicationListHandleActionSaga
} from './applicationListSaga'
import {conferenceGetData} from './appSaga'
import {dashboardGetRegistrationsByRoomType} from './dashboardSaga'
import LotteryCommand from '../commands/lotteryCommand'
import {healthCheckSaga, readLotteryInfoSaga, registerSaga, startLotteryDrawSaga} from './lotterySaga'
import ParticipantListCommand from '../participantList/participantListCommand'
import {
  participantListCloseModalSaga,
  participantListExecuteSaga,
  participantListGetParticipants,
  participantListGetRoomTypes,
  participantListHandleActionSaga,
  participantListListTypeChangeSaga
} from '../participantList/participantListSaga'

function* rootSaga(): any {
  yield all([
    takeEvery(AppCommand.GET_CONFERENCE, conferenceGetData),
    takeLatest(OverviewCommand.GET_REGISTRATIONS_BY_ROOM_TYPE, dashboardGetRegistrationsByRoomType),
    takeEvery(RoutingCommand.ROUTE_TO, routeToSaga),
    takeEvery(AuthenticationCommand.LOGIN, loginSaga),
    takeEvery(AuthenticationCommand.LOGOUT, logoutSaga),
    takeEvery(ApplicationListCommand.HANDLE_ACTION, applicationListHandleActionSaga),
    takeEvery(ApplicationListCommand.CLOSE_MODAL, applicationListCloseModalSaga),
    takeEvery(ApplicationListCommand.EXECUTE_ACTION, applicationListExecuteSaga),
    takeEvery(ApplicationListCommand.CHANGE_ROOM_TYPE_FILTER, applicationListChangeRoomTypeFilter),
    takeEvery(ApplicationListCommand.GET_ROOM_TYPES, applicationListGetRoomTypes),
    takeEvery(ApplicationListCommand.GET_APPLICATIONS, applicationListGetApplications),
    takeEvery(LotteryCommand.INFO, readLotteryInfoSaga),
    takeEvery(LotteryCommand.START, startLotteryDrawSaga),
    takeEvery(LotteryCommand.HEALTH_CHECK, healthCheckSaga),
    takeEvery(LotteryCommand.REGISTER, registerSaga),
    takeEvery(ParticipantListCommand.GET_PARTICIPANTS, participantListGetParticipants),
    takeEvery(ParticipantListCommand.HANDLE_ACTION, participantListHandleActionSaga),
    takeEvery(ParticipantListCommand.HANDLE_TABLE_ACTION, participantListHandleActionSaga),
    takeEvery(ParticipantListCommand.CLOSE_MODAL, participantListCloseModalSaga),
    takeEvery(ParticipantListCommand.EXECUTE_ACTION, participantListExecuteSaga),
    takeEvery(ParticipantListCommand.HANDLE_LIST_TYPE_CHANGE, participantListListTypeChangeSaga),
    takeEvery(ParticipantListCommand.GET_ROOM_TYPES, participantListGetRoomTypes)
  ])
}

export default rootSaga
