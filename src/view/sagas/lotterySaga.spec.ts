// @flow

import SagaTester from 'redux-saga-tester'
import rootSaga from './rootSaga'
import * as sinon from 'sinon'
import * as api from '../requests/api'
import {getLotteryInfo, healthCheck, register, startLotteryDraw} from '../commands/lotteryCommand'
import lotteryReducer from '../reducers/lotteryReducer'
import moment from 'moment'
import {conference, distribution, distributionWithErrors, info} from '../../test/testData'

const readApplicationsForLottery = sinon.stub(api, 'readApplicationsForLottery')
const readRegistrationsByRoomType = sinon.stub(api, 'readRegistrationsByRoomType')
const readConference = sinon.stub(api, 'readConference')
const readApplicationsByRoomType = sinon.stub(api, 'readApplicationsByRoomType')

describe('lotterySaga', () => {
  let sagaTester: SagaTester
  describe('reacts to read lottery info', () => {
    const INITIAL_STATE = {
      appState: {conference: conference},
      lottery: {info: []}
    }
    beforeEach(() => {
      sagaTester = new SagaTester({INITIAL_STATE, reducers: lotteryReducer})
      sagaTester.start(rootSaga)
    })

    afterEach(() => {
      readConference.reset()
      readApplicationsForLottery.reset()
      readRegistrationsByRoomType.reset()
      sagaTester.reset()
    })

    it('updating state when data available', () => {
      readConference.returns(conference)
      readRegistrationsByRoomType.returns([])
      readApplicationsForLottery.returns([])
      sagaTester.dispatch(getLotteryInfo())
      expect(readConference.getCalls().length).toBe(1)
      expect(readRegistrationsByRoomType.getCalls().length).toBe(1)
      expect(readApplicationsForLottery.getCalls().length).toBe(1)
      expect(sagaTester.getState().lottery.info.length).toBe(4)
    })
    it('clearing state when no conference available', () => {
      readConference.returns(undefined)
      sagaTester.dispatch(getLotteryInfo())
      expect(readConference.getCalls().length).toBe(1)
      expect(readRegistrationsByRoomType.getCalls().length).toBe(0)
      expect(readApplicationsForLottery.getCalls().length).toBe(0)
      expect(sagaTester.getState().lottery.info.length).toBe(0)
    })
    it('clearing state when error occurs', () => {
      readConference.returns(new Error())
      sagaTester.dispatch(getLotteryInfo())
      expect(readConference.getCalls().length).toBe(1)
      expect(sagaTester.getState().lottery.info.length).toBe(0)
    })
  })
  describe('reacts to start lottery draw', () => {
    const INITIAL_STATE = {
      appState: {conference: conference},
      lottery: {info: []}
    }
    beforeEach(() => {
      sagaTester = new SagaTester({INITIAL_STATE, reducers: lotteryReducer})
      sagaTester.start(rootSaga)
      readApplicationsByRoomType.returns([])
    })

    afterEach(() => {
      readApplicationsByRoomType.reset()
      sagaTester.reset()
    })

    it('and do not start when lottery day is in the future', () => {
      conference.lotteryDay = moment().add(5, 'day')
      sagaTester.dispatch(startLotteryDraw(conference, info))
      expect(readApplicationsByRoomType.getCalls().length).toBe(0)
    })
    it('and do start when lottery day is in the past', () => {
      conference.lotteryDay = moment().add(-5, 'day')
      sagaTester.dispatch(startLotteryDraw(conference, info))
      expect(readApplicationsByRoomType.getCalls().length).toBe(4) // Once per room type
      expect(sagaTester.getState().lottery.distribution.length).toBe(4)
    })
  })
  describe('reacts to distribution health check', () => {
    const INITIAL_STATE = {
      appState: {conference: conference},
      lottery: {info: [], distribution: []}
    }
    beforeEach(() => {
      sagaTester = new SagaTester({INITIAL_STATE, reducers: lotteryReducer})
      sagaTester.start(rootSaga)
    })

    afterEach(() => {
      sagaTester.reset()
    })

    it('with no error if everything is ok', () => {
      sagaTester.dispatch(healthCheck(distribution))
      expect(sagaTester.getState().lottery.distributionErrors.length).toBe(0) // Once per room type
    })
    it('with errors if something is rotten', () => {
      sagaTester.dispatch(healthCheck(distributionWithErrors))
      expect(sagaTester.getState().lottery.distributionErrors.length).toBeGreaterThan(0)
    })
  })
  describe('reacts to register distribution applications', () => {
    const INITIAL_STATE = {
      appState: {conference: conference},
      lottery: {info: [], distribution: []}
    }
    const registerApplication = sinon.stub(api, 'registerApplication')
    beforeEach(() => {
      sagaTester = new SagaTester({INITIAL_STATE, reducers: lotteryReducer})
      sagaTester.start(rootSaga)
    })

    afterEach(() => {
      registerApplication.reset()
      sagaTester.reset()
    })

    it('with no error if everything is ok', () => {
      registerApplication.returns(true)
      sagaTester.dispatch(register(distribution))
      const registrationResults = sagaTester.getState().lottery.registrationResults
      expect(registerApplication.getCalls().length).toBeGreaterThan(0)
      expect(registrationResults.length).toBeGreaterThan(0)
      expect(registrationResults.some(r => !r.registered)).toBe(false)
    })
    it('with errors if cannot register applications', () => {
      registerApplication.returns(false)
      sagaTester.dispatch(register(distribution))
      const registrationResults = sagaTester.getState().lottery.registrationResults
      expect(registerApplication.getCalls().length).toBeGreaterThan(0)
      expect(registrationResults.length).toBeGreaterThan(0)
      expect(registrationResults.some(r => !r.registered)).toBe(true)
    })
  })
})