import type {ApplicationListItem} from '../applicantList/commons'

import {
  deleteApplication,
  registerApplication,
  updateApplication,
  deleteParticipant,
  sendConfirmationReminder,
  updateParticipantRoomType
} from '../requests/api'
import {ParticipantListCommandId} from '../participantList/participantListCommand'
import {ParticipantListItem} from '../participantList/commons'

export const applicationListCommandIdToApiCall =
  (commandId: string): ((data: ApplicationListItem) => Promise<boolean>) => {
    switch (commandId) {
      case 'delete':
        return deleteApplication
      case 'edit':
        return updateApplication
      case 'register':
        return registerApplication
      default:
        throw new Error(`Unknown action '${commandId}' for application list.`)
    }
  }
export const participantListCommandIdToApiCall =
  (commandId: string): ((data:ParticipantListItem) => Promise<boolean>) => {
    switch (commandId) {
      case ParticipantListCommandId.DELETE_ROW:
        return deleteParticipant
      case ParticipantListCommandId.CHANGE_ROW_DETAILS:
        return updateParticipantRoomType
      case ParticipantListCommandId.SEND_CONFIRMATION_REMINDER:
      case ParticipantListCommandId.SEND_ALL_CONFIRMATION_REMINDERS:
        return sendConfirmationReminder
      default:
        throw new Error(`Unknown action '${commandId}' for application list.`)
    }
  }
