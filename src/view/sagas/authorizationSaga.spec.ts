import SagaTester from 'redux-saga-tester'
import rootSaga from './rootSaga'
import {
  login,
  logout
} from '../commands/authenticationCommand'
import * as sinon from 'sinon'
import * as api from '../requests/api'
import authenticationReducer, {AuthenticationState} from '../reducers/authenticationReducer'
import RoutingCommand from '../commands/routingCommand'
import axios from 'axios/index'
import {Action} from 'redux'
import AuthenticationEvent from '../events/authenticationEvents'

type SagaState = { authentication: AuthenticationState }

describe('rootSaga', () => {
  let sagaTester: SagaTester<SagaState>
  describe('authentication', () => {
    const INITIAL_STATE = {
      authentication: {
        token: '',
        userName: 'Guest',
        isAdministrator: false,
        hasFinished: false
      }
    }
    beforeEach(() => {
      sagaTester = new SagaTester({initialState: INITIAL_STATE, reducers: authenticationReducer})
      sagaTester.start(rootSaga)
    })

    afterEach(() => {
      sagaTester.reset()
    })

    it('reacts to logout dispatching LOGOUT_SUCCESS ', () => {
      axios.defaults.headers.common['Authorization'] = 'Bearer token'
      const newAuthState = {...sagaTester.getState().authentication, token: 'token', userName: 'TheName'}
      sagaTester.updateState({...INITIAL_STATE, authentication: newAuthState})
      sagaTester.dispatch(logout())
      return sagaTester.waitFor(AuthenticationEvent.LOGOUT_SUCCESS).then(() => {
        const action = sagaTester.getLatestCalledAction()
        expect(action.type).toBe('Event/LOGOUT_SUCCESS')
        expect(sagaTester.getState().authentication.token).toBe('')
        expect(sagaTester.getState().authentication.userName).toBe('Guest')
        expect(axios.defaults.headers.common['Authorization']).toBeUndefined()
      })
    })

    describe('reacts to login command', () => {
      const loginStub = sinon.stub(api, 'login')
      const getRouteToCommands = (actions: Action[]): Action[] => actions.filter(action => action.type === RoutingCommand.ROUTE_TO)

      afterEach(() => {
        loginStub.reset()
      })

      it('emitting login started, login success and route to when login succeed', async () => {
        const theToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFsZXhAZW1haWwuZGUiLCJuY' +
          'W1lIjoiQWxleCIsImlzQWRtaW5pc3RyYXRvciI6MCwiaWF0IjoxNTIxNTY3MjUyfQ.QJRC7fzx3s4Khhd_jXRtrbaB-' +
          'p3wx9lEjP-muNU_2-0'
        delete axios.defaults.headers.common['Authorization']
        loginStub.returns(Promise.resolve(theToken))
        sagaTester.dispatch(login('somebody@email.de', 'thePassword', '/home'))
        return sagaTester.waitFor(AuthenticationEvent.LOGIN_SUCCESS, true).then(() => {
          const actions = sagaTester.getCalledActions()
          expect(loginStub.getCalls().length).toBe(1)
          const state: any = sagaTester.getState()
          expect(state.authentication.token).toBe(theToken)
          expect(state.authentication.userName).toBe('Alex')
          expect(state.authentication.hasFinished).toBe(true)
          expect(axios.defaults.headers.common['Authorization']).toBeDefined()
          expect(axios.defaults.headers.common['Authorization'].indexOf('Bearer')).toBe(0)
          expect(getRouteToCommands(actions)).toHaveLength(1)
        })
      })
      it('emitting login started and login error when login fails', () => {
        delete axios.defaults.headers.common['Authorization']
        loginStub.returns(Promise.resolve(''))
        sagaTester.dispatch(login('somebody@email.de', 'thePassword', '/'))
        sagaTester.waitFor(AuthenticationEvent.LOGIN_ERROR, true).then(() => {
          const state: any = sagaTester.getState()
          expect(loginStub.getCalls().length).toBe(1)
          expect(state.authentication.token).toBe('')
          expect(state.authentication.userName).toBe('Guest')
          expect(state.authentication.hasFinished).toBe(true)
          expect(axios.defaults.headers.common['Authorization']).toBeUndefined()
        })
      })
      it('emitting login started and login error when token cannot be verified', () => {
        delete axios.defaults.headers.common['Authorization']
        loginStub.returns(Promise.resolve('non verifiable'))
        sagaTester.dispatch(login('somebody@email.de', 'thePassword', '/'))
        sagaTester.waitFor(AuthenticationEvent.LOGIN_ERROR, true).then(() => {
          const state: any = sagaTester.getState()
          expect(loginStub.getCalls().length).toBe(1)
          expect(state.authentication.token).toBe('')
          expect(state.authentication.userName).toBe('Guest')
          expect(state.authentication.hasFinished).toBe(true)
          expect(axios.defaults.headers.common['Authorization']).toBeUndefined()
        })
      })
    })

  })
})
