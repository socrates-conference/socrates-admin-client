import {Action} from 'redux'

export default class AuthenticationCommand {
  static get LOGIN(): string {
    return 'Command/AUTHENTICATION_LOGIN'
  }
  static get LOGOUT(): string {
    return 'Command/AUTHENTICATION_LOGOUT'
  }
}

type Command = Action
type LoginCommand = Command & { email: string, password: string, comesFrom: string }

export const login = (email: string, password: string, comesFrom: string): LoginCommand => ({
  type: AuthenticationCommand.LOGIN,
  email,
  password,
  comesFrom
})

export const logout = (): Command => ({type: AuthenticationCommand.LOGOUT})
