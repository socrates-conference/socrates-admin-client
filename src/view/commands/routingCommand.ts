import {Action} from 'redux'

export default class RoutingCommand {
  static get ROUTE_TO(): string { return 'RoutingCommand/ROUTE_TO' }
}

export const routeTo = (url: string): Action & {url: string} => ({type: RoutingCommand.ROUTE_TO, url})
