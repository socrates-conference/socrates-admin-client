import type {Conference} from '../reducers/appReducer'
import type {DrawItem, LotteryInfoRow} from '../reducers/lotteryReducer'
import {Action} from 'redux'

export default class LotteryCommand {
  static get START(): string { return 'LotteryCommand/START' }
  static get INFO(): string { return 'LotteryCommand/INFO' }
  static get HEALTH_CHECK(): string { return 'LotteryCommand/HEALTH_CHECK' }
  static get REGISTER(): string { return 'LotteryCommand/REGISTER' }
}

type Command = Action
type StartLotteryDrawCommand = Command & {conference?: Conference, info: LotteryInfoRow[]}
type DistributionCommand = Command & {distribution: DrawItem[]}

export const startLotteryDraw =
  (conference: Conference|undefined, info: Array<LotteryInfoRow>): StartLotteryDrawCommand => ({type: LotteryCommand.START, conference, info})
export const getLotteryInfo = (): Command => ({type: LotteryCommand.INFO})
export const healthCheck = (distribution: Array<DrawItem>): DistributionCommand => ({type: LotteryCommand.HEALTH_CHECK, distribution})
export const register = (distribution: Array<DrawItem>): DistributionCommand => ({type: LotteryCommand.REGISTER, distribution})
