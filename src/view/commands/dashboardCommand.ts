import {Action} from 'redux'

export default class DashboardCommand {
  static get GET_REGISTRATIONS_BY_ROOM_TYPE(): string {
    return 'DashboardCommand/GET_REGISTRATIONS_BY_ROOM_TYPE'
  }
}

export const getRegistrationsByRoomType =
  (): Action => ({type: DashboardCommand.GET_REGISTRATIONS_BY_ROOM_TYPE})
