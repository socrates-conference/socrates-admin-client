import {Action} from 'redux'

export default class AppCommand {
  static get GET_CONFERENCE(): string {
    return 'AppCommand/GET_CONFERENCE'
  }
}

export const getConference = (): Action => ({type: AppCommand.GET_CONFERENCE})
