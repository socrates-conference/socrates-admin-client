import { Action } from 'redux'
import type {ApplicationListItem} from '../applicantList/commons'

export type CommandAction = {
  type: string
  commandId: string
  data: any
  roomTypeFilter?: string
}

export type QueryAction = {
  type: string
  filter?: string
}

export class ApplicationListCommandId {
  static DELETE = 'delete'
  static EDIT = 'edit'
  static REGISTER = 'register'
}

export default class ApplicationListCommand {
  static get HANDLE_ACTION(): string {
    return 'ApplicationListCommand/HANDLE_ACTION'
  }
  static get EXECUTE_ACTION(): string {
    return 'ApplicationListCommand/EXECUTE_ACTION'
  }
  static get CLOSE_MODAL(): string {
    return 'ApplicationListCommand/CLOSE_MODAL'
  }
  static get CHANGE_ROOM_TYPE_FILTER(): string {
    return 'ApplicationListCommand/CHANGE_ROOM_TYPE_FILTER'
  }
  static get GET_ROOM_TYPES(): string {
    return 'ApplicationListCommand/GET_ROOM_TYPES'
  }
  static get GET_APPLICATIONS(): string {
    return 'ApplicationListCommand/GET_APPLICATIONS'
  }
}

export const handleAction =
  (commandId: string, data: ApplicationListItem):CommandAction =>
    ({type: ApplicationListCommand.HANDLE_ACTION, commandId, data})
export const closeModal = ():Action => ({type: ApplicationListCommand.CLOSE_MODAL})
export const executeAction =
  (data: ApplicationListItem, commandId: string, roomTypeFilter: string): CommandAction =>
    ({type: ApplicationListCommand.EXECUTE_ACTION, data, commandId, roomTypeFilter})
export const changeRoomTypeFilter =
  (filter: string): QueryAction => ({type: ApplicationListCommand.CHANGE_ROOM_TYPE_FILTER, filter})
export const getRoomTypes = (): QueryAction => ({type: ApplicationListCommand.GET_ROOM_TYPES})
export const getApplications = (filter: string): QueryAction => ({type: ApplicationListCommand.GET_APPLICATIONS, filter})


