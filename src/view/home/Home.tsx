import React from 'react'
import RoomsOccupancyContainer from './roomsOccupancy/RoomsOccupancyContainer'
import DiversityContainer from './diversity/DiversityContainer'
import {getRegistrationsByRoomType} from '../commands/dashboardCommand'
import {connect} from 'react-redux'

type DispatchFromProps = {
  getRegistrationsByRoomType: () => void
}

export class Home extends React.Component<DispatchFromProps> {
  constructor(props: DispatchFromProps){
    super(props)
    props.getRegistrationsByRoomType()
  }

  render = (): JSX.Element => {
    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-12">
            <div className="page-header"><h1>Overview</h1></div>
          </div>
        </div>
        <RoomsOccupancyContainer/>
        <hr/>
        <DiversityContainer/>
      </div>
    )
  }
}

const mapDispatchToProps = {
  getRegistrationsByRoomType
}

export default connect<{}, DispatchFromProps>(null, mapDispatchToProps)(Home)


