import React from 'react'
import {connect} from 'react-redux'
import Table from '../../common/table/Table'
import {getRegistrationsByRoomType} from '../../commands/dashboardCommand'
import type {
  Action,
  SortableRow
} from '../../common/table/TableTypes'

type StateFromProps = {
  rows: Array<SortableRow>
}
type DispatchFromProps = {
  getRegistrationsByRoomType: () => void
}

type Props = StateFromProps & DispatchFromProps
// const desiredDiversity = 0.2;

const columns = [
  {header: 'Type', field: 'roomType', visible: true},
  {header: 'Min. Diversity', field: 'minimumDiversity', visible: true},
  {header: 'Cur. Diversity', field: 'currentDiversity', visible: true}
]


function DiversityContainer(props: Props): JSX.Element {
  const tableActions: Array<Action> = [
    {
      execute: () => props.getRegistrationsByRoomType(),
      canExecute: () => true, buttonClass: 'btn-info',
      icon: 'sync', text: 'Refresh', visible: true
    }
  ]

  return (
    <div id="statistic" className="mt-4">
      <div><h2>Diversity</h2></div>
      <Table columns={columns} data={props.rows || []} tableActions={tableActions}/>
    </div>
  )
}


const mapStateToProps = (state: any): StateFromProps => {
  return {
    rows: {...state.dashboard}.diversityRows
  }
}

const mapDispatchToProps = {
  getRegistrationsByRoomType
}


export default connect<StateFromProps, DispatchFromProps>(mapStateToProps, mapDispatchToProps)(DiversityContainer)
