import React from 'react'
import {shallow} from 'enzyme'
import DiversityContainer from './DiversityContainer'
import StoreFactory, {HistoryFactory} from '../../store/store'

function* emptySaga(): any {
}

const reducer = (): any => {
  return {
    someState: {}
  }
}
const store = StoreFactory.createTestStore(reducer, emptySaga, HistoryFactory.createTestHistory())


describe('(Component) Home', () => {
  // @ts-ignore
  const wrapper = shallow(<DiversityContainer store={store}/>)

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
