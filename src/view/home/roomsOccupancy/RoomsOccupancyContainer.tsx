import React from 'react'
import {connect} from 'react-redux'
import Table from '../../common/table/Table'
import {getRegistrationsByRoomType} from '../../commands/dashboardCommand'
import type {Action} from '../../common/table/TableTypes'

type StateFromProps = {
  rows: Array<Object>
}

type DispatchFromProps = {
  getRegistrationsByRoomType: () => void
}

type Props = StateFromProps & DispatchFromProps


const columns = [
  {header: 'Type', field: 'roomType', visible: true},
  {header: 'Amount', field: 'numberOfRooms', displayClass: 'd-none d-md-table-cell', visible: true},
  {header: 'Capacity', field: 'capacity', visible: true},
  {header: 'Reserved orga', field: 'reservedOrganization', displayClass: 'd-none d-lg-table-cell', visible: true},
  {header: 'Reserved sponsors', field: 'reservedSponsors', displayClass: 'd-none d-lg-table-cell', visible: true},
  {header: 'Occupied', field: 'occupied', displayClass: 'd-none d-md-table-cell', visible: true},
  {header: 'Free', field: 'free', visible: true}
]

function RoomsOccupancyContainer(props: Props): JSX.Element {
  const tableActions: Array<Action> = [
    {
      execute: () => props.getRegistrationsByRoomType(),
      canExecute: () => true, buttonClass: 'btn-info',
      icon: 'sync', text: 'Refresh', visible: true
    }
  ]

  return (
    <div id="statistic" className="mt-4">
      <div><h2>Room statistic</h2></div>
      <Table columns={columns} data={props.rows || []} tableActions={tableActions}/>
    </div>
  )
}

const mapStateToProps = (state: any): StateFromProps => {
  return {
    rows: {...state.dashboard}.occupationRows
  }
}

const mapDispatchToProps = {
  getRegistrationsByRoomType
}

export default connect<StateFromProps, DispatchFromProps>(mapStateToProps, mapDispatchToProps)(RoomsOccupancyContainer)
