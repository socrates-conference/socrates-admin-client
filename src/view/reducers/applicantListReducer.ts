import ApplicantListEvent from '../events/applicantListEvents'
import type {ApplicationListItem} from '../applicantList/commons'
import {LabeledItem} from '../../types'

export type ApplicationListState = {
  currentRoomTypeFilter: string
  currentCommand: string
  currentRow?: ApplicationListItem
  errorMessage: string
  roomTypes: Array<LabeledItem>
  rows: Array<ApplicationListItem>
}

const INITIAL: ApplicationListState = {
  currentRoomTypeFilter: 'all',
  currentCommand: '',
  currentRow: undefined,
  errorMessage: '',
  roomTypes: [],
  rows: []
}

const applicantListReducer = (state: ApplicationListState = INITIAL, action: any): ApplicationListState => {
  switch (action.type) {
    case ApplicantListEvent.EXECUTE_SUCCEEDED:
      return {...state, currentCommand: '', errorMessage: '', currentRow : undefined}
    case ApplicantListEvent.EXECUTE_FAILED:
      return {...state, currentCommand: 'error', errorMessage: action.error.message}
    case ApplicantListEvent.EXECUTE_CANCELED:
      return {...state, currentCommand: '', errorMessage: '', currentRow : undefined}
    case ApplicantListEvent.GET_ROOM_TYPES_SUCCEEDED:
      return {...state, roomTypes: action.rows}
    case ApplicantListEvent.GET_ROOM_TYPES_FAILED:
      return {...state, roomTypes: []}
    case ApplicantListEvent.GET_APPLICATIONS_SUCCEEDED:
      return {...state, rows: action.rows}
    case ApplicantListEvent.GET_APPLICATIONS_FAILED:
      return {...state, rows: []}
    case ApplicantListEvent.HANDLED_ACTION:
      return {...state, currentCommand: action.commandId, currentRow: action.data}
    case ApplicantListEvent.UPDATE_ROOM_TYPE_FILTER:
      return {...state, currentRoomTypeFilter: action.filter}
    default:
      return state
  }
}

export default {applicationList: applicantListReducer}
