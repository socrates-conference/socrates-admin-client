import { Moment } from 'moment'
import AppEvents from '../events/appEvents'

export type RoomType = {
  id: string
  display: string
  nightPricePerPerson: number
  numberOfRooms: number
  peoplePerRoom: number
  reservedOrganization: number
  reservedSponsors: number
  lotterySortOrder: number
}

export type LengthOfStay = {
  id: string
  from: string
  to: string
  numberOfNights: number
  numberOfDinners: number
  sundayFlat: boolean
}

export type Conference = {
  name: string
  nameLong: string
  year: number
  startDate: Moment
  endDate: Moment
  startApplications: Moment
  endApplications: Moment
  lotteryDay: Moment
  dinnerPrice: number
  flatThursday: number
  flatFridaySaturday: number
  flatSunday: number
  roomTypes: Array<RoomType>
  lengthsOfStay: Array<LengthOfStay>
}

export type AppState = {
  conference?: Conference
}
const INITIAL: AppState = {
  conference: undefined
}

const AppReducer = (state: AppState = INITIAL, action: any): AppState => {
  if(action.type === AppEvents.GET_CONFERENCE_SUCCEEDED) {
    return {...state, conference: action.conference}
  }
  return state
}

export default {app: AppReducer}
