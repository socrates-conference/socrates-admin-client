import authenticationReducer from './authenticationReducer'
import applicantListReducer from './applicantListReducer'
import appReducer from './appReducer'
import dashboardReducer from './dashboardReducer'
import lotteryReducer from './lotteryReducer'
import participantListReducer from '../participantList/participantListReducer'

export default Object.assign({}, appReducer, dashboardReducer, authenticationReducer, applicantListReducer,
                             lotteryReducer, participantListReducer)
