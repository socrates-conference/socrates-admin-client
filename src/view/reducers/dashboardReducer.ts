import DashboardEvents from '../events/dashboardEvents'

export type RegistrationsByRoomType = {
  counter: number
  roomType: string
  diversityReason: string
}

export type DashboardState = {
  occupationRows: Array<Object>
  diversityRows: Array<Object>
}

const INITIAL: DashboardState = {
  occupationRows: [],
  diversityRows: []
}

const DashboardReducer = (state: DashboardState = INITIAL, action: any): DashboardState => {
  if(action.type === DashboardEvents.GET_REGISTRATIONS_BY_ROOM_TYPE_SUCCEEDED) {
    return {...state, diversityRows: action.diversityRows, occupationRows: action.occupationRows}
  }
  if(action.type === DashboardEvents.GET_REGISTRATIONS_BY_ROOM_TYPE_FAILED) {
    return {...state, diversityRows: [], occupationRows: []}
  }
  return state
}

export default {dashboard: DashboardReducer}
