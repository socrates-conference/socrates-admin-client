import LotteryEvents from '../events/lotteryEvents'
import type {ApplicationListItem} from '../applicantList/commons'

export type LotteryInfoRow = {
  roomTypeId: string
  roomType: string
  waitingPeople: number
  spotsToReserve: number
  registeredPeople: number
  diversityPeople: number
  free: number
}

export type DrawItem = { info: LotteryInfoRow, applications: ApplicationListItem[], drawn: number[] }

type State = {
  distribution: Array<Object>
  distributionErrors: Array<string>
  hasCheckedDistributionHealth: boolean
  info: Array<Object>
  log: Array<Object>
  registrationResults: Array<Object>
  registeringIsRunning: boolean
  rows: Array<Object>
}

const INITIAL: State = {
  distribution: [],
  distributionErrors: [],
  hasCheckedDistributionHealth: false,
  info: [],
  log: [],
  registrationResults: [],
  registeringIsRunning: false,
  rows: []
}

const lotteryReducer = (state: State = INITIAL, action: any): State => {
  if (action.type === LotteryEvents.READ_INFO_SUCCEEDED) {
    return {...state, info: action.rows}
  }
  if (action.type === LotteryEvents.READ_INFO_FAILED) {
    return {...state, info: []}
  }
  if (action.type === LotteryEvents.DISTRIBUTE_SLOTS_SUCCEEDED) {
    return {...state, distribution: action.draw}
  }
  if (action.type === LotteryEvents.HEALTH_CHECK_SUCCEEDED) {
    return {...state, distributionErrors: [], hasCheckedDistributionHealth: true}
  }
  if (action.type === LotteryEvents.HEALTH_CHECK_FAILED) {
    return {...state, distributionErrors: action.errors, hasCheckedDistributionHealth: true}
  }
  if (action.type === LotteryEvents.REGISTRATION_STARTED) {
    return {...state, registeringIsRunning: true, log: []}
  }
  if (action.type === LotteryEvents.REGISTERED) {
    const log = state.log.slice()
    log.push({text: action.text, status: action.status})
    return {...state, log}
  }
  if (action.type === LotteryEvents.REGISTRATION_DONE) {
    return {
      ...state, registrationResults: action.results, distribution: [],
      distributionErrors: [], hasCheckedDistributionHealth: false
    }
  }
  return state
}

export default {lottery: lotteryReducer}
