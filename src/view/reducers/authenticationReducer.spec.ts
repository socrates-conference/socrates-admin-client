import reducer, {AuthenticationState} from './authenticationReducer'
import AuthenticationEvent from '../events/authenticationEvents'

describe('authentication reducer', () => {
  describe('on user login success', () => {
    let currentState: AuthenticationState
    beforeEach(() => {
      const initialState = {
        token: '',
        isAdministrator: false,
        userName: '',
        hasFinished: false
      }
      currentState = reducer.authentication(initialState, {
        type: AuthenticationEvent.LOGIN_SUCCESS,
        token: 'theToken',
        data: {isAdministrator: false, name: 'UserName'}
      })
    })
    it('token is set', () => {
      expect(currentState.token).toEqual('theToken')
    })
    it('login has finished is true', () => {
      expect(currentState.hasFinished).toBe(true)
    })
    it('user is not an administrator', () => {
      expect(currentState.isAdministrator).toBe(false)
    })
    it('UserName is logged in', () => {
      expect(currentState.userName).toEqual('UserName')
    })
  })
  describe('on administrator login success', () => {
    let currentState: AuthenticationState
    beforeEach(() => {
      const initialState = {
        token: '',
        userName: '',
        isAdministrator: false,
        hasFinished: false
      }
      currentState = reducer.authentication(initialState, {
        type: AuthenticationEvent.LOGIN_SUCCESS,
        token: 'theToken',
        data: {isAdministrator: true, name: 'UserName'}
      })
    })
    it('token is set', () => {
      expect(currentState.token).toEqual('theToken')
    })
    it('login has finished is true', () => {
      expect(currentState.hasFinished).toBe(true)
    })
    it('user is an administrator', () => {
      expect(currentState.isAdministrator).toBe(true)
    })
    it('UserName is logged in', () => {
      expect(currentState.userName).toEqual('UserName')
    })
  })
  describe('on login started', () => {
    let currentState: AuthenticationState
    beforeEach(() => {
      const initialState: AuthenticationState = {
        token: '',
        isAdministrator: false,
        hasFinished: false,
        userName: ''
      }
      currentState = reducer.authentication(initialState, {type: AuthenticationEvent.LOGIN_STARTED})
    })
    it('token is empty', () => {
      expect(currentState.token).toEqual('')
    })
    it('login has finished is false', () => {
      expect(currentState.hasFinished).toBe(false)
    })
    it('user is not an administrator', () => {
      expect(currentState.isAdministrator).toBe(false)
    })
  })
  describe('on login error', () => {
    let currentState: AuthenticationState
    beforeEach(() => {
      const initialState: AuthenticationState = {
        token: '',
        isAdministrator: false,
        hasFinished: false,
        userName: ''
      }
      currentState = reducer.authentication(initialState, {type: AuthenticationEvent.LOGIN_ERROR})
    })
    it('token is empty', () => {
      expect(currentState.token).toEqual('')
    })
    it('login has finished is true', () => {
      expect(currentState.hasFinished).toBe(true)
    })
    it('user is not an administrator', () => {
      expect(currentState.isAdministrator).toBe(false)
    })
  })
  describe('on logout success', () => {
    let currentState: AuthenticationState
    beforeEach(() => {
      const initialState: AuthenticationState = {
        token: 'someValue',
        isAdministrator: true,
        hasFinished: true,
        userName: 'someName'
      }
      currentState = reducer.authentication(initialState, {type: AuthenticationEvent.LOGOUT_SUCCESS})
    })
    it('token is empty', () => {
      expect(currentState.token).toEqual('')
    })
    it('login has finished is false', () => {
      expect(currentState.hasFinished).toBe(false)
    })
    it('user is not an administrator', () => {
      expect(currentState.isAdministrator).toBe(false)
    })
  })
})
