import {
  AnyAction,
  applyMiddleware,
  combineReducers,
  compose,
  createStore,
  Store
} from 'redux'
import createSagaMiddleware from 'redux-saga'
import {createLogger} from 'redux-logger'
import * as History from 'history'
import {
  routerMiddleware,
  routerReducer
} from 'react-router-redux'
import createTestLogger from '../../test/logger'

const createBrowserHistory = History.createBrowserHistory
const createMemoryHistory = History.createMemoryHistory

export class HistoryFactory {
  static createHistory(basename = '/'): any {
    return createBrowserHistory({basename})
  }

  static createTestHistory():any {
    return createMemoryHistory()
  }
}

export default class StoreFactory {
  static _createStore(reducers: any, rootSaga: any, history: any, logger: any): Store {
    const sagaMiddleware = createSagaMiddleware()

    const middlewares = applyMiddleware<AnyAction>(
      sagaMiddleware,
      routerMiddleware(history),
      logger
    )
    const allReducers = Object.assign(reducers, {router: routerReducer})
    const store: unknown = createStore(
      combineReducers(allReducers),
      compose(middlewares)
    )

    sagaMiddleware.run(rootSaga)

    return store as Store
  }

  static createStore(reducers: any, rootSaga: any, history: any): any {
    return this._createStore(reducers, rootSaga, history, createLogger({collapsed: true}))
  }

  static createTestStore(reducers: any, rootSaga: any, history: any): any {
    return this._createStore(reducers, rootSaga, history, createTestLogger())
  }


}
