import rootSaga from '../sagas/rootSaga'
import StoreFactory, {HistoryFactory} from './store'
import rootReducer from '../reducers/rootReducer'
import { AnyAction, Store } from 'redux'

describe('store', () => {
  let store: Store<any, AnyAction>
  beforeEach(() =>{
    store = StoreFactory.createStore(rootReducer, rootSaga, HistoryFactory.createTestHistory())
  })
  it('can be created', () => {
    expect(store).toBeDefined()
  })
})
