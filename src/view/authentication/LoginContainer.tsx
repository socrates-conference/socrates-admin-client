import React, {Component} from 'react'
import {connect} from 'react-redux'
import type {AuthenticationState} from '../reducers/authenticationReducer'

import './authentication.css'
import LoginForm from './LoginForm'
import isValidEmailFormat from '../../utils/isValidEmailFormat'
import {login} from '../commands/authenticationCommand'
import {withRouter} from 'react-router'


type StateFromProps = {
  state: AuthenticationState
}

type DispatchFromProps = {
  login: (email: string, password: string, redirectGoal: string) => void
}

export type Props = StateFromProps & DispatchFromProps & {
  from?: string
  location?: any
}

type State = {
  email: string
  password: string
  hasValidEmail: boolean
  hasValidPassword: boolean
}


export class LoginContainer extends Component<Props, State> {
  state = {
    email: '',
    password: '',
    hasValidEmail: false,
    hasValidPassword: false
  }


  login = (): void => {
    const redirectGoal =
            this.props.location?.state?.from || '/home'
    this.props.login(this.state.email, this.state.password, redirectGoal)
  }

  onEmailChange = (email: string): void => {
    const hasValidEmail = email.trim().length > 0 && isValidEmailFormat(email)
    this.setState({...this.state, email, hasValidEmail})
  }

  onPasswordChange = (password: string): void => {
    const hasValidPassword = password.trim().length > 0
    this.setState({...this.state, password, hasValidPassword})
  }

  render = (): JSX.Element => {
    const {email, password, hasValidEmail, hasValidPassword} = this.state
    const showErrorMessage = this.props.state.hasFinished && this.props.state.token.trim().length === 0
    return (<div id="login">
      <LoginForm
              email={email}
              hasValidEmail={hasValidEmail}
              password={password}
              hasValidPassword={hasValidPassword}
              onEmailChange={this.onEmailChange}
              onPasswordChange={this.onPasswordChange}
              onSubmit={this.login}
              showErrorMessage={showErrorMessage}
      />
    </div>)
  }
}

const mapStateToProps = (state: any): StateFromProps => {
  return {state: {...state.authentication}}
}

const mapDispatchToProps = {
  login
}

export default withRouter(connect<StateFromProps, DispatchFromProps>(mapStateToProps, mapDispatchToProps)(LoginContainer))
