import React from 'react'
import {mount, ReactWrapper} from 'enzyme'
import LoginForm from './LoginForm'
import * as sinon from 'sinon'
import { SinonSpy } from 'sinon'

describe('(Component) LoginForm', () => {
  let loginForm: ReactWrapper
  describe('renders', () => {
    beforeEach(() => {
      loginForm = _mount({})
    })
    it('without exploding', () => {
      expect(loginForm).toHaveLength(1)
    })

    it('correctly', () => {
      expect(loginForm).toMatchSnapshot()
    })
  })

  describe('filled with valid data', () => {
    let spy: SinonSpy
    beforeEach(() => {
      spy = sinon.spy()
      loginForm = _mount({
        email: 'valid@email.de',
        hasValidEmail: true,
        password: 'password',
        hasValidPassword: true,
        onSubmit: spy
      })
    })
    afterEach(()=>{
      spy.resetHistory()
    })
    it('button is enabled', () => {
      expect(loginForm.find('#auth-login').prop('disabled')).toBe(false)
    })
    it('on button click submit is called', () => {
      loginForm.find('#auth-login').simulate('submit')
      expect(spy.calledOnce).toBe(true)
    })

  })
  describe('button is disabled', () => {
    it('when email is invalid ', () => {
      loginForm = _mount({
        email: '',
        hasValidEmail: false,
        password: 'password',
        hasValidPassword: true
      })
      expect(loginForm.find('#auth-login').prop('disabled')).toBe(true)
    })
    it('when password is empty', () => {
      loginForm = _mount({
        email: 'valid@email.de',
        hasValidEmail: true,
        password: '',
        hasValidPassword: false
      })
      expect(loginForm.find('#auth-login').prop('disabled')).toBe(true)
    })
  })

  describe('change is detected', () => {
    function setInputValue(selector:string, value:string): void {
      const wrapper = loginForm.find(selector)
      // @ts-ignore
      wrapper.instance().value = value
      wrapper.simulate('change', {target: {value}})
      loginForm.update()
    }

    let spy: SinonSpy
    beforeEach(() => {
      spy = sinon.spy()
    })
    afterEach(()=>{
      spy.resetHistory()
    })
    it('when email changes, changes are registered', () => {
      loginForm = _mount({onEmailChange: spy})
      setInputValue('#auth-email', 'valid@email.de')
      expect(spy.withArgs('valid@email.de').calledOnce).toBe(true)
    })
    it('when password changes, changes are registered', () => {
      loginForm = _mount({onPasswordChange: spy})
      setInputValue('#auth-password', 'pwd')
      expect(spy.withArgs('pwd').calledOnce).toBe(true)
    })
  })
})

const defaultProps = {
  email: '',
  hasValidEmail: false,
  password: '',
  hasValidPassword: false,
  onEmailChange: ()=>{},
  onPasswordChange: ()=>{},
  onSubmit: ()=>{}
}
const _mount = (props:any): ReactWrapper => {
  const finalProps = {...defaultProps, ...props}
  return mount(<LoginForm {...finalProps}/>)
}
