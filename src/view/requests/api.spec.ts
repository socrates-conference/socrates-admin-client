import axios from 'axios'
import {
  readApplications, login, deleteApplication, registerApplication, updateApplication, readOptions,
  readApplicationsByRoomType, readConference, readRegistrationsByRoomType, readParticipants, deleteParticipant
} from './api'
import{mocked} from 'ts-jest/utils'
import {emptyApplicationListItem} from '../applicantList/commons'
import {emptyParticipantListItem} from '../participantList/commons'

jest.mock('axios')
const axiosMock = mocked(axios, true)
describe('api', () => {
  describe('reads', () => {
    let promise:any
    beforeEach(() => {
      axiosMock.get.mockImplementation(() => {
        return promise
      })
    })
    afterEach(() => {
      axiosMock.get.mockReset()
    })

    it('all applications', (done) => {
      promise = Promise.resolve({data: []})
      readApplications().then((rows) => {
        expect(rows.length).toBe(0)
        done()
      })
    })
    it('applications by room', (done) => {
      promise = Promise.resolve({data: []})
      readApplicationsByRoomType('testRoom').then((rows) => {
        expect(rows.length).toBe(0)
        done()
      })
    })
    it('application options', (done) => {
      promise = Promise.resolve({data: []})
      readOptions().then((options) => {
        expect(options.length).toBe(0)
        done()
      })
    })
  })
  describe('login', () => {
    let promise:Promise<any>
    beforeEach(() => {
      axiosMock.post.mockImplementation(() => {
        return promise
      })
    })
    afterEach(() => {
      axiosMock.post.mockReset()
    })
    it('returns the token string, as received from the server on calling login', (done) => {
      promise = Promise.resolve({data: {token: 'TheToken'}})
      login('token').then((result) => {
        expect(result).toEqual('TheToken')
        done()
      })
    })
    it('login returns empty string if server request fails', (done) => {
      promise = Promise.reject(new Error('error'))
      login('token').catch((result) => {
        expect(result.message).toBe('error')
        done()
      })
    })
  })
  describe('delete application', () => {
    let promise: Promise<any>
    beforeEach(() => {
      axiosMock.delete.mockImplementation(() => {
        return promise
      })
    })
    afterEach(() => {
      axiosMock.delete.mockReset()
    })
    it('returns true when some rows affected', (done) => {
      promise = Promise.resolve({status: 200})
      deleteApplication(emptyApplicationListItem).then((result) => {
        expect(result).toBe(true)
        done()
      })
    })
    it('returns false when no rows affected', (done) => {
      promise = Promise.resolve({status: 500})
      deleteApplication(emptyApplicationListItem).then((result) => {
        expect(result).toBe(false)
        done()
      })
    })
    it('returns false when no rows affected', (done) => {
      promise = Promise.reject(new Error('error'))
      deleteApplication(emptyApplicationListItem).catch((result) => {
        expect(result.message).toBe('error')
        done()
      })
    })
  })
  describe('update application', () => {
    let promise: Promise<any>
    beforeEach(() => {
      axiosMock.put.mockImplementation(() => {
        return promise
      })
    })
    afterEach(() => {
      axiosMock.put.mockReset()
    })
    it('returns true when some rows affected', (done) => {
      promise = Promise.resolve({status: 200})
      updateApplication(emptyApplicationListItem).then((result) => {
        expect(result).toBe(true)
        done()
      })
    })
    it('returns false when no rows affected', (done) => {
      promise = Promise.resolve({status: 500})
      updateApplication(emptyApplicationListItem).then((result) => {
        expect(result).toBe(false)
        done()
      })
    })
    it('returns false when no rows affected', (done) => {
      promise = Promise.reject(new Error('error'))
      updateApplication(emptyApplicationListItem).catch((result) => {
        expect(result.message).toBe('error')
        done()
      })
    })
  })
  describe('register application', () => {
    let promise: Promise<any>
    beforeEach(() => {
      axiosMock.post.mockImplementation(() => {
        return promise
      })
    })
    afterEach(() => {
      axiosMock.post.mockReset()
    })
    it('returns true when some rows affected', (done) => {
      promise = Promise.resolve({status: 200})
      registerApplication(emptyApplicationListItem).then((result) => {
        expect(result).toBe(true)
        done()
      })
    })
    it('returns false when no rows affected', (done) => {
      promise = Promise.resolve({status: 500})
      registerApplication(emptyApplicationListItem).then((result) => {
        expect(result).toBe(false)
        done()
      })
    })
    it('returns false when no rows affected', (done) => {
      promise = Promise.reject(new Error('error'))
      registerApplication(emptyApplicationListItem).catch((result) => {
        expect(result.message).toBe('error')
        done()
      })
    })
  })
  describe('reads app data', () => {
    let promise: Promise<any>
    beforeEach(() => {
      axiosMock.get.mockImplementation(() => {
        return promise
      })
    })
    afterEach(() => {
      axiosMock.get.mockReset()
    })
    it('returns conference data', (done) => {
      promise = Promise.resolve({data: {name:'SoCraTes'}})
      readConference().then((result) => {
        expect(result.name).toEqual('SoCraTes')
        done()
      })
    })
  })
  describe('reads dashboard data', () => {
    let promise: Promise<any>
    beforeEach(() => {
      axiosMock.get.mockImplementation(() => {
        return promise
      })
    })
    afterEach(() => {
      axiosMock.get.mockReset()
    })
    it('returns RegistrationsByRoomType data', (done) => {
      promise = Promise.resolve({data: []})
      readRegistrationsByRoomType().then((result) => {
        expect(result).toEqual([])
        done()
      })
    })
  })
  describe('read participants', () => {
    let promise: Promise<any>
    beforeEach(() => {
      axiosMock.get.mockImplementation(() => {
        return promise
      })
    })
    afterEach(() => {
      axiosMock.get.mockReset()
    })

    it('without error', (done) => {
      promise = Promise.resolve({data: []})
      readParticipants().then((rows) => {
        expect(rows.length).toBe(0)
        done()
      })
    })
  })
  describe('delete participant', () => {
    let promise: Promise<any>
    beforeEach(() => {
      axiosMock.delete.mockImplementation(() => {
        return promise
      })
    })
    afterEach(() => {
      axiosMock.delete.mockReset()
    })
    it('returns true when status 200', (done) => {
      promise = Promise.resolve({status: 200})
      deleteParticipant(emptyParticipantListItem).then((result) => {
        expect(result).toBe(true)
        done()
      })
    })
    it('returns false when status 500', (done) => {
      promise = Promise.resolve({status: 500})
      deleteParticipant(emptyParticipantListItem).then((result) => {
        expect(result).toBe(false)
        done()
      })
    })
    it('returns false when error', (done) => {
      promise = Promise.reject(new Error('error'))
      deleteParticipant(emptyParticipantListItem).catch((result) => {
        expect(result.message).toBe('error')
        done()
      })
    })
  })
})
