import axios from 'axios'
import type {ApplicationListItem} from '../applicantList/commons'
import config from '../../config'
import type {RegistrationsByRoomType} from '../reducers/dashboardReducer'
import type {Conference} from '../reducers/appReducer'
import moment from 'moment/moment'
import type {ParticipantListItem} from '../participantList/commons'
import {Optional} from '../../types'

export const login = (requestToken: string): Promise<string> => {
  return axios.post(`${config.adminBackend}/login`, {requestToken})
    .then(response => response.data.token)
}


export const readApplications = (): Promise<Array<ApplicationListItem>> => {
  return axios.get(config.adminBackend + '/management/application-list').then(response => response.data)
}
export const readApplicationsByRoomType = (roomType: string): Promise<Array<ApplicationListItem>> => {
  return axios.get(config.adminBackend + '/management/application-list/by-room/' + roomType)
    .then(response => response.data)
}

export const readOptions = (): Promise<Optional<Array<Object>>> => {
  return axios.get(config.serverBackend + '/applications/options')
    .then(response => response.data)
}

export const updateApplication = (data: ApplicationListItem): Promise<boolean> =>
  axios.put(config.adminBackend + '/management/application-list/' + data.applicantId, data)
    .then(result => result.status === 200)

export const deleteApplication = (data: ApplicationListItem): Promise<boolean> =>
  axios.delete(config.adminBackend + '/management/application-list/' + data.personId)
    .then(result => result.status === 200)

export const registerApplication = (data: ApplicationListItem): Promise<boolean> =>
  axios.post(config.adminBackend + '/management/application-list/' + data.personId, data)
    .then(result => result.status === 200)

export const readConference = async (): Promise<Conference> => {
  try {
    const {data} = await axios.get(config.adminBackend + '/management/conference')
    if (data && data.name) {
      return {
        name: data.name,
        nameLong: data.nameLong,
        year: data.year,
        startDate: moment(data.startDate),
        endDate: moment(data.endDate),
        startApplications: moment(data.startApplications),
        endApplications: moment(data.endApplications),
        lotteryDay: moment(data.lotteryDay),
        dinnerPrice: data.dinnerPrice,
        flatThursday: data.flatThursday,
        flatFridaySaturday: data.flatFridaySaturday,
        flatSunday: data.flatSunday,
        roomTypes: data.roomTypes,
        lengthsOfStay: data.lengthsOfStay
      }
    }
  } catch (e ) {
    throw new Error(e.message)
  }
  throw new Error('No Conference Data available')

}

export const readRegistrationsByRoomType = (): Promise<Array<RegistrationsByRoomType>> => {
  return axios.get(config.adminBackend + '/management/lottery/registrations/by-room-type/')
    .then(result => result.data)
}

export const readApplicationsForLottery = (): Promise<Array<Object>> => {
  return axios.get(config.adminBackend + '/management/lottery/applications/by-room-type/')
    .then(result => result.data)
}


export const readParticipants = (): Promise<Array<ParticipantListItem>> => {
  return axios.get(config.adminBackend + '/management/registration-list').then(response => response.data)
}

export const deleteParticipant = (data: ParticipantListItem): Promise<boolean> =>
  axios.delete(config.adminBackend + '/management/registration-list/' + data.personId)
    .then(result => result.status === 200)

export const updateParticipantRoomType = (data: ParticipantListItem): Promise<boolean> =>
  axios.post(config.adminBackend + '/management/registration-list/' + data.personId, {roomType: data.roomTypes})
    .then(result => result.status === 200)

export const sendConfirmationReminder = (data: ParticipantListItem): Promise<boolean> =>
  axios.put(config.adminBackend + '/management/registration-list/confirmation-reminder/'
    + (data.personId > 0 ? data.personId : ''))
    .then(result => result.status === 200)
