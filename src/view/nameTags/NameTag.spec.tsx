import React from 'react'
import {shallow, ShallowWrapper} from 'enzyme'
import NameTag from './NameTag'


describe('(Component) NameTag', () => {
  let wrapper: ShallowWrapper<NameTag>
  beforeEach(() => {
    const data = {
      nickName: 'Nick',
      fullName: 'Nick Name',
      social: '@nick_name',
      pronouns: ['they', 'he', 'her']
    }
    wrapper = shallow(<NameTag data={data}/>)
  })

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
})
