import React from 'react'
import {
  shallow,
  ShallowWrapper
} from 'enzyme'
import NameTagsContainer, {NameTagsContainer as NameTagsContainerComponent, NameTagProps} from './NameTagsContainer'
import {createStore} from 'redux'


const INITIAL_STATE = {
  participantList: {
    rows: [
      {
        firstName: 'Nick',
        lastName: 'Name',
        labelName: 'Nickey',
        social: '@Nick',
        pronoun: 'a,b/c , D / e',
        isConfirmed: true
      }
    ]
  }
}
const reducer = (): any  => ({...INITIAL_STATE})
const store = createStore(reducer, INITIAL_STATE)

describe('(Component) NameTagsContainer', () => {
  let wrapper: ShallowWrapper<NameTagProps, {}, NameTagsContainerComponent>
  beforeEach(() => {
    // @ts-ignore
    wrapper = shallow(<NameTagsContainer store={store}/>).dive()
  })

  it('renders without exploding', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })
  it('derives tag props from state', () => {
    const data = wrapper.props().data
    expect(data).toHaveLength(1)
    expect(data[0].nickName).toEqual('Nickey')
    expect(data[0].fullName).toEqual('Nick Name')
    expect(data[0].social).toEqual('@Nick')
    expect(data[0].pronouns).toEqual(['a', 'b', 'c', 'd', 'e'])
  })
})
