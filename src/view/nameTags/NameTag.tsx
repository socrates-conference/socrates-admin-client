import React from 'react'
// @ts-ignore
import styled from '@react-pdf/styled-components'
import {Font} from '@react-pdf/renderer'

export type NameTagData = {
  nickName?: string
  fullName: string
  social: string
  pronouns: Array<string>
}

type Props = {
  data: NameTagData
}

const NameTagArea = styled.View`
    height: 5.7cm;
    width: 9cm;
    justify-content: space-between;
    flex-direction: column;
    border-bottom: 1pt dashed #000;
    border-right: 1pt dashed #000;
`

const NameArea = styled.View`
    margin-bottom: 5pt;
    flex-direction: row;
    min-height: 3cm;
    border-bottom: 2pt solid #999;
`

const NickName = styled.Text`
    margin: auto;
    width: 66%;
    padding: 5pt;
    font-size: 22pt;
    font-family: 'Helvetica-Bold';
    font-weight: heavy;
    text-align: center;
`

const FullName = styled.Text`
    margin-top: 10pt;
    font-size: 12pt;
    font-family: 'Helvetica';
    font-weight: normal;
    text-align: center;
`

const SocialMedia = styled.Text`
    font-size: 18pt;
    font-family: 'Helvetica';
    text-align: center;
    color: #555555;
`

const Pronoun = styled.Text`
    margin: auto;
    width: 34%;
    font-size: 22pt;
    font-family: 'Helvetica-Bold';
    text-align: center;
    color: #555555;
`
const PrintableArea = styled.View`
    width: 100%;
    height: 4.7cm;
`

const EmptyArea = styled.View`
    width: 100%;
    height: 1cm;
`


const noWrap = (word: string): string[] => [word]
Font.registerHyphenationCallback(noWrap)

export default function NameTag(props: Props): JSX.Element {
  const data = props.data
  return <NameTagArea wrap={false}>
    <PrintableArea>
      <NameArea>
        <NickName>{data.nickName}</NickName>
        <Pronoun>{data.pronouns.join('\n')}</Pronoun>
      </NameArea>
      {data.social !== '' && <SocialMedia>{data.social}</SocialMedia>}
    </PrintableArea>
    <EmptyArea>
      <FullName>{data.fullName}</FullName>
    </EmptyArea>
  </NameTagArea>
}

