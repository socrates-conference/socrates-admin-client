import React, {Component} from 'react'
import {connect} from 'react-redux'
import {
  Document,
  Page,
  PDFViewer,
  StyleSheet,
  View
} from '@react-pdf/renderer'
import type {NameTagData} from './NameTag'
import NameTag from './NameTag'
import {getParticipants} from '../participantList/participantListCommand'
import {ParticipantListItem} from '../participantList/commons'

type StateFromProps = {
  data: Array<NameTagData>
}
type DispatchFromProps = {
  getParticipants: () => void
}

export type NameTagProps = StateFromProps & DispatchFromProps

const styles = StyleSheet.create({
  page: {},
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  }
})

export class NameTagsContainer extends Component<NameTagProps> {
  componentDidMount = (): void => {
    this.props.getParticipants()
  }

  render = (): JSX.Element => {
    return (<div className="container">
      <Header />
      <NameTagsPdf data={this.props.data} />
    </div>)
  }
}

const Header = (): JSX.Element => <div className="row">
  <div className="col-sm-12">
    <div className="page-header"><h1>Name Tags</h1></div>
  </div>
</div>

const NameTagsPdf = ({data}:{data: NameTagData[]}): JSX.Element => {
  return <div className="row">
    <div className="col-md-12">
      <PDFViewer width="100%" height="800px">
        <Document>
          <Page size="A4" style={styles.page} orientation="landscape" wrap>
            <NameTags data={data} />
          </Page>
        </Document>
      </PDFViewer>
    </div>
  </div>
}

const NameTags = ({data}:{data: NameTagData[]}): JSX.Element => {
  return <View style={styles.container}>
    {data.map((item, index) => <NameTag key={index} data={item}/>)}
  </View>
}

const mapStateToProps = (state: any): StateFromProps => {
  function toFullName(participant: ParticipantListItem): string {
    return participant.firstName || participant.lastName
      ? `${participant.firstName} ${participant.lastName}`.trim()
      : ''
  }

  function toNickName(participant: ParticipantListItem): string | undefined {
    return participant.labelName ? participant.labelName : participant.firstName
  }

  function toPronouns(participant: ParticipantListItem): string[] {
    return participant.pronoun
      ? participant.pronoun.replace(/,/g, '/').toLowerCase().split('/')
        .map((p: string) => p.trim())
      : []
  }

  const toNameTagItem = (participant: ParticipantListItem): NameTagData => ({
    fullName: toFullName(participant),
    nickName: toNickName(participant),
    social: participant.social || '',
    pronouns: toPronouns(participant)
  })
  const rows: ParticipantListItem[] = state.participantList?.rows
  const confirmedRows: ParticipantListItem[] = rows?.filter(p => p.isConfirmed)
  const tags: NameTagData[] | undefined = confirmedRows?.map(toNameTagItem)
  return {
    data: tags
  }
}

const mapDispatchToProps = {getParticipants}

export default connect<StateFromProps, DispatchFromProps>(mapStateToProps, mapDispatchToProps)(NameTagsContainer)
