import type {Config} from './types'

const config: Config = {
  environment: 'dev',
  jwtSecret: '$lsRTf!gksTRcDWs', // jwt key to work locally.
  serverBackend: '/server/api/v1',
  adminBackend: '/server/api/v1'
}

export default config
