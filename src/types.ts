export type Config = {
  environment: string
  jwtSecret: string
  serverBackend: string
  adminBackend: string
}

export type Optional<T> = T | undefined

export type LabeledItem<T = string> = { value: T, label: string}


export function ensure<T>(item: Optional<T>): T {
  if (item === undefined) throw new Error ('Item is not supposed to be undefined')
  return item
}

export function empty(text: Optional<string>): boolean {
  const probe = text
  return probe === undefined || probe.trim().length === 0
}
