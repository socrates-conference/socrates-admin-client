import './setup'
import {JSDOM} from 'jsdom'

export default function (): HTMLElement | null {
  // @ts-ignore
  global.jsdom = new JSDOM('<!doctype html><html><body><div id="root"></div></div></body></html>',
                           {url: 'https://testhost.org'})
  // @ts-ignore
  global.window = global.jsdom.window
  global.window.scrollTo = () => {}
  global.document = window.document
  global.navigator = window.navigator
  return global.document.getElementById('root')
}
