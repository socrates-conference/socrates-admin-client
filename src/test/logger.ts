import {AnyAction, Store } from "redux"

const renderToConsole = (obj: Record<string, any>, rightArrow:string): string => {
  let formatted = ''
  Object.keys(obj).forEach(key => {
    if (key.length > 0) {
      formatted += `${rightArrow} ${key} `
    }
    if (obj.hasOwnProperty(key)) {
      formatted += `${JSON.stringify(obj[key])}\n`
    }
  })

  return formatted
}

const downArrow = '▼'
const rightArrow = '▶'

export default () => {
  return (store:Store<any, AnyAction>) => (next:Function) => (action: AnyAction) => {
    const isDebugMode = process.env.DEBUG && process.env.DEBUG.indexOf('*')>=0
    if (!isDebugMode || action.type === '@@RESET_TESTER') {
      return next(action)
    }

    const {getState} = store

    const prevState = renderToConsole(getState(), rightArrow)
    const actionValue = renderToConsole(action, rightArrow)
    const returnValue = next(action)
    const nextState = renderToConsole(getState(), rightArrow)
    const time = new Date()

    const message = `${downArrow} action ${action.type} @`
      + ` ${time.getHours()}:${time.getMinutes()}:${time.getSeconds()}`

    const output = `${message}\n` +
      `  prev state\n${prevState}\n` +
      `  action\n${actionValue}\n` +
      `  next\n${nextState}`

    console.log(output)
    return returnValue
  }
}
