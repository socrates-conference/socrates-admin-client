/* eslint-disable */

import moment from 'moment';

export const conference = {
  name: 'SoCraTes',
  nameLong: 'Software Craft and Testing Conference',
  year: 2018,
  startDate: moment('2108-08-23T17:00:00.000+02'),
  endDate: moment('2108-08-26T17:00:00.000+02'),
  startApplications: moment('2108-04-15T19:00:00.000+02'),
  endApplications: moment('2108-08-01T19:00:00.000+02'),
  lotteryDay: moment('2108-05-15T19:00:00.000+02'),
  dinnerPrice: 13.30,
  flatThursday: 7.90,
  flatFridaySaturday: 52.90,
  flatSunday: 45.00,
  roomTypes: [
    {
      id: 'single',
      display: 'Single',
      nightPricePerPerson: 62.50,
      numberOfRooms: 120,
      peoplePerRoom: 1,
      reservedOrganization: 10,
      reservedSponsors: 50,
      lotterySortOrder: 4
    },
    {
      id: 'bedInDouble',
      display: 'Double shared',
      nightPricePerPerson: 47.95,
      numberOfRooms: 16,
      peoplePerRoom: 2,
      reservedOrganization: 4,
      reservedSponsors: 0,
      lotterySortOrder: 3
    },
    {
      id: 'juniorExclusively',
      display: 'Junior (exclusively)',
      nightPricePerPerson: 62.50,
      numberOfRooms: 42,
      peoplePerRoom: 1,
      reservedOrganization: 0,
      reservedSponsors: 0,
      lotterySortOrder: 2
    },
    {
      id: 'juniorShared',
      display: 'Junior (shared)',
      nightPricePerPerson: 45.45,
      numberOfRooms: 14,
      peoplePerRoom: 2,
      reservedOrganization: 0,
      reservedSponsors: 0,
      lotterySortOrder: 1
    }
  ],
  lengthOfStay: [
    {id: 'thSa', from: 'Thursday', to: 'Saturday evening', numberOfNights: 2, numberOfDinners: 3, sundayFlat: false},
    {id: 'thSuM', from: 'Thursday', to: 'Sunday morning', numberOfNights: 3, numberOfDinners: 3, sundayFlat: false},
    {id: 'thSuA', from: 'Thursday', to: 'Sunday evening', numberOfNights: 3, numberOfDinners: 3, sundayFlat: true},
    {id: 'ThMo', from: 'Thursday', to: 'Monday morning', numberOfNights: 4, numberOfDinners: 4, sundayFlat: true}
  ]
};

export const info =
  [
    {
      roomTypeId: 'single',
      roomType: 'Single',
      waitingPeople: 134,
      registeredPeople: 0,
      diversityPeople: 60,
      free: 120
    }, {
    roomTypeId: 'bedInDouble',
    roomType: 'Double shared',
    waitingPeople: 134,
    registeredPeople: 0,
    diversityPeople: 16,
    free: 32
  }, {
    roomTypeId: 'juniorExclusively',
    roomType: 'Junior (exclusively)',
    waitingPeople: 133,
    registeredPeople: 0,
    diversityPeople: 21,
    free: 42
  }, {
    roomTypeId: 'juniorShared',
    roomType: 'Junior (shared)',
    waitingPeople: 100,
    registeredPeople: 0,
    diversityPeople: 14,
    free: 28
  }
  ];

export const distribution = [
  {
    info: {
      roomTypeId: 'single',
      roomType: 'Single',
      waitingPeople: 134,
      registeredPeople: 0,
      diversityPeople: 60,
      free: 5
    },
    applications:
      [
        {
          personId: 1153,
          applicantId: 1,
          date: '2018-03-02T04:48:13.000Z',
          name: 'name 31',
          email: 'name31@email.de',
          roomTypes: ['single'],
          diversityReason: ''
        }, {
        personId: 1351,
        applicantId: 2,
        date: '2018-03-02T04:53:14.000Z',
        name: 'name 229',
        email: 'name229@email.de',
        roomTypes: ['single'],
        diversityReason: 'yes'
      }, {
        personId: 1376,
        applicantId: 3,
        date: '2018-03-02T10:18:03.000Z',
        name: 'name 254',
        email: 'name254@email.de',
        roomTypes: ['single'],
        diversityReason: ''
      }, {
        personId: 1214,
        applicantId: 4,
        date: '2018-03-02T12:08:15.000Z',
        name: 'name 92',
        email: 'name92@email.de',
        roomTypes: ['single'],
        diversityReason: ''
      }, {
        personId: 1382,
        applicantId: 5,
        date: '2018-03-02T15:03:13.000Z',
        name: 'name 260',
        email: 'name260@email.de',
        roomTypes: ['single'],
        diversityReason: ''
      }
      ],
    drawn: [1, 2, 3, 4, 5]
  },
  {
    info: {
      roomTypeId: 'bedInDouble',
      roomType: 'Double shared',
      waitingPeople: 134,
      registeredPeople: 0,
      diversityPeople: 16,
      free: 4
    },
    applications:
      [
        {
          personId: 1358,
          applicantId: 6,
          date: '2018-03-02T08:27:37.000Z',
          name: 'name 236',
          email: 'name236@email.de',
          roomTypes: ['bedInDouble'],
          diversityReason: ''
        }, {
        personId: 1378,
        applicantId: 7,
        date: '2018-03-03T00:58:01.000Z',
        name: 'name 256',
        email: 'name256@email.de',
        roomTypes: ['bedInDouble'],
        diversityReason: ''
      }, {
        personId: 1278,
        applicantId: 8,
        date: '2018-03-03T06:20:38.000Z',
        name: 'name 156',
        email: 'name156@email.de',
        roomTypes: ['bedInDouble'],
        diversityReason: ''
      }, {
        personId: 1297,
        applicantId: 9,
        date: '2018-03-03T08:22:47.000Z',
        name: 'name 175',
        email: 'name175@email.de',
        roomTypes: ['bedInDouble'],
        diversityReason: ''
      }, {
        personId: 1170,
        applicantId: 10,
        date: '2018-03-03T09:17:57.000Z',
        name: 'name 48',
        email: 'name48@email.de',
        roomTypes: ['bedInDouble'],
        diversityReason: ''
      }, {
        personId: 1242,
        applicantId: 11,
        date: '2018-03-03T10:42:47.000Z',
        name: 'name 120',
        email: 'name120@email.de',
        roomTypes: ['bedInDouble'],
        diversityReason: ''
      }, {
        personId: 1395,
        applicantId: 12,
        date: '2018-03-03T10:58:00.000Z',
        name: 'name 273',
        email: 'name273@email.de',
        roomTypes: ['bedInDouble'],
        diversityReason: 'yes'
      }
      ],
    drawn: [12, 11, 10, 9]
  },
  {
    info: {
      roomTypeId: 'juniorExclusively',
      roomType: 'Junior (exclusively)',
      waitingPeople: 133,
      registeredPeople: 0,
      diversityPeople: 21,
      free: 3
    },
    applications:
      [
        {
          personId: 1180,
          applicantId: 13,
          date: '2018-03-02T03:44:36.000Z',
          name: 'name 58',
          email: 'name58@email.de',
          roomTypes: ['juniorExclusively'],
          diversityReason: ''
        }, {
        personId: 1171,
        applicantId: 14,
        date: '2018-03-02T04:43:54.000Z',
        name: 'name 49',
        email: 'name49@email.de',
        roomTypes: ['juniorExclusively'],
        diversityReason: 'yes'
      }, {
        personId: 1285,
        applicantId: 15,
        date: '2018-03-02T05:21:16.000Z',
        name: 'name 163',
        email: 'name163@email.de',
        roomTypes: ['juniorExclusively'],
        diversityReason: ''
      }, {
        personId: 1391,
        applicantId: 16,
        date: '2018-03-02T11:27:13.000Z',
        name: 'name 269',
        email: 'name269@email.de',
        roomTypes: ['juniorExclusively'],
        diversityReason: 'yes'
      }, {
        personId: 1369,
        applicantId: 17,
        date: '2018-03-02T13:52:01.000Z',
        name: 'name 247',
        email: 'name247@email.de',
        roomTypes: ['juniorExclusively'],
        diversityReason: ''
      }, {
        personId: 1138,
        applicantId: 18,
        date: '2018-03-02T17:51:42.000Z',
        name: 'name 16',
        email: 'name16@email.de',
        roomTypes: ['juniorExclusively'],
        diversityReason: ''
      }, {
        personId: 1382,
        applicantId: 19,
        date: '2018-03-02T19:01:37.000Z',
        name: 'name 260',
        email: 'name260@email.de',
        roomTypes: ['juniorExclusively'],
        diversityReason: ''
      }, {
        personId: 1274,
        applicantId: 20,
        date: '2018-03-02T19:32:31.000Z',
        name: 'name 152',
        email: 'name152@email.de',
        roomTypes: ['juniorExclusively'],
        diversityReason: ''
      }
      ],
    drawn: [20, 15, 18]
  },
  {
    info: {
      roomTypeId: 'juniorShared',
      roomType: 'Junior (shared)',
      waitingPeople: 100,
      registeredPeople: 0,
      diversityPeople: 14,
      free: 5
    },
    applications:
      [
        {
          personId: 1155,
          applicantId: 21,
          date: '2018-03-02T09:18:53.000Z',
          name: 'name 33',
          email: 'name33@email.de',
          roomTypes: ['juniorShared'],
          diversityReason: 'yes'
        }, {
        personId: 1305,
        applicantId: 22,
        date: '2018-03-02T14:42:37.000Z',
        name: 'name 183',
        email: 'name183@email.de',
        roomTypes: ['juniorShared'],
        diversityReason: ''
      }, {
        personId: 1386,
        applicantId: 23,
        date: '2018-03-02T15:05:43.000Z',
        name: 'name 264',
        email: 'name264@email.de',
        roomTypes: ['juniorShared'],
        diversityReason: ''
      }, {
        personId: 1134,
        applicantId: 24,
        date: '2018-03-02T15:51:36.000Z',
        name: 'name 12',
        email: 'name12@email.de',
        roomTypes: ['juniorShared'],
        diversityReason: ''
      }, {
        personId: 1247,
        applicantId: 25,
        date: '2018-03-02T16:29:07.000Z',
        name: 'name 125',
        email: 'name125@email.de',
        roomTypes: ['juniorShared'],
        diversityReason: 'yes'
      }, {
        personId: 1245,
        applicantId: 26,
        date: '2018-03-02T17:45:23.000Z',
        name: 'name 123',
        email: 'name123@email.de',
        roomTypes: ['juniorShared'],
        diversityReason: ''
      }, {
        personId: 1359,
        applicantId: 27,
        date: '2018-03-02T19:31:31.000Z',
        name: 'name 237',
        email: 'name237@email.de',
        roomTypes: ['juniorShared'],
        diversityReason: 'yes'
      }, {
        personId: 1157,
        applicantId: 28,
        date: '2018-03-02T21:15:54.000Z',
        name: 'name 35',
        email: 'name35@email.de',
        roomTypes: ['juniorShared'],
        diversityReason: ''
      }, {
        personId: 1182,
        applicantId: 29,
        date: '2018-03-02T23:32:30.000Z',
        name: 'name 60',
        email: 'name60@email.de',
        roomTypes: ['juniorShared'],
        diversityReason: ''
      }
      ],
    drawn: [29, 28, 27, 25, 24]
  }];

export const distributionWithErrors = [
  {
    info: {
      roomTypeId: 'single',
      roomType: 'Single',
      waitingPeople: 134,
      registeredPeople: 0,
      diversityPeople: 60,
      free: 5
    },
    applications:
      [
        {
          personId: 1153,
          applicantId: 1,
          date: '2018-03-02T04:48:13.000Z',
          name: 'name 31',
          email: 'name31@email.de',
          roomTypes: ['single'],
          diversityReason: ''
        }, {
        personId: 1351,
        applicantId: 2,
        date: '2018-03-02T04:53:14.000Z',
        name: 'name 229',
        email: 'name229@email.de',
        roomTypes: ['single'],
        diversityReason: 'yes'
      }, {
        personId: 1376,
        applicantId: 3,
        date: '2018-03-02T10:18:03.000Z',
        name: 'name 254',
        email: 'name254@email.de',
        roomTypes: ['single'],
        diversityReason: ''
      }, {
        personId: 1214,
        applicantId: 4,
        date: '2018-03-02T12:08:15.000Z',
        name: 'name 92',
        email: 'name92@email.de',
        roomTypes: ['single'],
        diversityReason: ''
      }, {
        personId: 1382,
        applicantId: 5,
        date: '2018-03-02T15:03:13.000Z',
        name: 'name 260',
        email: 'name260@email.de',
        roomTypes: ['single'],
        diversityReason: ''
      }
      ],
    drawn: [1, 2, 3, 4, 5]
  },
  {
    info: {
      roomTypeId: 'bedInDouble',
      roomType: 'Double shared',
      waitingPeople: 134,
      registeredPeople: 0,
      diversityPeople: 16,
      free: 4
    },
    applications:
      [
        {
          personId: 1358,
          applicantId: 6,
          date: '2018-03-02T08:27:37.000Z',
          name: 'name 236',
          email: 'name236@email.de',
          roomTypes: ['bedInDouble'],
          diversityReason: ''
        }, {
        personId: 1378,
        applicantId: 7,
        date: '2018-03-03T00:58:01.000Z',
        name: 'name 256',
        email: 'name256@email.de',
        roomTypes: ['bedInDouble'],
        diversityReason: ''
      }, {
        personId: 1278,
        applicantId: 8,
        date: '2018-03-03T06:20:38.000Z',
        name: 'name 156',
        email: 'name156@email.de',
        roomTypes: ['bedInDouble'],
        diversityReason: ''
      }, {
        personId: 1297,
        applicantId: 9,
        date: '2018-03-03T08:22:47.000Z',
        name: 'name 175',
        email: 'name175@email.de',
        roomTypes: ['bedInDouble'],
        diversityReason: ''
      }, {
        personId: 1170,
        applicantId: 10,
        date: '2018-03-03T09:17:57.000Z',
        name: 'name 48',
        email: 'name48@email.de',
        roomTypes: ['bedInDouble'],
        diversityReason: ''
      }, {
        personId: 1242,
        applicantId: 11,
        date: '2018-03-03T10:42:47.000Z',
        name: 'name 120',
        email: 'name120@email.de',
        roomTypes: ['bedInDouble'],
        diversityReason: ''
      }, {
        personId: 1395,
        applicantId: 12,
        date: '2018-03-03T10:58:00.000Z',
        name: 'name 273',
        email: 'name273@email.de',
        roomTypes: ['bedInDouble'],
        diversityReason: 'yes'
      }
      ],
    drawn: [1, 2, 3, 4, 5]
  },
  {
    info: {
      roomTypeId: 'juniorExclusively',
      roomType: 'Junior (exclusively)',
      waitingPeople: 133,
      registeredPeople: 0,
      diversityPeople: 21,
      free: 3
    },
    applications:
      [
        {
          personId: 1180,
          applicantId: 13,
          date: '2018-03-02T03:44:36.000Z',
          name: 'name 58',
          email: 'name58@email.de',
          roomTypes: ['juniorExclusively'],
          diversityReason: ''
        }, {
        personId: 1171,
        applicantId: 14,
        date: '2018-03-02T04:43:54.000Z',
        name: 'name 49',
        email: 'name49@email.de',
        roomTypes: ['juniorExclusively'],
        diversityReason: 'yes'
      }, {
        personId: 1285,
        applicantId: 15,
        date: '2018-03-02T05:21:16.000Z',
        name: 'name 163',
        email: 'name163@email.de',
        roomTypes: ['juniorExclusively'],
        diversityReason: ''
      }, {
        personId: 1391,
        applicantId: 16,
        date: '2018-03-02T11:27:13.000Z',
        name: 'name 269',
        email: 'name269@email.de',
        roomTypes: ['juniorExclusively'],
        diversityReason: 'yes'
      }, {
        personId: 1369,
        applicantId: 17,
        date: '2018-03-02T13:52:01.000Z',
        name: 'name 247',
        email: 'name247@email.de',
        roomTypes: ['juniorExclusively'],
        diversityReason: ''
      }, {
        personId: 1138,
        applicantId: 18,
        date: '2018-03-02T17:51:42.000Z',
        name: 'name 16',
        email: 'name16@email.de',
        roomTypes: ['juniorExclusively'],
        diversityReason: ''
      }, {
        personId: 1382,
        applicantId: 19,
        date: '2018-03-02T19:01:37.000Z',
        name: 'name 260',
        email: 'name260@email.de',
        roomTypes: ['juniorExclusively'],
        diversityReason: ''
      }, {
        personId: 1274,
        applicantId: 20,
        date: '2018-03-02T19:32:31.000Z',
        name: 'name 152',
        email: 'name152@email.de',
        roomTypes: ['juniorExclusively'],
        diversityReason: ''
      }
      ],
    drawn: [20, 15, 18]
  },
  {
    info: {
      roomTypeId: 'juniorShared',
      roomType: 'Junior (shared)',
      waitingPeople: 100,
      registeredPeople: 0,
      diversityPeople: 14,
      free: 5
    },
    applications:
      [
        {
          personId: 1155,
          applicantId: 21,
          date: '2018-03-02T09:18:53.000Z',
          name: 'name 33',
          email: 'name33@email.de',
          roomTypes: ['juniorShared'],
          diversityReason: 'yes'
        }, {
        personId: 1305,
        applicantId: 22,
        date: '2018-03-02T14:42:37.000Z',
        name: 'name 183',
        email: 'name183@email.de',
        roomTypes: ['juniorShared'],
        diversityReason: ''
      }, {
        personId: 1386,
        applicantId: 23,
        date: '2018-03-02T15:05:43.000Z',
        name: 'name 264',
        email: 'name264@email.de',
        roomTypes: ['juniorShared'],
        diversityReason: ''
      }, {
        personId: 1134,
        applicantId: 24,
        date: '2018-03-02T15:51:36.000Z',
        name: 'name 12',
        email: 'name12@email.de',
        roomTypes: ['juniorShared'],
        diversityReason: ''
      }, {
        personId: 1247,
        applicantId: 25,
        date: '2018-03-02T16:29:07.000Z',
        name: 'name 125',
        email: 'name125@email.de',
        roomTypes: ['juniorShared'],
        diversityReason: 'yes'
      }, {
        personId: 1245,
        applicantId: 26,
        date: '2018-03-02T17:45:23.000Z',
        name: 'name 123',
        email: 'name123@email.de',
        roomTypes: ['juniorShared'],
        diversityReason: ''
      }, {
        personId: 1359,
        applicantId: 27,
        date: '2018-03-02T19:31:31.000Z',
        name: 'name 237',
        email: 'name237@email.de',
        roomTypes: ['juniorShared'],
        diversityReason: 'yes'
      }, {
        personId: 1157,
        applicantId: 28,
        date: '2018-03-02T21:15:54.000Z',
        name: 'name 35',
        email: 'name35@email.de',
        roomTypes: ['juniorShared'],
        diversityReason: ''
      }, {
        personId: 1182,
        applicantId: 29,
        date: '2018-03-02T23:32:30.000Z',
        name: 'name 60',
        email: 'name60@email.de',
        roomTypes: ['juniorShared'],
        diversityReason: ''
      }
      ],
    drawn: [29, 28, 27, 25, 24]
  }];