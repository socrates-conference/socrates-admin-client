import createLogger from './logger'
import {Optional} from '../types'
import {Action, AnyAction, Store} from 'redux'

describe('logger should', () => {
  let consoleLog: any
  let debugMode: Optional<string>

  beforeEach(() => {
    consoleLog = console.log
    debugMode = process.env.DEBUG
  })

  afterEach(() => {
    console.log = consoleLog
    process.env.DEBUG = debugMode
  })

  // @ts-ignore
  const store: Store<any, AnyAction> = {
    getState: () => ({})
  }

  const action = {
    type: 'SOMETHING'
  }

  const logger = createLogger()

  const nextMiddleware = (dispatchedAction: Action): any => {
    expect(dispatchedAction.type).toEqual('SOMETHING')
    return {}
  }

  it('write to console.log in debug mode', done => {
    process.env.DEBUG = '*'
    console.log = () => {
      done()
    }

    logger(store)(nextMiddleware)(action)
  })

  it('not write to console.log in non-debug mode', () => {
    process.env.DEBUG = undefined
    console.log = () => {
      fail()
    }

    logger(store)(nextMiddleware)(action)
  })
})
