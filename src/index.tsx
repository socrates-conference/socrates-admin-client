import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import rootSaga from './view/sagas/rootSaga'
import StoreFactory, {HistoryFactory} from './view/store/store'
import {unregister} from './serviceWorker'
import rootReducer from './view/reducers/rootReducer'
import './view/icons'
import './index.scss'

const history = HistoryFactory.createHistory('/admin')
const store = StoreFactory.createStore(rootReducer, rootSaga, history)
if (process.env.NODE_ENV === 'development') {
  console.info('%cAttaching store to global scope. You can access it in the console using the variable %cstore%c.',
               'color: gray; font-weight: lighter;', 'font-weight: bold;', 'font-weight: inherit;')
  // @ts-ignore
  global.store = store
}
unregister()
ReactDOM.render(<App store={store} history={history}/>, document.getElementById('root'))
