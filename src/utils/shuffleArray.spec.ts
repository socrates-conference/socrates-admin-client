import shuffleArray from './shuffleArray'

describe('shuffleArray', () => {
  it('does nothing, when there is nothing to be sorted', () => {
    const origin:any[] = []
    const shuffled = shuffleArray(origin)
    expect(origin.length).toEqual(shuffled.length)
  })
  it('returns an array with the same length as the original', () => {
    const origin = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    const shuffled = shuffleArray(origin)
    expect(origin.length).toEqual(shuffled.length)
  })
  it('returns an array that contains the same elements as the original', () => {
    const origin = [1, 2, 3]
    const shuffled = shuffleArray(origin)
    expect(shuffled).toContain(3)
    expect(shuffled).toContain(2)
    expect(shuffled).toContain(1)
  })
})
