import moment from 'moment/moment'

export default function dateFormat (value: string, format: string): string {
  if (value) {
    const theMoment = moment.utc(value)
    return theMoment.isValid() ? theMoment.format(format) : ''
  } else {
    return ''
  }
}
