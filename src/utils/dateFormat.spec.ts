import dateFormat from './dateFormat'

describe('dateFormat: ', () => {
  it('returns a string for a valid date', () => {
    const theDate = '2018-04-23T19:23:54.000Z'
    const formatted = dateFormat(theDate, 'DD.MM.YYYY HH:mm:ss.SSS.Z')
    expect(formatted.length).toBeGreaterThan(0)
  })
  it('returns empty string on invalid date string', () => {
    const theDate = 'invalid'
    expect(dateFormat(theDate, 'DD.MM.YYYY HH:mm:ss')).toEqual('')
  })
  it('returns empty string on on empty input', () => {
    const theDate = ''
    expect(dateFormat(theDate, 'DD.MM.YYYY HH:mm:ss')).toEqual('')
  })
})