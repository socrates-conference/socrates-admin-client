// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom/extend-expect'
import {JSDOM} from 'jsdom'

import {configure} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
configure({adapter: new Adapter()})
// @ts-ignore
global.jsdom = new JSDOM('<!doctype html><html><body><div id="root"></div></div></body></html>',
                         {url: 'https://testhost.org'})
// @ts-ignore
global.window = global.jsdom.window
global.window.scrollTo = () => {}
global.document = window.document
global.navigator = window.navigator
